package com.eltech.companyguide.utils;

import com.eltech.companyguide.entity.Address;
import com.eltech.companyguide.entity.Company;
import com.eltech.companyguide.model.CompanyElement;

/**
 * Created by vLiakhav on 01.05.2016.
 */
public class CompanyMapper {

    public static CompanyElement mapCompanyToCompanyElement(Company company) {
        return null;
    }

    public static Company mapCompanyElementToCompany(CompanyElement companyElement) {
        Company company = new Company();
        if (companyElement.getId() != null)
            company.setId(companyElement.getId());
        company.setFullName(companyElement.getFullName());

        Address address = new Address();
        address.setCountry(companyElement.getCountry());
        address.setCity(companyElement.getCity());
        address.setStreet(companyElement.getStreet());
        address.setBuilding(companyElement.getBuilding());
        address.setOffice(companyElement.getOffice());
        address.setPhone(companyElement.getPhone());
        address.setFax(companyElement.getFax());
        company.setAddress(address);

        company.setActivities(companyElement.getActivities());
        company.setGoods(companyElement.getGoods());
        company.setInformation(companyElement.getInformation());

        return company;
    }
}
