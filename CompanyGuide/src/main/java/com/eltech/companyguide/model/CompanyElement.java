package com.eltech.companyguide.model;

import com.eltech.companyguide.entity.Address;
import com.eltech.companyguide.entity.Company;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by vLiakhav on 01.05.2016.
 */
public class CompanyElement {

    private Long id;

    private StringProperty fullName;

    private StringProperty activities;

    private StringProperty goods;

    private StringProperty information;

    private StringProperty address;


    private StringProperty country;

    private StringProperty city;

    private StringProperty street;

    private IntegerProperty building;

    private IntegerProperty  office;

    private StringProperty phone;

    private StringProperty fax;

    public CompanyElement() {
        this.fullName = new SimpleStringProperty("");
        this.activities = new SimpleStringProperty("");
        this.goods = new SimpleStringProperty("");
        this.information = new SimpleStringProperty("");
        this.address = new SimpleStringProperty("");

        this.country = new SimpleStringProperty("");
        this.city = new SimpleStringProperty("");
        this.street = new SimpleStringProperty("");
        this.building = new SimpleIntegerProperty();
        this.office = new SimpleIntegerProperty();
        this.phone = new SimpleStringProperty("");
        this.fax = new SimpleStringProperty("");
    }
    public CompanyElement(Company company) {
        this.id = company.getId();
        this.fullName = new SimpleStringProperty(company.getFullName());
        this.activities = new SimpleStringProperty(company.getActivities());
        this.goods = new SimpleStringProperty(company.getGoods());
        this.information = new SimpleStringProperty(company.getInformation());
        this.address = new SimpleStringProperty(getAddressAsString(company.getAddress()));

        this.country = new SimpleStringProperty(company.getAddress().getCountry());
        this.city = new SimpleStringProperty(company.getAddress().getCity());
        this.street = new SimpleStringProperty(company.getAddress().getStreet());
        this.building = new SimpleIntegerProperty(company.getAddress().getBuilding());
        this.office = new SimpleIntegerProperty(company.getAddress().getOffice());
        this.phone = new SimpleStringProperty(company.getAddress().getPhone());
        this.fax = new SimpleStringProperty(company.getAddress().getFax());

    }

    private String getAddressAsString(Address address) {
        StringBuilder sb = new StringBuilder();
        sb.append(address.getCountry()).append(", ");
        sb.append(address.getCity()).append(", ул. ");
        sb.append(address.getStreet()).append(", д. ");
        sb.append(address.getBuilding()).append(", о.");
        sb.append(address.getOffice()).append(", тел. ");
        sb.append(address.getPhone()).append(", факс. ");
        sb.append(address.getFax());
        return sb.toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFax() {
        return fax.get();
    }

    public StringProperty faxProperty() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax.set(fax);
    }

    public String getPhone() {
        return phone.get();
    }

    public StringProperty phoneProperty() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone.set(phone);
    }

    public int getOffice() {
        return office.get();
    }

    public IntegerProperty officeProperty() {
        return office;
    }

    public void setOffice(int office) {
        this.office.set(office);
    }

    public int getBuilding() {
        return building.get();
    }

    public IntegerProperty buildingProperty() {
        return building;
    }

    public void setBuilding(int building) {
        this.building.set(building);
    }

    public String getStreet() {
        return street.get();
    }

    public StringProperty streetProperty() {
        return street;
    }

    public void setStreet(String street) {
        this.street.set(street);
    }

    public String getCity() {
        return city.get();
    }

    public StringProperty cityProperty() {
        return city;
    }

    public void setCity(String city) {
        this.city.set(city);
    }

    public String getCountry() {
        return country.get();
    }

    public StringProperty countryProperty() {
        return country;
    }

    public void setCountry(String country) {
        this.country.set(country);
    }

    public String getAddress() {
        return address.get();
    }

    public StringProperty addressProperty() {
        return address;
    }

    public void setAddress(String address) {
        this.address.set(address);
    }

    public String getInformation() {
        return information.get();
    }

    public StringProperty informationProperty() {
        return information;
    }

    public void setInformation(String information) {
        this.information.set(information);
    }

    public String getGoods() {
        return goods.get();
    }

    public StringProperty goodsProperty() {
        return goods;
    }

    public void setGoods(String goods) {
        this.goods.set(goods);
    }

    public String getActivities() {
        return activities.get();
    }

    public StringProperty activitiesProperty() {
        return activities;
    }

    public void setActivities(String activities) {
        this.activities.set(activities);
    }

    public String getFullName() {
        return fullName.get();
    }

    public StringProperty fullNameProperty() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName.set(fullName);
    }
}
