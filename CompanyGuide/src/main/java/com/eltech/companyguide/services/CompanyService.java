package com.eltech.companyguide.services;

import com.eltech.companyguide.dao.CompanyDAO;
import com.eltech.companyguide.entity.Company;
import com.eltech.companyguide.model.CompanyElement;
import com.eltech.companyguide.utils.CompanyMapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Created by vLiakhav on 01.05.2016.
 */
public class CompanyService {
    private CompanyDAO companyDAO;

    public CompanyService() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Companies");
        EntityManager em = emf.createEntityManager();
        companyDAO = new CompanyDAO(em);
    }

    public ObservableList<CompanyElement> getAllCompanies() {
        List<Company> companies = companyDAO.selectAllRecords();
        ObservableList<CompanyElement> companyData = FXCollections.observableArrayList();
        if(companies.size() > 0) {
            for (Company company : companies) {
                companyData.add(new CompanyElement(company));
            }
        }
        return companyData;
    }

    public boolean removeCompanyByName(CompanyElement companyElement) {
        Company company = companyDAO.getById(companyElement.getId());
        if (company != null) {
            companyDAO.remove(company);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public boolean updateCompany(CompanyElement companyElement) {
        if (companyElement != null) {
            Company company = CompanyMapper.mapCompanyElementToCompany(companyElement);
            companyDAO.update(company);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public boolean addCompany(CompanyElement companyElement) {
        if (companyElement != null) {
            Company company = CompanyMapper.mapCompanyElementToCompany(companyElement);
            companyDAO.insert(company);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

}
