package com.eltech.companyguide.controllers;

import com.eltech.companyguide.Main;
import com.eltech.companyguide.model.CompanyElement;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class CompanyOverviewController {

    @FXML
    private TableView<CompanyElement> companiesTable;
    @FXML
    private TableColumn<CompanyElement, String> fullName;


    @FXML
    private Label fullNameLabel;
    @FXML
    private Label addressLabel;
    @FXML
    private Label activitiesLabel;
    @FXML
    private Label goodsLabel;
    @FXML
    private Label informationLabel;

    // Reference to the main application.
    private Main mainApp;

    public CompanyOverviewController() {

    }

    @FXML
    private void initialize() {
        // Clear person details.
        showCompaniesDetails(null);

        // Listen for selection changes and show the person details when changed.
        companiesTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<CompanyElement>() {
            public void changed(ObservableValue<? extends CompanyElement> observable,
                                CompanyElement oldValue, CompanyElement newValue) {
                showCompaniesDetails(newValue);

            }
        });


        // Initialize the person table with the two columns.
        fullName.setCellValueFactory(new PropertyValueFactory("fullName"));

    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        companiesTable.setItems(mainApp.getService().getAllCompanies());
    }

    /**
     * Fills all text fields to show details about the person.
     * If the specified person is null, all text fields are cleared.
     *
     * @param companyElement the person or null
     */
    private void showCompaniesDetails(CompanyElement companyElement) {
        if (companyElement != null) {
            // Fill the labels with info from the person object.
            fullNameLabel.setText(companyElement.getFullName());
            addressLabel.setText(companyElement.getAddress());
            activitiesLabel.setText(companyElement.getActivities());
            goodsLabel.setText(companyElement.getGoods());
            informationLabel.setText(companyElement.getInformation());
        } else {
            // Person is null, remove all the text.
            fullNameLabel.setText("");
            addressLabel.setText("");
            activitiesLabel.setText("");
            goodsLabel.setText("");
            informationLabel.setText("");
        }
    }


    /**
     * Called when the user clicks on the delete button.
     */
    @FXML
    private void handleDeleteCompany() {
        int selectedIndex = companiesTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            if (mainApp.getService().removeCompanyByName(companiesTable.getSelectionModel().getSelectedItem())) {
                companiesTable.getItems().remove(selectedIndex);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initOwner(mainApp.getPrimaryStage());
                alert.setTitle("Удалено успешно");
                alert.setHeaderText("Запись удалена успешно");
                alert.setContentText("");
                alert.showAndWait();
            }
        }else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Ничего не выбрано");
            alert.setHeaderText("Ничего не выбрано");
            alert.setContentText("Пожалуйста, выберите какую-либо запись");
            alert.showAndWait();
        }

    }

    /**
     * Called when the user clicks the edit button. Opens a dialog to edit
     * details for the selected person.
     */
    @FXML
    private void handleEditCompany() {
        CompanyElement selectedCompany = companiesTable.getSelectionModel().getSelectedItem();
        if (selectedCompany != null) {
            boolean okClicked = mainApp.showCompanyEditDialog(selectedCompany);
            if (okClicked) {
                showCompaniesDetails(selectedCompany);
            }

        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Ничего не выбрано");
            alert.setHeaderText("Ничего не выбрано");
            alert.setContentText("Пожалуйста, выберите какую-либо запись");

            alert.showAndWait();
        }
    }

    /**
     * Called when the user clicks the new button. Opens a dialog to edit
     * details for a new person.
     */
    @FXML
    private void handleNewCompany() {
        CompanyElement companyElement = new CompanyElement();
        mainApp.showCompanyEditDialog(companyElement);
    }

    public TableView<CompanyElement> getCompaniesTable() {
        return companiesTable;
    }

    public void setCompaniesTable(TableView<CompanyElement> companiesTable) {
        this.companiesTable = companiesTable;
    }
}
