package com.eltech.companyguide.controllers;

import com.eltech.companyguide.Main;
import com.eltech.companyguide.model.CompanyElement;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.List;

/**
 * Created by vLiakhav on 01.05.2016.
 */
public class CompanyEditDialogController {

    @FXML
    private TextField fullNameField;

    @FXML
    private TextField countryField;

    @FXML
    private TextField cityField;

    @FXML
    private TextField streetField;

    @FXML
    private TextField buildingField;

    @FXML
    private TextField officeField;

    @FXML
    private TextField phoneField;

    @FXML
    private TextField faxField;

    @FXML
    private TextArea activitiesField;

    @FXML
    private TextArea goodsField;

    @FXML
    private TextArea informationField;

    private Stage dialogStage;
    private CompanyElement company;
    private boolean okClicked = false;

    // Reference to the main application.
    private Main mainApp;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }


    public void setCompany(CompanyElement company) {
        this.company = company;

        fullNameField.setText(company.getFullName());
        countryField.setText(company.getCountry());
        cityField.setText(company.getCity());
        streetField.setText(company.getStreet());
        buildingField.setText(Integer.toString(company.getBuilding()));
        officeField.setText(Integer.toString(company.getOffice()));
        phoneField.setText(company.getPhone());
        faxField.setText(company.getFax());
        activitiesField.setText(company.getActivities());
        goodsField.setText(company.getGoods());
        informationField.setText(company.getInformation());
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        boolean isNewRecord = Boolean.FALSE;
        if (isInputValid()) {
            if (company == null) {
                company = new CompanyElement();
                isNewRecord = Boolean.TRUE;
            }
            company.setFullName(fullNameField.getText());
            company.setCountry(countryField.getText());
            company.setCity(cityField.getText());
            company.setStreet(streetField.getText());
            company.setBuilding(Integer.valueOf(buildingField.getText()));
            company.setOffice(Integer.valueOf(officeField.getText()));
            company.setPhone(phoneField.getText());
            company.setFax(faxField.getText());
            company.setActivities(activitiesField.getText());
            company.setGoods(goodsField.getText());
            company.setInformation(informationField.getText());

            okClicked = true;

            boolean isSuccess = isNewRecord ? mainApp.getService().addCompany(company) : mainApp.getService().updateCompany(company);
            if (isSuccess) {
                updateRecordsInCompanyList();

                dialogStage.close();

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initOwner(mainApp.getPrimaryStage());
                alert.setTitle("Изменени успешно");
                alert.setHeaderText("Запись изменена успешно");
                alert.setContentText("");
                alert.showAndWait();
            }
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (fullNameField.getText() == null || fullNameField.getText().length() < 1
                || fullNameField.getText().length() > 500) {
            errorMessage += "Ошибка ввода в поле -Полное название- (Длина должна составлять 0-500 символов)!\n";
        }

        if (countryField.getText() == null || countryField.getText().length() < 1
                || countryField.getText().length() > 50) {
            errorMessage += "Ошибка ввода в поле -Страна- (Длина должна составлять 0-50 символов)!\n";
        }

        if (cityField.getText() == null || cityField.getText().length() < 1
                || cityField.getText().length() > 50 ) {
            errorMessage += "Ошибка ввода в поле -Город- (Длина должна составлять 0-50 символов)!\n";
        }

        if (streetField.getText() == null || streetField.getText().length() < 1
                || streetField.getText().length() > 50) {
            errorMessage += "Ошибка ввода в поле -Улица- (Длина должна составлять 0-50 символов)!\n";
        }

        if (buildingField.getText() == null || buildingField.getText().length() == 0) {
            errorMessage += "Ошибка ввода в поле -Улица-!\n";
        } else {
            // try to parse the postal code into an int.
            try {
                int building = Integer.parseInt(buildingField.getText());
                if (building < 1 || building > 9999) {
                    errorMessage += "Ошибка ввода в поле -Улица-!\n";
                }
            } catch (NumberFormatException e) {
                errorMessage += "Ошибка ввода в поле -Улица-!\n";
            }
        }

        if (officeField.getText() == null || officeField.getText().length() == 0) {
            errorMessage += "Ошибка ввода в поле -Офис-!\n";
        } else {
            try {
                int office = Integer.parseInt(officeField.getText());
                if (office < 1 || office > 9999) {
                    errorMessage += "Ошибка ввода в поле -Офис-!\n";
                }
            } catch (NumberFormatException e) {
                errorMessage += "Ошибка ввода в поле -Офис-!\n";
            }
        }

        if (phoneField.getText() == null || phoneField.getText().length() < 1
                || phoneField.getText().length() > 12) {
            errorMessage += "Ошибка ввода в поле -Номер телефона- (Длина должна составлять 0-12 символов)!\n";
        }

        if (faxField.getText() == null || faxField.getText().length() < 1 ||
                faxField.getText().length() > 12) {
            errorMessage += "Ошибка ввода в поле -Факс- (Длина должна составлять 0-12 символов)!\n";
        }

        if (activitiesField.getText() == null || activitiesField.getText().length() == 0) {
            errorMessage += "Ошибка ввода в поле -Виды деятельности-!\n";
        }

        if (goodsField.getText() == null || goodsField.getText().length() == 0) {
            errorMessage += "Ошибка ввода в поле -Товары и услуги-!\n";
        }

        if (informationField.getText() == null || informationField.getText().length() == 0) {
            errorMessage += "Ошибка ввода в поле -Информация-!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Ошибка ввода");
            alert.setHeaderText("Плжалуйста, введите корректные данные");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;
    }

    public void updateRecordsInCompanyList() {
        mainApp.getCompanyOverviewController().getCompaniesTable().setItems(mainApp.getService().getAllCompanies());
    }
}
