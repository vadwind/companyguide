package com.eltech.companyguide;

import java.io.IOException;

import com.eltech.companyguide.controllers.CompanyEditDialogController;
import com.eltech.companyguide.controllers.CompanyOverviewController;
import com.eltech.companyguide.model.CompanyElement;
import com.eltech.companyguide.services.CompanyService;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class Main extends Application {

    private Stage primaryStage;
    private AnchorPane rootLayout;

    private CompanyService service;

    private CompanyEditDialogController companyEditDialogController;
    private CompanyOverviewController companyOverviewController;

    @Override
    public void start(Stage primaryStage) {
        service = new CompanyService();
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Company guide");

        initRootLayout();
    }

    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/fxml/view.fxml"));
            rootLayout = (AnchorPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();

            // Give the controller access to the main app.
            companyOverviewController = loader.getController();
            companyOverviewController.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }

    public CompanyService getService() {
        return service;
    }

    public void setService(CompanyService service) {
        this.service = service;
    }


    public boolean showCompanyEditDialog(CompanyElement companyElement) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/fxml/edit_dialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Редактировать");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            companyEditDialogController = loader.getController();
            companyEditDialogController.setDialogStage(dialogStage);
            companyEditDialogController.setMainApp(this);
            companyEditDialogController.setCompany(companyElement);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return companyEditDialogController.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public CompanyEditDialogController getCompanyEditDialogController() {
        return companyEditDialogController;
    }

    public void setCompanyEditDialogController(CompanyEditDialogController companyEditDialogController) {
        this.companyEditDialogController = companyEditDialogController;
    }

    public CompanyOverviewController getCompanyOverviewController() {
        return companyOverviewController;
    }

    public void setCompanyOverviewController(CompanyOverviewController companyOverviewController) {
        this.companyOverviewController = companyOverviewController;
    }
}
