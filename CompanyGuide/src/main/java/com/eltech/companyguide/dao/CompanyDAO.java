package com.eltech.companyguide.dao;

import com.eltech.companyguide.entity.Address;
import com.eltech.companyguide.entity.Company;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vLiakhav on 01.05.2016.
 */
public class CompanyDAO {

    private static Logger logger = Logger.getLogger(CompanyDAO.class);

    private EntityManager entityManager;

    /**
     * Constructor EntityManager
     * @param entityManager
     */
    public CompanyDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Select all companies
     * @return List of companies
     */
    public List<Company> selectAllRecords() {

//        Address address = new Address();
//        address.setBuilding(2);
//        address.setCity("Москва");
//        address.setCountry("Россия");
//        address.setFax("123123123");
//        address.setOffice(11);
//        address.setPhone("123123123");
//        address.setStreet("Лиговский прсопект");
//
//        Company company = new Company();
//        company.setAddress(address);
//        company.setFullName("Новая компания 2");
//        company.setActivities("Консsgdалтинг");
//        company.setGoods("Ничего не происхо sdgдит");
//        company.setInformation("Информация");
//        new CompanyDAO(entityManager).insert(company);


        logger.info("Get all companies");
        List<Company> companies = entityManager.createNativeQuery("SELECT * FROM company ", Company.class).getResultList();
        return (companies != null)? companies : new ArrayList<Company>();
    }

    /**
     * Insert
     * @param company
     */
    public void insert(Company company) {
        entityManager.getTransaction().begin();
        entityManager.persist(company);
        entityManager.getTransaction().commit();
        entityManager.clear();
    }

    /**
     * Remove
     * @param company
     */
    public void remove(Company company) {
        entityManager.getTransaction().begin();
        entityManager.remove(company);
        entityManager.getTransaction().commit();
        entityManager.clear();
    }

    /**
     * Update
     * @param company
     */
    public void update(Company company) {
        entityManager.getTransaction().begin();
        entityManager.merge(company);
        entityManager.getTransaction().commit();
        entityManager.clear();
    }

    /**
     * Select by id
     * @return List of companies
     */
    public Company getById(Long id) {
        Query query = entityManager.createNativeQuery("SELECT * FROM company WHERE id=?", Company.class);
        query.setParameter(1, id);
        List<Company> companies = query.getResultList();
        return (companies != null && companies.size() > 0)? companies.get(0) : null;
    }
}
