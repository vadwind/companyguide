package com.eltech.companyguide.dao;

import org.apache.log4j.Logger;

import javax.persistence.EntityManager;

/**
 * Created by vLiakhav on 01.05.2016.
 */
public class AddressDAO {

    private static Logger logger = Logger.getLogger(AddressDAO.class);

    private EntityManager entityManager;

    /**
     * Constructor EntityManager
     * @param entityManager
     */
    public AddressDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}
