-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Апр 29 2016 г., 00:22
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `company_guide`
--

-- --------------------------------------------------------

--
-- Структура таблицы `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `building` int(4) NOT NULL,
  `office` int(4) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `fax` varchar(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `address`
--

INSERT INTO `address` (`id`, `country`, `city`, `street`, `building`, `office`, `phone`, `fax`) VALUES
(3, 'Россия', 'Санкт-Петербург', 'Торжковская', 15, 268, '+79112978522', '+79112978522'),
(4, 'Россия', 'Санкт-Петербург', 'Профессора Попова', 5, 5, '+79112201213', '+79112201213');

-- --------------------------------------------------------

--
-- Структура таблицы `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(500) NOT NULL,
  `address_id` int(11) NOT NULL,
  `activities` text NOT NULL,
  `goods` text NOT NULL,
  `information` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `company`
--

INSERT INTO `company` (`id`, `full_name`, `address_id`, `activities`, `goods`, `information`) VALUES
(1, 'Общежитие №8', 3, 'Обеспечивает жильём студентов СПбГЭТУ "ЛЭТИ"', 'Нет', 'Никакой'),
(2, 'СПбГЭТУ "ЛЭТИ" им. В.И. Ульянова (Ленина)', 4, 'Высшее учебное заведение', 'Нет', 'Никакой');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `company`
--
ALTER TABLE `company`
  ADD CONSTRAINT `company_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
