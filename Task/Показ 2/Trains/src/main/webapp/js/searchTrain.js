/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 23.04.14
 * Time: 2:52
 * To change this template use File | Settings | File Templates.
 */

var map;
var namesStations = new Array();
var coordinats = new Array();
var station;

$(document).ready(function() {
    station = 1;
    $.ajax({
        url: '/Trains/station/getStationsCoord',
        type: 'POST',
        data: ({
        }),
        success: function (data) {
            if(data != '') {
                var stations = JSON.parse(data);
                var i = 0;
                for(var key in stations){
                    namesStations[i] = stations[key]['name'];
                    var arraySt = new Array(stations[key]['lat'], stations[key]['lng'])
                    coordinats.push(arraySt);
                    i ++;
                }

                map = new GMaps({
                    div: '#map',
                    lat: 55,
                    lng: 37,
                    zoom: 3,
                    click: function(e){
                        console.log(e);
                    }
                });

                for(i = 0; i < namesStations.length; i ++) {
                    map.drawOverlay({
                        lat: coordinats[i][0],
                        lng: coordinats[i][1],
                        content: '<div class="overlay">'+namesStations[i]+'</div>'
                    });
                    map.addMarker({
                        lat: coordinats[i][0],
                        lng: coordinats[i][1],
                        title: namesStations[i],
                        click: function(marker) {
                            if(station == 1) {
                                station = 2;
                                $('#startStation').val(marker.title);
                            }
                            else {
                                station = 1;
                                $('#endStation').val(marker.title);
                            }
                        }
                    });

                }
            }
        }
    });
    $(document).on('submit', '.edit_marker', function(e) {
        alert("fdsfs");

});

});




$(document).ready(function() {

    $('#startStation').autocomplete({
        serviceUrl: '/Trains/ticket/getStations',
        paramName: "stationName",
        delimiter: ",",
        transformResult: function(response) {
            return {
                //must convert json to javascript object before process
                suggestions: $.map($.parseJSON(response), function(item) {
                    return { value: item.name, data: item.id };
                })

            };

        }
    });

});

$(document).ready(function() {

    $('#endStation').autocomplete({
        serviceUrl: '/Trains/ticket/getStations',
        paramName: "stationName",
        delimiter: ",",
        transformResult: function(response) {

            return {
                //must convert json to javascript object before process
                suggestions: $.map($.parseJSON(response), function(item) {

                    return { value: item.name, data: item.id };
                })

            };

        }

    });

});

$(document).ready(function () {
    $("#ticket").validate({
        rules: {
            startStation: {
                required: true,
                minlength: 2,
                maxlength: 50
            },
            endStation: {
                required: true,
                minlength: 2,
                maxlength: 50
            },
            dateDeparture: {
                required: true
            }
        },
        messages: {
            startStation: {
                required: "It`s field required",
                minlength: "Minimum 2 symbols",
                maxlength: "Maximum 50 symbols"
            },
            endStation: {
                required: "It`s field required",
                minlength: "Minimum 2 symbols",
                maxlength: "Maximum 50 symbols"
            },
            dateDeparture: {
                required: "It`s field required"
            }
        }
    });
});
