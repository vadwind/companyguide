/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 30.04.14
 * Time: 22:40
 * To change this template use File | Settings | File Templates.
 */
var map;
var panorama;
var isMap;
$(document).ready(function(){
    var lat = $('#lat').text();
    var lng = $('#lng').text();
    isMap = 1;

    map = new GMaps({
        div: '#map',
        zoom: 18,
        lat: lat,
        lng: lng
    });

    map.addMarker({
        lat: $('#lat').text(),
        lng: $('#lng').text()
    });

    $('#panBut').click(function () {
        if(isMap == 1) {
            isMap = 0;
            $('#panBut').text("View map");
            panorama = GMaps.createPanorama({
                el: '#map',
                lat : lat,
                lng : lng
            });
        }
        else {
            isMap = 1;
            $('#panBut').text("View panorama");
            map = new GMaps({
                div: '#map',
                zoom: 18,
                lat: lat,
                lng: lng
            });

            map.addMarker({
                lat: $('#lat').text(),
                lng: $('#lng').text()
            });
        }

    });
});


