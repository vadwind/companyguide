/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 30.04.14
 * Time: 20:56
 * To change this template use File | Settings | File Templates.
 */
var map;
$(document).ready(function(){
    var lat = $('#lat').val();
    var lng = $('#lng').val();
    var zoom = 18;
    if(lat == '' && lng == '') {
        var lat = 55.759450;
        var lng = 37.561437;
        zoom = 4;
    }
    map = new GMaps({
        div: '#map',
        zoom: zoom,
        lat: lat,
        lng: lng,
        click: function(e){
            var lat = e.latLng.lat();
            var lng = e.latLng.lng();
            $('#lat').val(lat);
            $('#lng').val(lng);

            map.removeMarkers();
            map.addMarker({
                lat: lat,
                lng: lng
            });

        }
    });
    if($('#lat').val() != '' && $('#lng').val() != '') {
        map.addMarker({
            lat: $('#lat').val(),
            lng: $('#lng').val()
        });
    }
});

$(document).ready(function(){
    $('#geocoding_form').submit(function(e){
        e.preventDefault();
        GMaps.geocode({
            address: $('#address').val().trim(),
            callback: function(results, status){
                if(status=='OK'){
                    var latlng = results[0].geometry.location;
                    map.setCenter(latlng.lat(), latlng.lng());
                    map.addMarker({
                        lat: latlng.lat(),
                        lng: latlng.lng()
                    });
                    $('#lat').val(latlng.lat());
                    $('#lng').val(latlng.lng());
                }
            }
        });
    });
});