<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 25.04.14
  Time: 9:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
    <title>Train admin</title>
</head>
<body>
<c:set var="activeMenu" value="admin" />
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">
    <h1>Manage of user ${user.surname} ${user.name}</h1>

    <form:form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/user/updateRole" commandName="user">
        <div class="control-group">
            <form:label path="login" class="control-label">Login</form:label>
            <div class="controls">
                <form:input path="login" readonly="true"/>
            </div>
        </div>

        <div class="control-group">
            <form:label path="surname" class="control-label">Surname</form:label>
            <div class="controls">
                <form:input path="surname" readonly="true"/>
            </div>
        </div>

        <div class="control-group">
            <form:label path="name" class="control-label">Name</form:label>
            <div class="controls">
                <form:input path="name" readonly="true"/>
            </div>
        </div>

        <div class="control-group">
            <form:label path="email" class="control-label">Email</form:label>
            <div class="controls">
                <form:input path="email" readonly="true"/>
            </div>
        </div>

        <div class="control-group">
            <form:label path="role" class="control-label">Change role</form:label>
            <div class="controls">
                <form:select path="role">
                    <option>ROLE_CLIENT</option>
                    <option>ROLE_ADMIN</option>
                </form:select>
            </div>
        </div>

        <form:hidden path="id"/>

        <div class="control-group">
            <div class="controls">
                <input type="submit" value="Save" class="btn btn-info"/>
            </div>
        </div>

    </form:form>
    <c:if test="${!empty ticketsList}">
        <h3>Tickets</h3>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Train</th>
                <th>Start station</th>
                <th>End station</th>
                <th>Departure date and time</th>
                <th>Arrive date and time</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${ticketsList}" var="ticket">
                <tr>
                    <td><a href="/Trains/train/view/${ticket.trainId}">${ticket.train} ${ticket.startStationTrain} - ${ticket.endStationTrain}</a></td>
                    <td>${ticket.startStation}</td>
                    <td>${ticket.endStation}</td>
                    <td>${ticket.dateDeparture} ${ticket.timeDeparture}</td>
                    <td>${ticket.dateArrive} ${ticket.timeArrive}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
</body>
</html>