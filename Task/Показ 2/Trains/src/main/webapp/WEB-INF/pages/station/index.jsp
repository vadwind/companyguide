<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 15.04.14
  Time: 20:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
    <title>Stations</title>
</head>
<body>
<c:set var="activeMenu" value="station" />
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">
    <h1>Stations</h1>
    <h3>List of stations</h3>
    <c:if test="${!empty stationList}">
        <table id="tableSmart" class="table table-hover">
            <thead>
            <tr>
                <th>Name</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${stationList}" var="station">
                <tr>
                    <td>${station.name}</td>
                    <td><div class="span4 offset4"></div></div><a href="${pageContext.request.contextPath}/station/view/${station.id}"><button class="btn btn-warning">View timetable</button></a></div></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
</body>
</html>