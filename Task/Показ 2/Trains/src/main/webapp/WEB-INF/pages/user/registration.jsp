<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 13.04.14
  Time: 17:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf8">
        <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/userForm.js"></script>
        <title>Registration of user</title>
    </head>
    <body>
    <%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
    <div class="container">
        <h1>Registration of user</h1>
        <form:form method="post" action="add" commandName="user" class="form-horizontal">
            <div class="control-group">
                <form:label path="login" class="control-label">Login</form:label>
                <div class="controls">
                    <form:input path="login"/>
                </div>
            </div>

            <div class="control-group">
                <form:label path="password" class="control-label">Password</form:label>
                <div class="controls">
                    <form:input path="password" type="password"/>
                </div>
            </div>

            <div class="control-group">
                <form:label path="repeatPassword" class="control-label">Repeat password</form:label>
                <div class="controls">
                    <form:input path="repeatPassword" type="password"/>
                </div>
            </div>

            <div class="control-group">
                <form:label path="email" class="control-label">Email</form:label>
                <div class="controls">
                    <form:input path="email"/>
                </div>
            </div>

            <div class="control-group">
                <form:label path="surname" class="control-label">Surname</form:label>
                <div class="controls">
                    <form:input path="surname" />
                </div>
            </div>

            <div class="control-group">
                <form:label path="name" class="control-label">Name</form:label>
                <div class="controls">
                    <form:input path="name" />
                </div>
            </div>

            <div class="control-group">
                <form:label path="birthday" class="control-label">Birthday</form:label>
                <div class="controls">
                    <form:input path="birthday" type="date"/>
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <input type="submit" value="Save" class="btn btn-info"/>
                </div>
            </div>

        </form:form>
        <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
    </div>
    </body>
</html>