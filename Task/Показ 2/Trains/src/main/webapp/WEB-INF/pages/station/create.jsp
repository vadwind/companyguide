<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 13.04.14
  Time: 23:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/addStation.js"></script>
    <title>Add station</title>
</head>
<body>
<c:set var="activeMenu" value="admin" />
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">
    <div class="row">
        <h1>Create station</h1>
    </div>
    <div class="row">
        <div class="span3">
                    <form:form class="form-signin" method="post" action="add" commandName="station">
                        <form:label class="control-label" path="name">Name</form:label>
                        <form:input class="input-block-level" path="name" />
                        <form:label class="control-label" path="lat">Lat</form:label>
                        <form:input class="input-block-level" path="lat" readonly="true"/>
                        <form:label class="control-label" path="lng">Lng</form:label>
                        <form:input class="input-block-level" path="lng" readonly="true" />
                        <input class="btn btn-info" type="submit" value="Save" />
                    </form:form>
         </div>


        <div class="span9">
            <form id="geocoding_form" class="form-search">
                <input type="text" id="address" name="address" class="input-medium search-query">
                <button type="submit" class="btn">Search</button>
            </form>
            <div class="popin">
                <div id="map"></div>
            </div>
        </div>
    </div>

    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
</body>
</html>