<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 22.04.14
  Time: 22:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/userForm.js"></script>
    <title>Profile of user ${user.surname} ${user.name})</title>
</head>
<body>
<div class="container">
    <%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
    <h2>Profile of user ${user.surname} ${user.name}</h2>
    <form:form method="post" action="${pageContext.request.contextPath}/user/update" commandName="user" class="form-horizontal">
        <div class="control-group">
            <form:label path="login" class="control-label">Login</form:label>
            <div class="controls">
                <form:input path="login" readonly="true"/>
            </div>
        </div>

        <div class="control-group">
            <form:label path="password" class="control-label">Password</form:label>
            <div class="controls">
                <form:input path="password" type="password"/>
            </div>
        </div>

        <div class="control-group">
            <form:label path="repeatPassword" class="control-label">Repeat password</form:label>
            <div class="controls">
                <form:input path="repeatPassword" type="password"/>
            </div>
        </div>

        <div class="control-group">
            <form:label path="email" class="control-label">Email</form:label>
            <div class="controls">
                <form:input path="email"/>
            </div>
        </div>

        <div class="control-group">
            <form:label path="surname" class="control-label">Surname</form:label>
            <div class="controls">
                <form:input path="surname" />
            </div>
        </div>

        <div class="control-group">
            <form:label path="name" class="control-label">Name</form:label>
            <div class="controls">
                <form:input path="name" />
            </div>
        </div>

        <div class="control-group">
            <form:label path="dateBirthday" class="control-label">Birthday</form:label>
            <div class="controls">
                <form:input path="dateBirthday" type="date"/>
            </div>
        </div>
        <form:hidden path="id"/>

        <div class="control-group">
            <div class="controls">
                <input type="submit" value="Save" class="btn btn-info"/>
            </div>
        </div>

    </form:form>
    <a href="<c:url value="/user/tickets/${user.id}" />"><button class="btn btn-large btn-warning">My tickets</button></a>
    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
</body>
</html>