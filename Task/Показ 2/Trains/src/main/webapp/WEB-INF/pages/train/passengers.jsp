<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 17.04.14
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Train ${train.trainNumber}</title>
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
</head>
<body>
<c:set var="activeMenu" value="admin" />
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">
    <h1>Train ${train.trainNumber}</h1>
    <h3>Passengers</h3>
    <c:if test="${!empty passengersList}">
        <table id="tableSmart" class="table table-hover">
            <thead>
            <tr>
                <th>Passenger</th>
                <th>Birthday</th>
                <th>Train</th>
                <th>Start station</th>
                <th>End station</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${passengersList}" var="passenger">
                <tr>
                    <td>${passenger.surname} ${passenger.name}</td>
                    <td>${passenger.birthday}</td>
                    <td>${passenger.train}</td>
                    <td><a href="/Trains/station/view/name=${passenger.startStation}">${passenger.startStation}</a></td>
                    <td><a href="/Trains/station/view/name=${passenger.endStation}">${passenger.endStation}</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
</body>
</html>