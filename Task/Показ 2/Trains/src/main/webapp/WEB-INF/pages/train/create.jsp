<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 17.04.14
  Time: 21:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <title>Add train</title>
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/addTrain.js"></script>
</head>
<body>
<c:set var="activeMenu" value="admin" />
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">
    <h1>Add train</h1>
    <form:form method="post" action="add" commandName="train" class="form-horizontal">
        <div class="control-group">
            <form:label path="trainNumber" class="control-label">Number of train</form:label>
            <div class="controls">
                <form:input path="trainNumber" />
            </div>
        </div>
        <div class="control-group">
            <form:label path="countPassengers" class="control-label">Count passengers</form:label>
            <div class="controls">
                <form:input path="countPassengers" />
            </div>
        </div>

        <div class="control-group">
            <div class="controls">
                <input class="btn" type="submit" value="Save" />
            </div>
        </div>

    </form:form>

    <h3>Timetable</h3>
    <button class="btn btn-info" id="addstation">Add station</button>
    <button class="btn btn-danger" id="clearTimetable">Clear timetable</button>

    <table class="table table-hover" class="timetableList" id="timetableList">
        <thead>
            <tr>
                <th>Station</th>
                <th>Date</th>
                <th>Time arrive</th>
                <th>Time departure</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="bodyTable">
            <c:forEach items="${timetableList}" var="timetable">
                <tr>
                    <td>${timetable.station}</td>
                    <td>${timetable.date}</td>
                    <td>${timetable.timeArrive}</td>
                    <td>${timetable.timeDeparture}</td>
                    <td><button class="btn btn-danger" onclick="deleteStation('${timetable.station}')">Delete</button></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>

<div id="myModal" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Add station</h3>
    </div>
    <div class="modal-body">
        <p>
            <form id="station" action="add" method="POST">
                <label for="modstation">Select station</label>
                <select id="modstation" name="station" required/>
                <c:forEach items="${stationList}" var="station">
                    <option>${station.name}</option>
                </c:forEach>
                </select>
                <label for="moddate">Date</label>
                <input type="date" id="moddate" name="date" required/>
                <label for="modtimearrive">Time arrive</label>
                <input type="time" id="modtimearrive" name="timearrive" required/>
                <label for="modtimedeparture">Time departure</label>
                <input type="time" id="modtimedeparture" name="timedeparture" required/>
            </form>
        </p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button id="savestation" class="btn btn-primary">Save changes</button>
    </div>
</div>

<div id="errorMyModal" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Message</h3>
    </div>
    <div id="errorModalBody" class="modal-body">
    </div>
    <div class="modal-footer">
        <button id="errorButton" class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>

</body>
</html>