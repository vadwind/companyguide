<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 24.04.14
  Time: 14:10
  To change this template use File | Settings | File Templates.
--%>
<div class="popin">
    <h2>Search train</h2>
    <form:form class="form-signin" method="post" action="${pageContext.request.contextPath}/ticket/searchTrain" commandName="ticket">
        <form:label class="control-label" path="startStation">Departure station</form:label>
        <form:input class="input-block-level" path="startStation" />

        <form:label class="control-label" path="endStation">Arrive station</form:label>
        <form:input class="input-block-level" path="endStation" />

        <form:label class="control-label" path="dateDeparture">Date</form:label>
        <form:input class="input-block-level" path="dateDeparture" type="date"/>

        <input class="btn btn-my" type="submit" value="Search" />
    </form:form>
</div>