<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 24.04.14
  Time: 0:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
    <title>Buy ticket</title>
</head>
<body>
<c:set var="activeMenu" value="search" />
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">
    <h2>Buy ticket</h2>
    <c:if test="${!empty ticket && !empty train}">
        <h3>Information about ticket</h3>
        <table class="table table-hover">
            <tr>
                <th>Train</th>
                <th>Departure station</th>
                <th>Departure date and time</th>
                <th>Arrive station</th>
                <th>Arrive date and time</th>
            </tr>
            <tr>
                <td><a href="/Trains/train/view/${train.trainId}">${train.train} ${train.startStationTrain} - ${train.endStationTrain}</a></td>
                <td>${ticket.startStation}</td>
                <td>${train.dateDeparture} ${train.timeDeparture}</td>
                <td>${ticket.endStation}</td>
                <td>${train.dateArrive} ${train.timeArrive}</td>
            </tr>
        </table>
    </c:if>

    <c:if test="${!empty user}">
        <h3>Information about passenger</h3>
        <table class="table table-hover">
            <tr>
                <th>Surname</th>
                <th>Name</th>
                <th>Birthday</th>
            </tr>
            <tr>
                <td>${user.surname}</td>
                <td>${user.name}</td>
                <td>${user.birthday}</td>
            </tr>
        </table>
    </c:if>

    <form action="${pageContext.request.contextPath}/ticket/buyTicket" method="post">
        <div class="row">
            <div class="span2 offset10">
                <input class="btn btn-large btn-info" type="submit" value="Buy ticket">
            </div>
        </div>
    </form>

    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
</body>
</html>