<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 21.04.14
  Time: 21:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/userForm.js"></script>
    <title>Log in</title>
</head>
<body>
    <%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
    <div class="container">
        <div class="row">
            <div class="span4 offset1">
                <h2>Authentication</h2>
            </div>
        </div>
        <form method="POST" class="form-horizontal" action="<c:url value="/j_spring_security_check" />">
            <div class="control-group">
                <label class="control-label" for="login">Login</label>
                <div class="controls">
                    <input id="login" type="text" name="j_username" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="login">Password</label>
                <div class="controls">
                    <input id="password" type="password" name="j_password" />
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <label class="checkbox">
                        <input type="checkbox" name="_spring_security_remember_me" />  Remember
                    </label>
                    <input class="btn" type="submit" value="Log in" />
                </div>
            </div>
        </form>
        <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
    </div>
</body>
</html>