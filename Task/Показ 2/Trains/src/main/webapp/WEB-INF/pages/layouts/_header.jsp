<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 21.04.14
  Time: 23:26
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/pages/includes/_modal.jsp" %>

<div class="container">
    <a class="navbar-link" href="/Trains"><img src="/Trains/img/header.jpg" class="img-rounded bot-img"></a>
    <div class="navbar nav-top">
        <div class="navbar-inner">
            <div class="container">
                <%--<p class="navbar-text pull-left">--%>
                    <%--<a class="brand" href="/Trains">Trains</a>--%>
                    <ul class="nav pull-left">
                        <li <c:if test="${activeMenu eq 'index'}">class="active"</c:if>><a class="navbar-link" href="/Trains">Home</a></li>
                        <li <c:if test="${activeMenu eq 'station'}">class="active"</c:if>><a class="navbar-link" href="/Trains/station/index">Timetable</a></li>
                        <li <c:if test="${activeMenu eq 'train'}">class="active"</c:if>><a class="navbar-link" href="/Trains/train/index">Trains</a></li>
                        <li <c:if test="${activeMenu eq 'search'}">class="active"</c:if>><a class="navbar-link" href="/Trains/ticket/search">Search ticket</a></li>
                        <sec:authorize ifAnyGranted="ROLE_ADMIN">
                             <li <c:if test="${activeMenu eq 'admin'}">class="active"</c:if>><a class="navbar-link" href="<c:url value="/admin" />">Administration panel</a></li>
                        </sec:authorize>
                    </ul>
                <%--</p>--%>
                <p class="navbar-text pull-right">
                    <c:choose>
                        <c:when test="${pageContext.request.userPrincipal.name != null}">
                            Hello: <a href="<c:url value="/user/view/user=${pageContext.request.userPrincipal.name}" />"
                               class="navbar-link"> ${pageContext.request.userPrincipal.name}</a> |
                            <a href="<c:url value="/user/logout" />" class="navbar-link"> Logout</a>
                        </c:when>
                        <c:otherwise>
                            <a href="<c:url value="/user/registration" />" class="navbar-link">Sign up</a> |
                            <a href="<c:url value="/user/login" />" class="navbar-link">Log in</a>
                        </c:otherwise>
                    </c:choose>
                </p>

            </div>
        </div>
    </div>
</div>