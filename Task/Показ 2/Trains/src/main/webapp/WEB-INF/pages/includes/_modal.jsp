<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 25.04.14
  Time: 17:20
  To change this template use File | Settings | File Templates.
--%>
<c:if test="${message != null}">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#myModal').modal('show');
        });

        $(document).ready(function () {
            $('#clsb2').click(function () {
                location.reload()
                $('#myModal').modal('hide');
            });
        });
        $(document).ready(function () {
            $('#clsb1').click(function () {
                location.reload()
                $('#myModal').modal('hide');
            });
        });
    </script>
    <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button id="clsb2" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Message</h3>
        </div>
        <div class="modal-body" id="modal-body">
            <p>${message}</p>
        </div>
        <div class="modal-footer">
            <button id="clsb1" class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </div>
</c:if>