package com.tsystems.trains.util;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.tsystems.trains.DTO.TicketDTO;
import com.tsystems.trains.DTO.UserDTO;

import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 30.04.14
 * Time: 2:28
 * To change this template use File | Settings | File Templates.
 */

/**
 * Class for creating pdf
 */
public class CreatePDF {

    Document document = new Document(PageSize.A4, 50, 50, 50, 50);
    UserDTO user;
    TicketDTO ticket;

    /**
     * Create pdf in file for sending
     * @param user User
     * @param ticket Ticket
     */
    public CreatePDF(UserDTO user, TicketDTO ticket) {
        this.user = user;
        this.ticket = ticket;
        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("D:/T-Systems/pdf/ticket" + ticket.getId() + ".pdf"));
            document.open();
            addBody();
        } catch (Exception e) {
        }
        document.close();

    }

    /**
     * Create pdf in browse
     * @param user  User
     * @param ticket Ticket
     * @param response Response
     */
    public CreatePDF(UserDTO user, TicketDTO ticket, HttpServletResponse response) {
        this.user = user;
        this.ticket = ticket;
        try {
            PdfWriter.getInstance(document, response.getOutputStream());
            document.open();
            addBody();
        } catch (Exception e) {
        }
        document.close();

    }


    /**
     * Add body in pdf
     * @throws DocumentException
     * @throws IOException
     */
    private void addBody() throws DocumentException, IOException {
        //image
        Image icon = Image.getInstance(new URL("http://localhost:8080/Trains/img/tsystems.png"));
        document.add(icon);

        //ticket number
        document.add(new Paragraph("Ticket number: " + ticket.getTrain(),
                FontFactory.getFont(FontFactory.TIMES_ROMAN, 25, Font.BOLD, new CMYKColor(0, 0, 0, 255))));

        //user data
        document.add(new Paragraph("User data",
                FontFactory.getFont(FontFactory.COURIER, 25, Font.BOLD, new CMYKColor(0, 0, 0, 255))));

        //table with user data
        PdfPTable userTable = new PdfPTable(3);
        userTable.setSpacingBefore(25);
        userTable.setSpacingAfter(25);
        PdfPCell c1 = new PdfPCell(new Phrase("Surname"));
        userTable.addCell(c1);
        PdfPCell c2 = new PdfPCell(new Phrase("Name"));
        userTable.addCell(c2);
        PdfPCell c3 = new PdfPCell(new Phrase("Birthday"));
        userTable.addCell(c3);
        userTable.addCell(user.getSurname());
        userTable.addCell(user.getName());
        userTable.addCell(user.getBirthday());
        document.add(userTable);

        //train information
        document.add(new Paragraph("Train information",
                FontFactory.getFont(FontFactory.COURIER, 25, Font.BOLD, new CMYKColor(0, 0, 0, 255))));
        document.add(new Paragraph("Train number - " + ticket.getTrain() + ".  " + ticket.getStartStationTrain()
                + " - " + ticket.getEndStationTrain(),
                FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, Font.BOLD, new CMYKColor(0, 0, 0, 255))));

        //ticket information
        document.add(new Paragraph("Ticket information",
                FontFactory.getFont(FontFactory.COURIER, 25, Font.BOLD, new CMYKColor(0, 0, 0, 255))));
        //table with user data
        PdfPTable ticketTable = new PdfPTable(4);
        ticketTable.setSpacingBefore(25);
        ticketTable.setSpacingAfter(250);
        c1 = new PdfPCell(new Phrase("Station departure"));
        ticketTable.addCell(c1);
        c2 = new PdfPCell(new Phrase("Time departure"));
        ticketTable.addCell(c2);
        c3 = new PdfPCell(new Phrase("Station arrive"));
        ticketTable.addCell(c3);
        PdfPCell c4 = new PdfPCell(new Phrase("Time arrive"));
        ticketTable.addCell(c4);

        ticketTable.addCell(ticket.getStartStation());
        ticketTable.addCell(ticket.getDateDeparture() + " " + ticket.getTimeDeparture());
        ticketTable.addCell(ticket.getEndStation());
        ticketTable.addCell(ticket.getDateArrive() + " " + ticket.getTimeArrive());
        document.add(ticketTable);
        //Have a good trip!
        document.add(new Paragraph("Have a good trip, " + user.getName() + "!",
                FontFactory.getFont(FontFactory.COURIER_BOLD, 25, Font.BOLD, new CMYKColor(0, 255, 0, 0))));
        icon = Image.getInstance(new URL("http://localhost:8080/Trains/img/pdf.jpg"));
        document.add(icon);
    }
}
