package com.tsystems.trains.dao;

import com.tsystems.trains.entity.Station;
import com.tsystems.trains.entity.Train;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 15.04.14
 * Time: 21:32
 * To change this template use File | Settings | File Templates.
 */

/**
 * Class implements DAO for train
 */
@Repository
public class TrainDAO {
    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Select all stations
     * @return List of stations
     */
    public List<Train> selectAllRecords() {
        return sessionFactory.getCurrentSession().createQuery("from Train").list();
    }

    /**
     * Search train by it id
     * @param trainId Id
     * @return Train
     */
    public Train findByPk(Long trainId) {
        List<Train> trains = sessionFactory.getCurrentSession().createQuery("from Train where id=" + trainId).list();
        if(!trains.isEmpty()) {
            return trains.get(0);
        }
        return null;
    }

    /**
     * Search train by it trainNumber
     * @param trainNumber Train Number
     * @return Train
     */
    public Train findByTrainNumber(String trainNumber) {
        List<Train> trains = sessionFactory.getCurrentSession().createQuery("from Train where trainNumber='" + trainNumber +"'").list();
        if(!trains.isEmpty()) {
            return trains.get(0);
        }
        return null;
    }

    /**
     * Delete record
     * @param train Train entity
     */
    public void delete(Train train) {
        sessionFactory.getCurrentSession().delete(train);
    }

    /**
     * Update record
     * @param train Train entity
     */
    public void update(Train train) {
        sessionFactory.getCurrentSession().update(train);
    }

    /**
     * Insert new record
     * @param train Train entity
     */
    public void insert(Train train) {
        sessionFactory.getCurrentSession().persist(train);
    }

    /**
     * Check train
     * @param train Train entity
     * @return true if train there
     */
    public boolean isTrain(Train train) {
        List<Train> trains = sessionFactory.getCurrentSession().createQuery("from Train where trainNumber='" + train.getTrainNumber()+"'").list();
        if(!trains.isEmpty()) {
            return true;
        }
        return false;
    }


}
