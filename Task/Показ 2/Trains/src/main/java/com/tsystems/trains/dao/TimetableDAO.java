package com.tsystems.trains.dao;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 14.04.14
 * Time: 0:35
 * To change this template use File | Settings | File Templates.
 */

import com.tsystems.trains.entity.Station;
import com.tsystems.trains.entity.Timetable;
import com.tsystems.trains.entity.Train;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Class implements DAO for User
 */
@Repository
public class TimetableDAO {
    @Autowired
    private SessionFactory sessionFactory;


    /**
     * check is station in train
     * @param station
     * @return
     */
    public boolean isStationInTrains(Station station) {
        List<Timetable> timetables = sessionFactory.getCurrentSession().createQuery("from Timetable where station_id=" + station.getId()).list();
        if(!timetables.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Get trains on station
     * @param stationId
     * @return
     */
    public List<Timetable> selectTrainsOnStations(Long stationId) {
        List<Timetable> timetables = sessionFactory.getCurrentSession().createQuery("from Timetable where station_id=" + stationId + " order by date asc").list();
        return timetables;
    }

    /**
     * Insert new record
     * @param timetable Timetable entity
     */
    public void insert(Timetable timetable) {
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();
        sessionFactory.getCurrentSession().persist(timetable);
    }

    /**
     * Get Station by train and station
     * @param trainId  Id of train
     * @param stationId Id of station
     * @return
     */
    public Timetable getStationByTrainAndStation(Long trainId, Long stationId) {
        List<Timetable> timetables = sessionFactory.getCurrentSession().createQuery("from Timetable where station_id=" + stationId + " AND train_id=" + trainId).list();
        if(!timetables.isEmpty()) {
            return timetables.get(0);
        }
        return null;
    }

    /**
     * Search trains on station of departure in date
     * @param stationDepartureId  Id of station
     * @param date Date
     * @return list of timetables
     */
    public List<Timetable> searchTrainsOnStationOfDeparture(Long stationDepartureId, String date) {
        List<Timetable> timetables = sessionFactory.getCurrentSession().createQuery("from Timetable where station_id=" + stationDepartureId + " AND DATE_FORMAT(date,'%Y-%m-%d')='" + date +"'").list();
        return timetables;
    }
}
