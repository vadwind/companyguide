package com.tsystems.trains.DTO;

import com.tsystems.trains.entity.Station;
import com.tsystems.trains.entity.Timetable;
import com.tsystems.trains.entity.Train;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 16.04.14
 * Time: 19:46
 * To change this template use File | Settings | File Templates.
 */

/**
 * Class DTO for timetable
 */
public class TimetableDTO implements Serializable {

    private long id;
    private String station;
    private String train;
    private String date;
    private String timeArrive;
    private String timeDeparture;
    private int vacancies;
    private int numberSequence;

    /**
     * Default constructor
     */
    public TimetableDTO() {

    }

    /**
     * Constructor by entity class
     * @param timetable
     */
    public TimetableDTO(Timetable timetable) {
        this.id = timetable.getId();
        this.station = timetable.getStation().getName();
        this.train = timetable.getTrain().getTrainNumber();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        this.date = dateFormat.format(timetable.getDate());
        this.timeArrive = timetable.getTimeArrive().toString();
        this.timeDeparture = timetable.getTimeDeparture().toString();
        this.vacancies = timetable.getVacancies();
        this.numberSequence = timetable.getNumberSequence();

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getTrain() {
        return train;
    }

    public void setTrain(String train) {
        this.train = train;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeDeparture() {
        return timeDeparture;
    }

    public void setTimeDeparture(String timeDeparture) {
        this.timeDeparture = timeDeparture;
    }

    public String getTimeArrive() {
        return timeArrive;
    }

    public void setTimeArrive(String timeArrive) {
        this.timeArrive = timeArrive;
    }

    public int getVacancies() {
        return vacancies;
    }

    public void setVacancies(int vacancies) {
        this.vacancies = vacancies;
    }

    public int getNumberSequence() {
        return numberSequence;
    }

    public void setNumberSequence(int numberSequence) {
        this.numberSequence = numberSequence;
    }
}
