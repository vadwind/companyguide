package com.tsystems.trains.entity;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 24.03.14
 * Time: 21:33
 * To change this template use File | Settings | File Templates.
 */

/**
 * Class User
 */

@Entity
@Table(name = "user")
public class User implements Serializable {

    @Id
    private long id;
    private String role;

    @NotEmpty
    @Size(min=2, max=50)
    private String login;
    @NotEmpty
    @Size(min=2, max=50)
    private String password;
    @NotEmpty
    @Size(min=2, max=50)
    private String surname;
    @NotEmpty
    @Size(min=2, max=50)
    private String name;
    @NotNull
    @Past
    private Date birthday;
    @NotEmpty
    @Email
    @Size(min=2, max=50)
    private String email;

    private static final long serialVersionUID = 1L;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
