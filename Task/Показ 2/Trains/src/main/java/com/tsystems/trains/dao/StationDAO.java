package com.tsystems.trains.dao;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 13.04.14
 * Time: 22:56
 * To change this template use File | Settings | File Templates.
 */

import com.tsystems.trains.entity.Station;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Class implements DAO for station
 */
@Repository
public class StationDAO {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Insert new record
     * @param station Station entity
     */
    public void insert(Station station) {
        sessionFactory.getCurrentSession().persist(station);
    }

    /**
     * Delete record
     * @param station Station entity
     */
    public void delete(Station station) {
        sessionFactory.getCurrentSession().delete(station);
    }

    /**
     * Update record
     * @param station Station entity
     */
    public void update(Station station) {
        sessionFactory.getCurrentSession().update(station);
    }

    /**
     * Search station by it id
     * @param stationId Id
     * @return Station
     */
    public Station findByPk(Long stationId) {
        List<Station> stations = sessionFactory.getCurrentSession().createQuery("from Station where id=" + stationId).list();
        if(!stations.isEmpty()) {
            return stations.get(0);
        }
        return null;
    }

    /**
     * Search station by it name
     * @param name
     * @return Station
     */
    public Station findByName(String name) {
        List<Station> stations = sessionFactory.getCurrentSession().createQuery("from Station where name='" + name + "'").list();
        if(!stations.isEmpty())
            return stations.get(0);
        return null;
    }


    /**
     * Check station
     * @param station Station entity
     * @return true if station there
     */
    public boolean isStation(Station station) {
        List<Station> stations = sessionFactory.getCurrentSession().createQuery("from Station where name='" + station.getName()+"'").list();
        if(!stations.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Select all stations
     * @return List of stations
     */
    public List<Station> selectAllRecords() {
        return sessionFactory.getCurrentSession().createQuery("from Station").list();
    }

    /**
     * Select all sort stations
     * @return List of stations
     */
    public List<Station> selectAllSortRecords() {
        return sessionFactory.getCurrentSession().createQuery("from Station order by name asc").list();
    }
}
