package com.tsystems.trains.DTO;

import com.tsystems.trains.entity.Train;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.text.SimpleDateFormat;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 15.04.14
 * Time: 22:52
 * To change this template use File | Settings | File Templates.
 */

/**
 * Class DTO for Train
 */
public class TrainDTO  implements Serializable{
    private long id;

    @NotNull
    @Size(min=1, max=10)
    private String trainNumber;

    @NotNull @Min(1) @Max(5000)
    private int countPassengers;

    private String startStation;
    private String endStation;
    private String date;

    private String timeArrive;
    private String timeDeparture;

    /**
     * Default constructor
     */
    public TrainDTO() {

    }

    /**
     * Constructor by entity class
     * @param train
     */
    public TrainDTO(Train train) {
        this.id = train.getId();
        this.trainNumber = train.getTrainNumber();
        this.countPassengers = train.getCountPassengers();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        this.date = dateFormat.format(train.getStations().get(0).getDate());
        this.startStation = train.getStations().get(0).getStation().getName();
        this.endStation = train.getStations().get(train.getStations().size() - 1).getStation().getName();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public int getCountPassengers() {
        return countPassengers;
    }

    public void setCountPassengers(int countPassengers) {
        this.countPassengers = countPassengers;
    }

    public String getStartStation() {
        return startStation;
    }

    public void setStartStation(String startStation) {
        this.startStation = startStation;
    }

    public String getEndStation() {
        return endStation;
    }

    public void setEndStation(String endStation) {
        this.endStation = endStation;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeArrive() {
        return timeArrive;
    }

    public void setTimeArrive(String timeArrive) {
        this.timeArrive = timeArrive;
    }

    public String getTimeDeparture() {
        return timeDeparture;
    }

    public void setTimeDeparture(String timeDeparture) {
        this.timeDeparture = timeDeparture;
    }

    /**
     * Get entity class train
     * @return entity class train
     */
    public Train getEntityObject() {
        Train train = new Train();
        train.setId(this.id);
        train.setTrainNumber(this.trainNumber);
        train.setCountPassengers(this.countPassengers);
        return train;
    }
}
