package com.tsystems.trains.dao;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 16.04.14
 * Time: 1:04
 * To change this template use File | Settings | File Templates.
 */

import com.tsystems.trains.DTO.TicketDTO;
import com.tsystems.trains.entity.*;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Class implements DAO for ticket
 */
@Repository
public class TicketDAO {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Search ticket by train
     * @param trainId Id
     * @return Train
     */
    public List<Ticket> findByTrain(Long trainId) {
        List<Ticket> tickets = sessionFactory.getCurrentSession().createQuery("from Ticket where train_id=" + trainId).list();
        return tickets;
    }

    /**
     * Insert new record
     * @param ticket Ticket entity
     */
    public void insert(Ticket ticket) {
        sessionFactory.getCurrentSession().persist(ticket);
    }

    /**
     * Search tickets by user
     * @param user User
     * @return List of tickets
     */
    public List<Ticket> findByUser(User user) {
        List<Ticket> tickets = sessionFactory.getCurrentSession().createQuery("from Ticket where user_id=" + user.getId()).list();
        return tickets;
    }

    /**
     * Search tickets by user and train
     * @param user User
     * @return List of tickets
     */
    public Ticket findByUserAndTrain(User user, Long trainId) {
        List<Ticket> tickets = sessionFactory.getCurrentSession().createQuery("from Ticket where user_id=" + user.getId() + " AND train_id=" + trainId).list();
        if(!tickets.isEmpty())
            return tickets.get(0);
        return  null;
    }

    /**
     * Search tickets by id
     * @return List of tickets
     */
    public Ticket findByPk(Long ticketId) {
        List<Ticket> tickets = sessionFactory.getCurrentSession().createQuery("from Ticket where id=" + ticketId).list();
        if(!tickets.isEmpty())
            return tickets.get(0);
        return  null;
    }

    /**
     * Search trains
     * @param trainsOnStationOfDeparture  List of trains
     * @param stationArrive station of departure
     * @return List of trains
     */
    public List<TicketDTO>findTickets(List<Timetable> trainsOnStationOfDeparture, Station stationDeparture, Station stationArrive) {
        List<TicketDTO> resultTicketList = new ArrayList<TicketDTO>();

        for(Timetable trainTimetable : trainsOnStationOfDeparture) {
            Train train = trainTimetable.getTrain();
            //check station departure
            List<Timetable> stationsTrain = train.getStations();
            int vacancies = checkStationDepartureAndVacancies(stationsTrain, stationDeparture, stationArrive);

            if(vacancies > 0) {
                //set TicketDTO
                TicketDTO ticketDTO = new TicketDTO();
                //train
                ticketDTO.setTrain(train.getTrainNumber());
                ticketDTO.setTrainId(train.getId());
                ticketDTO.setStartStationTrain(train.getStations().get(0).getStation().getName());
                ticketDTO.setEndStationTrain(train.getStations().get(train.getStations().size() - 1).getStation().getName());

                //vacancies
                ticketDTO.setVacancies(new Long(vacancies));

                resultTicketList.add(ticketDTO);
            }
        }
        return resultTicketList;
    }

    /**
     * Check station arrive
     * @param stationsTrain Stations
     * @param stationArrive Station arrive
     * @param stationDeparture Station departure
     * @return true - if ok
     */
    private int checkStationDepartureAndVacancies(List<Timetable> stationsTrain, Station stationDeparture, Station stationArrive) {
        //check station arrive
        int sequenceDeparture = 0, sequenceArrive = 0;
        for(Timetable t: stationsTrain) {
            if(t.getStation().equals(stationDeparture))
                sequenceDeparture = t.getNumberSequence();
            if(t.getStation().equals(stationArrive))
                sequenceArrive = t.getNumberSequence();
        }

        //check vacancies
        if(sequenceArrive > sequenceDeparture) {
            List<Timetable> trains = sessionFactory.getCurrentSession().createQuery("from Timetable where train_id=" + stationsTrain.get(0).getTrain().getId()
                                                                                    + " AND vacancies > 0 AND number_sequence > " + (sequenceDeparture -1 )+
                                                                                    " AND number_sequence < " + sequenceArrive).list();

            if(trains.size() == (sequenceArrive - sequenceDeparture)) {
                int vacancies = trains.get(0).getVacancies();
                for(Timetable t: trains) {
                    if(t.getVacancies() < vacancies) {
                        vacancies = t.getVacancies();
                    }
                }
                return vacancies;
            }
        }
        return 0;
    }

    /**
     * Check is user in this train
     * @param train Train
     * @param user User
     * @return true - if user is in train
     */
    public boolean isUserInTrain(Train train, User user) {
        List<Ticket> tickets = sessionFactory.getCurrentSession().createQuery("from Ticket where train_id=" + train.getId()+" AND user_id=" + user.getId()).list();
        if(!tickets.isEmpty())
            return true;
        return false;
    }

    /**
     * Check ticket
     * @param stationDeparture Departure station
     * @param stationArrive  Arrive station
     * @param train  Train
     * @return true iа ok
     */
    public boolean isTicket(Station stationDeparture, Station stationArrive,
                             Train train) {
        //check station departure
        List<Timetable> stationsTrain = train.getStations();
        int vacancies = checkStationDepartureAndVacancies(stationsTrain, stationDeparture, stationArrive);
        if(vacancies > 0) {
            if(checkTime(stationDeparture, train))
                return true;
        }
        return false;
    }

    /**
     * Check time
     * @param stationDeparture station of arrive
     * @param train train
     * @return true if ok
     */
    private boolean checkTime(Station stationDeparture, Train train) {
        // get timetable
        Timetable station = (Timetable) sessionFactory.getCurrentSession().createQuery("from Timetable where station_id=" + stationDeparture.getId() + " AND train_id=" + train.getId()).list().get(0);
        DateTime nowDateTime = new DateTime();

        String[] dateArray;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateArray = dateFormat.format(station.getDate()).split("-");

        String[] timeDepartureArray;
        timeDepartureArray = station.getTimeDeparture().toString().split(":");

        DateTime dateTimeDeparture = new DateTime(
                Integer.parseInt(dateArray[0]),
                Integer.parseInt(dateArray[1]),
                Integer.parseInt(dateArray[2]),
                Integer.parseInt(timeDepartureArray[0]),
                Integer.parseInt(timeDepartureArray[1]),
                0);
        if(nowDateTime.toDate().after(dateTimeDeparture.toDate()))
            return false;

        Duration interval = new Duration(60*10*1000L);
        Duration duration = new Duration(nowDateTime, dateTimeDeparture);
        return !duration.isShorterThan(interval);
    }

    /**
     * Change vacancies
     * @param stationsTrain  All stations if train
     * @param stationArrive  station of arrive
     * @param stationDeparture station of departure
     */
    public void changeVacancies(List<Timetable> stationsTrain, Station stationDeparture, Station stationArrive) {
        //get station
        int sequenceArrive = 0, sequenceDeparture = 0;
        for(Timetable t: stationsTrain) {
            if(t.getStation().equals(stationDeparture))
                sequenceDeparture = t.getNumberSequence();
            if(t.getStation().equals(stationArrive))
                sequenceArrive = t.getNumberSequence();
        }

        List<Timetable> trains = sessionFactory.getCurrentSession().createQuery("from Timetable where train_id=" + stationsTrain.get(0).getTrain().getId()
                + " AND vacancies > 0 AND number_sequence > " + (sequenceDeparture -1 )+
                " AND number_sequence < " + sequenceArrive).list();
        for(Timetable stationItem: trains) {
            stationItem.setVacancies(stationItem.getVacancies() - 1);
            sessionFactory.getCurrentSession().update(stationItem);
        }
    }




}
