package com.tsystems.trains.controller;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 15.04.14
 * Time: 21:31
 * To change this template use File | Settings | File Templates.
 */

import com.google.gson.Gson;
import com.tsystems.trains.DTO.TimetableDTO;
import com.tsystems.trains.DTO.TrainDTO;
import com.tsystems.trains.entity.Station;
import com.tsystems.trains.service.StationService;
import com.tsystems.trains.service.TrainService;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Train controller
 */
@Controller
@RequestMapping("/train")
public class TrainController {

    @Autowired
    TrainService trainService;

    @Autowired
    StationService stationService;

    /**
     * Admin panel for station
     * @param model
     * @return
     */
    @RequestMapping("/admin")
    public String admin(HttpSession session, ModelMap model) {
        model.put("trainList", trainService.getAllTrains());
        if(session.getAttribute("message") != null) {
            model.put("message", session.getAttribute("message"));
            session.removeAttribute("message");
        }
        return "train/admin";
    }

    /**
     * Delete train
     * @param trainId
     * @return
     */
    @RequestMapping("/delete/{trainId}")
    public String delete(HttpSession session,
                         @PathVariable("trainId") Long trainId) {
        TrainDTO trainDTO = trainService.getTrain(trainId);
        if(trainDTO != null) {
            if(trainService.deleteTrain(trainId)) {
                session.setAttribute("message", "Train successfully been delete");
            }
            else {
                session.setAttribute("message", "Train didn`t been delete");
            }
            return "redirect:/train/admin";
        }
        return "error404";
    }

    /**
     * View train
     * @param trainId Train
     * @param model
     * @return
     */
    @RequestMapping("/view/{trainId}")
    public String view(@PathVariable("trainId") Long trainId,
                       ModelMap model) {
        TrainDTO trainDTO = trainService.getTrain(trainId);
        if(trainDTO != null) {
            model.put("train", trainDTO);
            model.put("timetableList", trainService.getTrainTimetable(trainId));
            return "train/view";
        }
        return "error404";
    }

    /**
     * Index for train
     * @param model
     * @return
     */
    @RequestMapping("/index")
    public String index(ModelMap model) {
        model.put("trainList", trainService.getAllTrains());
        return "train/index";
    }

    /**
     * Default for train
     * @return
     */
    @RequestMapping("/")
    public String home() {
        return "redirect:/train/index";
    }

    /**
     * Update train
     * @param trainId
     * @return
     */
    @RequestMapping("/update/{trainId}")
    public String update(HttpSession session,
                         @PathVariable("trainId") Long trainId,
                         ModelMap model) {
        TrainDTO trainDTO = trainService.getTrain(trainId);
        if(trainDTO != null) {
            if(session.getAttribute("message") != null) {
                model.put("message", session.getAttribute("message"));
                session.removeAttribute("message");
            }
            model.put("train", trainDTO);
            model.put("timetableList", trainService.getTrainTimetable(trainId));
            return "train/update";
        }
        return "error404";
    }

    /**
     * Refresh train
     * @param trainDTO  Train
     * @return  view
     */
    @RequestMapping(value = "/refresh", method = RequestMethod.POST)
    public String refreshStation(HttpSession session,
                                 @Valid @ModelAttribute("station")TrainDTO trainDTO,
                                 BindingResult result) {
        if(result.hasErrors()) {
            session.setAttribute("message", "Incorrect data. Train number must be from 1 to 10 symbols" +
                    "and number of passengers must from 1 to 5000");
            return "redirect:/train/update/"+trainDTO.getId();
        }
        if(trainService.updateTrain(trainDTO)) {
            session.setAttribute("message", "This train already exists");
            return "redirect:/train/update/"+trainDTO.getId();
        }
        return "redirect:/train/admin";
    }

    /**
     * Passengers of train
     * @param trainId Train
     * @param model
     * @return
     */
    @RequestMapping("/passengers/{trainId}")
    public String passengers(@PathVariable("trainId") Long trainId,
                       ModelMap model) {
        TrainDTO trainDTO = trainService.getTrain(trainId);
        if(trainDTO != null) {
            model.put("train", trainService.getTrain(trainId));
            model.put("passengersList", trainService.getPassengersTrain(trainId));
            return "train/passengers";
        }
        return "error404";
    }

    /**
     * Form for creating new train
     * @param model Model
     * @return  view
     */
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String create(HttpSession session, ModelMap model) {
        if(session.getAttribute("message") != null) {
            model.put("message", session.getAttribute("message"));
            session.removeAttribute("message");
        }
        if(session.getAttribute("timetableList") != null) {
            List<TimetableDTO> timetableList = (List<TimetableDTO>) session.getAttribute("timetableList");
            model.put("timetableList", timetableList);
        }
        model.put("stationList", stationService.getAllStations(true));
        model.put("train", new TrainDTO());
        return "train/create";
    }

    /**
     * Save of new
     * @return  view
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addTrain(HttpSession session, @Valid @ModelAttribute("train")TrainDTO train,
                           BindingResult result) {
        if(result.hasErrors()) {
            session.setAttribute("message", "Incorrect data. Train number must be from 1 to 10 symbols" +
                    "and number of passengers must from 1 to 5000");
            return "redirect:/train/create";
        }
        if(session.getAttribute("timetableList") != null) {
            List<TimetableDTO> timetableList = (List<TimetableDTO>) session.getAttribute("timetableList");
            if(timetableList.size() < 2) {
                session.setAttribute("message", "List of station must contains more 1");
                return "redirect:/train/create";
            }
            if(!trainService.addTrain(train.getEntityObject(), timetableList)) {
                session.setAttribute("message", "Incorrect data. Train number must be from 1 to 10 symbols " +
                        "and number of passengers must from 1 to 5000");
                return "redirect:/train/create";
            }
            session.setAttribute("message", "Train successfully been add");
            return "redirect:/train/admin";
        }
        session.setAttribute("message", "List of station is empty");
        return "redirect:/train/create";
    }

    /**
     * Add station ti list of stations of train
     * @param stationName
     * @param date
     * @param timeArrive
     * @param timeDeparture
     * @return
     */
    @RequestMapping(value = "/station", method = RequestMethod.POST)
    public @ResponseBody TimetableDTO addStation(HttpSession session,
                                                       @RequestParam String stationName,
                                                       @RequestParam String date,
                                                       @RequestParam String timeArrive,
                                                       @RequestParam String timeDeparture) {
        TimetableDTO timetableDTO = new TimetableDTO();
        String response = isCorrectStation(session, stationName, date, timeArrive, timeDeparture);
        if(response.equals("true")) {
            timetableDTO.setStation(stationName);
            timetableDTO.setDate(date);
            timetableDTO.setTimeArrive(timeArrive);
            timetableDTO.setTimeDeparture(timeDeparture);

            List<TimetableDTO> timetableList = new ArrayList<TimetableDTO>();
            if(session.getAttribute("timetableList") != null) {
                timetableList = (List<TimetableDTO>) session.getAttribute("timetableList");
            }
            timetableList.add(timetableDTO);
            session.setAttribute("timetableList", timetableList);
        }
        else
            timetableDTO.setDate(response);
        return timetableDTO;
    }

    /**
     * Clear timetable
     */

    @RequestMapping(value = "/cleartimetable", method = RequestMethod.POST)
    public @ResponseBody void clearTimetable(HttpSession session) {
        if(session.getAttribute("timetableList") != null) {
            session.removeAttribute("timetableList");
        }
    }

    @RequestMapping("/clearStation/{nameStation}")
    public String clearStation(HttpSession session, @PathVariable("nameStation") String nameStation) {
        if(session.getAttribute("timetableList") != null) {
            ArrayList<TimetableDTO> timetableList = (ArrayList<TimetableDTO>) session.getAttribute("timetableList");
            for(TimetableDTO timetableDTO : timetableList) {
                if(timetableDTO.getStation().equals(nameStation)) {
                    timetableList.remove(timetableDTO);
                    break;
                }
            }
            timetableList.trimToSize();
            if(timetableList.isEmpty())
                session.removeAttribute("timetableList");
            else
                session.setAttribute("timetableList", timetableList);
        }
        return "redirect:/train/create";
    }

    /**
     * Check station
     * @param stationName
     * @param date
     * @param timeArrive
     * @param timeDeparture
     * @return true if all are ok
     */
    private String isCorrectStation(HttpSession session,
                                     String stationName,
                                     String date,
                                     String timeArrive,
                                     String timeDeparture) {
        if(!checkRightStation(date, timeArrive, timeDeparture))
            return "Error date and time. Date and time must be later now. Stop time " +
                    "must be no more 10 hours";
        if(isStationInList(session, stationName))
            return "Such a station is already in the timetable";
        if(!correctPreviousStation(session, date, timeArrive))
            return "Date and time must be later than previous station in timetable. " +
                    "The time interval between two stations shouldn`t be more than a 3 days";
        return "true";
    }

    /**
     * Check station (empty, date, time)
     * @param date
     * @param timeArrive
     * @param timeDeparture
     * @return
     */
    private boolean checkRightStation(String date,
                                      String timeArrive,
                                      String timeDeparture) {
        if(date.isEmpty() || timeArrive.isEmpty() || timeDeparture.isEmpty())
            return false;

        String[] dateArray;
        dateArray = date.split("-");

        String[] timeArriveArray;
        timeArriveArray = timeArrive.split(":");

        String[] timeDepartureArray;
        timeDepartureArray = timeDeparture.split(":");

        DateTime nowDateTime = new DateTime();

        DateTime dateTimeArrive = new DateTime(
                                                Integer.parseInt(dateArray[0]),
                                                Integer.parseInt(dateArray[1]),
                                                Integer.parseInt(dateArray[2]),
                                                Integer.parseInt(timeArriveArray[0]),
                                                Integer.parseInt(timeArriveArray[1]),
                                                0);
        DateTime dateTimeDeparture = new DateTime(
                                                Integer.parseInt(dateArray[0]),
                                                Integer.parseInt(dateArray[1]),
                                                Integer.parseInt(dateArray[2]),
                                                Integer.parseInt(timeDepartureArray[0]),
                                                Integer.parseInt(timeDepartureArray[1]),
                                                0);

        if(nowDateTime.toDate().after(dateTimeArrive.toDate()))
            return false;
        if(dateTimeArrive.toDate().after(dateTimeDeparture.toDate()))
            return false;
        //check 10 hours
        Duration interval = new Duration(60*60*10*1000L);
        Duration duration = new Duration(dateTimeArrive, dateTimeDeparture);
        return duration.isShorterThan(interval);
    }

    /**
     * Check station in stationList
     * @param session
     * @param stationName
     * @return
     */
    private boolean isStationInList(HttpSession session, String stationName) {
        List<TimetableDTO> timetableList = new ArrayList<TimetableDTO>();
        if(session.getAttribute("timetableList") != null) {
            timetableList = (List<TimetableDTO>) session.getAttribute("timetableList");
            for(TimetableDTO timetableDTO : timetableList) {
                if(timetableDTO.getStation().equals(stationName))
                    return true;
            }
        }
        return false;
    }

    /**
     * Compare with previous station
     * @param session
     * @param date
     * @param timeArrive
     * @return
     */
    private boolean correctPreviousStation(HttpSession session,
                                            String date,
                                            String timeArrive) {
        String[] dateArray;
        dateArray = date.split("-");

        String[] timeArriveArray;
        timeArriveArray = timeArrive.split(":");

        DateTime dateTimeArrive = new DateTime(
                Integer.parseInt(dateArray[0]),
                Integer.parseInt(dateArray[1]),
                Integer.parseInt(dateArray[2]),
                Integer.parseInt(timeArriveArray[0]),
                Integer.parseInt(timeArriveArray[1]),
                0);

        List<TimetableDTO> timetableList = new ArrayList<TimetableDTO>();
        if(session.getAttribute("timetableList") != null) {
            timetableList = (List<TimetableDTO>) session.getAttribute("timetableList");
            TimetableDTO previvousStation = timetableList.get(timetableList.size() - 1);
            dateArray = previvousStation.getDate().split("-");
            String[] timeDepartureArray;
            timeDepartureArray = previvousStation.getTimeDeparture().split(":");

            DateTime dateTimePrevivousStation = new DateTime(
                            Integer.parseInt(dateArray[0]),
                            Integer.parseInt(dateArray[1]),
                            Integer.parseInt(dateArray[2]),
                            Integer.parseInt(timeDepartureArray[0]),
                            Integer.parseInt(timeDepartureArray[1]),
                            0);
            if(dateTimeArrive.toDate().before(dateTimePrevivousStation.toDate()))
                return false;
            Duration interval = new Duration(60*60*72*1000L);
            Duration duration = new Duration(dateTimePrevivousStation, dateTimeArrive);
            return duration.isShorterThan(interval);
        }
        return true;
    }

    /**
     * Get stations of train
     * @param trainId Id of train
     * @return array
     */
    @RequestMapping(value = "/getStationsCoord", method = RequestMethod.POST)
    public @ResponseBody String getStationsCoordinats(@RequestParam Long trainId) {
        List<Station> stations = trainService.getTrainStations(trainId);
        if(stations != null) {
            Gson gson = new Gson();
            String json = gson.toJson(stations);
            return json;
        }
        return "";
    }

}
