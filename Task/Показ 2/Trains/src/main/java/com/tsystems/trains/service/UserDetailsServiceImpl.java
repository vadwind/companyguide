package com.tsystems.trains.service;

import com.tsystems.trains.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 22.04.14
 * Time: 0:55
 * To change this template use File | Settings | File Templates.
 */
@Service("userDetailsServiceImpl")
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDAO userDAO;

    /**
     * Load user for authentication
     * @param login Login
     * @return
     * @throws UsernameNotFoundException
     */
    public UserDetails loadUserByUsername(String login)throws UsernameNotFoundException{

        com.tsystems.trains.entity.User user = userDAO.getUser(login);
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(user.getRole()));

        UserDetails authenticatedUser = new User(
                user.getLogin(),
                user.getPassword(),
                true,
                true,
                true,
                true,
                authorities
        );
        return authenticatedUser;
    }
}
