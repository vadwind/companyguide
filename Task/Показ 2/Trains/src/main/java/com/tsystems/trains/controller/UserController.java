package com.tsystems.trains.controller;

import com.tsystems.trains.DTO.UserDTO;
import com.tsystems.trains.entity.User;
import com.tsystems.trains.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 13.04.14
 * Time: 1:10
 * To change this template use File | Settings | File Templates.
 */

/**
 * User controller
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    /**
     * Bind string from jstl to Date in bean
     * @param webDataBinder
     */
    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    /**
     * Form for registration
     * @param model Model
     * @return  view
     */
    @RequestMapping("/login")
    public String login(HttpSession session,
                        ModelMap model) {
        model.put("user", new User());
        if(session.getAttribute("message") != null) {
            model.put("message", session.getAttribute("message"));
            session.removeAttribute("message");
        }
        return "user/login";
    }

    /**
     * Form for registration
     * @param model Model
     * @return  view
     */
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(HttpSession session, ModelMap model) {
        model.put("user", new UserDTO());
        if(session.getAttribute("message") != null) {
            model.put("message", session.getAttribute("message"));
            session.removeAttribute("message");
        }
        return "user/registration";
    }

    /**
     * Save of new user
     * @param userDTO  new User
     * @return  view
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addUser(HttpSession session,
                          @Valid @ModelAttribute("user")UserDTO userDTO,
                          BindingResult result) {
        userDTO.setRole("ROLE_CLIENT");
        if(result.hasErrors()) {
            session.setAttribute("message", "Incorrect data. Login, passwords, surname, " +
                    "name and email must be from 2 to 50 symbols");
            return "redirect:/user/registration";
        }
        if(userService.registrationUser(userDTO.getEntityObject())) {
            session.setAttribute("message", "You successfully registered. Please log in");
            return "redirect:/user/login";
        }
        else {
            session.setAttribute("message", "User already exists");
            return "redirect:/user/registration";
        }
    }

    /**
     * View tickets of user
     * @param userId
     * @param model
     * @return
     */
    @RequestMapping("/tickets/{userId}")
    public String tickets(HttpSession session, @PathVariable("userId") Long userId,
                       ModelMap model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDTO user = userService.getUser(userId);
        if(user != null) {
            if(user.getLogin().equals(auth.getName()))  {
                model.put("user", user);
                model.put("ticketsList", userService.getUserTickets(userId));
                if(session.getAttribute("message") != null) {
                    model.put("message", session.getAttribute("message"));
                    session.removeAttribute("message");
                }
                return "user/tickets";
            }
            return "redirect:/error403";
        }
        return "error404";
    }

    /**
     * View tickets of user profile
     * @param username
     * @param model
     * @return
     */
    @RequestMapping("/view/user={username}")
    public String view(HttpSession session, @PathVariable("username") String username,
                          ModelMap model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getUSerEntityByUsername(username);
        if(user != null) {
            if(username.equals(auth.getName())) {
                UserDTO userDTO = new UserDTO(user);
                userDTO.setId(user.getId());
                model.put("user", userDTO);
                if(session.getAttribute("message") != null) {
                    model.put("message", session.getAttribute("message"));
                    session.removeAttribute("message");
                }
                return "user/view";
            }
            return "redirect:/error403";
        }
        return "error404";
    }

    /**
     * View tickets of user profile
     * @param userId
     * @param model
     * @return
     */
    @RequestMapping("/view/{userId}")
    public String view(HttpSession session, @PathVariable("userId") Long userId,
                       ModelMap model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDTO userDTO = userService.getUser(userId);
        if(userDTO != null) {
            if(userDTO.getLogin().equals(auth.getName())) {
                userDTO.setId(userId);
                model.put("user", userDTO);
                if(session.getAttribute("message") != null) {
                    model.put("message", session.getAttribute("message"));
                    session.removeAttribute("message");
                }
                return "user/view";
            }
            return "redirect:/error403";
        }
        return "error404";
    }

    /**
     * Save of new user
     * @param userDTO  new User
     * @return  view
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateUser(HttpSession session,
                             @Valid @ModelAttribute("user")UserDTO userDTO,
                             BindingResult result,
                             ModelMap model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(userDTO.getLogin().equals(auth.getName())) {
            User oldUser = userService.getUserEntity(userDTO.getId());
            userDTO.setRole(oldUser.getRole());
            User user = userDTO.getEntityObject();
            user.setId(userDTO.getId());
            model.put("user", userDTO);
            if(result.hasErrors()) {
                session.setAttribute("message", "Incorrect data. Login, passwords, surname," +
                        "name must be from 2 to 50 symbols");
            }
            else {
                if(userService.updateUser(user)) {
                    session.setAttribute("message", "You successfully changed data");
                }
                else {
                    session.setAttribute("message", "User already exists");
                }
            }
            return "redirect:/user/view/"+userDTO.getId();
        }
        return "redirect:/error403";
    }

    /**
     * Admin panel for user
     * @param model
     * @return
     */
    @RequestMapping("/admin")
    public String admin(HttpSession session, ModelMap model) {
        model.put("userList", userService.getAllUsers());
        if(session.getAttribute("message") != null) {
            model.put("message", session.getAttribute("message"));
            session.removeAttribute("message");
        }
        return "user/admin";
    }

    /**
     * Update user
     * @param userId
     * @return view
     */
    @RequestMapping("/updateAdmin/{userId}")
    public String updateAdmin(@PathVariable("userId") Long userId,
                         ModelMap model) {
        UserDTO user = userService.getUser(userId);
        if(user != null) {
            user.setId(userId);
            model.put("user", user);
            model.put("ticketsList", userService.getUserTickets(userId));
            return "user/updateAdmin";
        }
        return "error404";
    }

    /**
     * Delete user
     * @param userId
     * @return view
     */
    @RequestMapping("/delete/{userId}")
    public String delete(HttpSession session, @PathVariable("userId") Long userId) {
        UserDTO user = userService.getUser(userId);
        if(user != null) {
            if(userService.deleteUser(userId)) {
                session.setAttribute("message", "User successfully deleted");
            }
            else {
                session.setAttribute("message", "User didn`t been delete");
            }
            return "redirect:/user/admin";
        }
        return "error404";
    }

    /**
     * Update role of user
     * @param userDTO
     * @param model
     * @return
     */
    @RequestMapping(value = "/updateRole", method = RequestMethod.POST)
    public String updateRole(@ModelAttribute("user")UserDTO userDTO, ModelMap model) {
        User user = userService.getUserEntity(userDTO.getId());
        user.setRole(userDTO.getRole());
        userService.updateUser(user);
        model.put("user", user);
        return "redirect:/user/admin";
    }


}