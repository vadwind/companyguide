package com.tsystems.trains.DTO;

import com.tsystems.trains.entity.User;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 22.04.14
 * Time: 21:17
 * To change this template use File | Settings | File Templates.
 */

/**
 * Class DTO for user
 */
public class UserDTO implements Serializable {
    private long id;
    private String role;
    @NotEmpty
    @Size(min=2, max=50)
    private String login;
    @NotEmpty
    @Size(min=2, max=50)
    private String password;
    @NotEmpty
    @Size(min=2, max=50)
    private String repeatPassword;
    @NotEmpty
    @Size(min=2, max=50)
    private String surname;
    @NotEmpty
    @Size(min=2, max=50)
    private String name;

    @NotEmpty
    private String birthday;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @NotNull
    private Date dateBirthday;

    @NotEmpty
    @Email
    @Size(min=2, max=50)
    private String email;

    /**
     * Default constructor
     */
    public UserDTO() {

    }

    /**
     * Constructor by entity
     * @param user
     */
    public UserDTO(User user) {
        this.id = user.getId();
        this.role = user.getRole();
        this.login = user.getLogin();
        this.surname = user.getSurname();
        this.name = user.getName();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        this.birthday = dateFormat.format(user.getBirthday());
        this.dateBirthday = user.getBirthday();
        this.email = user.getEmail();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public Date getDateBirthday() {
        return dateBirthday;
    }

    public void setDateBirthday(Date dateBirthday) {
        this.dateBirthday = dateBirthday;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        this.birthday = dateFormat.format(dateBirthday);
    }

    public void setBirthday(String birthday) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.dateBirthday = dateFormat.parse(birthday);
        } catch (ParseException e) {
        }
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User getEntityObject() {
        User user = new User();
        user.setRole(role);
        user.setLogin(login);
        user.setPassword(password);
        user.setSurname(surname);
        user.setName(name);
        user.setEmail(email);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            user.setBirthday(dateFormat.parse(birthday));
        } catch (ParseException e) {
        }
        return user;
    }
}
