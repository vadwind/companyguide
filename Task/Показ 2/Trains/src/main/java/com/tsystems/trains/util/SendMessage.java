package com.tsystems.trains.util;

import com.tsystems.trains.DTO.UserDTO;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 30.04.14
 * Time: 0:31
 * To change this template use File | Settings | File Templates.
 */

/**
 * Class for sending messages
 */
public class SendMessage implements Runnable {

    Thread thread;
    UserDTO user;
    Long ticketId;

    /**
     * Public constructor
     * @param user
     * @param ticketId
     */
    public SendMessage(UserDTO user, Long ticketId) {
        this.user = user;
        this.ticketId = ticketId;
        thread = new Thread(this);
        thread.start();
    }

    /**
     * Start thread
     */
    public void run() {
        sendMessage();

    }

    /**
     * Send message
     */
    public void sendMessage() {
        final String username = "trains.tsystems@gmail.com";
        final String password = "trains123456";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            MimeBodyPart messageBodyPart = new MimeBodyPart();

            Multipart multipart = new MimeMultipart();

            messageBodyPart = new MimeBodyPart();
            String file = "D:/T-Systems/pdf/ticket" + ticketId + ".pdf";
            String fileName = "ticket" + ticketId +".pdf";
            DataSource source = new FileDataSource(file);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(fileName);
            multipart.addBodyPart(messageBodyPart);

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("from-email@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(user.getEmail()));
            message.setSubject("Trains");
            message.setText("Dear, " + user.getSurname() + " " + user.getName()
                    + "!\n\nYou bought ticket in system Trains. Your ticket is in attached file in next email" +
                    "\n\n \n\n \n\nBest regards, Trains!");
            Transport.send(message);
            message.setContent(multipart);
            Transport.send(message);
        } catch (MessagingException e) {
        }

    }
}
