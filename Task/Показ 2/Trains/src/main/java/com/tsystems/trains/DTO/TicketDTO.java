package com.tsystems.trains.DTO;

import com.tsystems.trains.entity.Ticket;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 17.04.14
 * Time: 21:34
 * To change this template use File | Settings | File Templates.
 */
public class TicketDTO implements Serializable {

    private long id;
    private String surname;
    private String name;
    private String birthday;
    private String train;
    @NotEmpty
    @Size(min=2, max=50)
    private String startStation;
    @NotEmpty
    @Size(min=2, max=50)
    private String endStation;

    private String startStationTrain;
    private String endStationTrain;
    private String timeDeparture;
    private String timeArrive;
    private String dateDeparture;
    private String dateArrive;
    private Long trainId;
    private Long vacancies;

    /**
     * Default constructor
     */
    public TicketDTO() {

    }

    /**
     * Constructor by entity class
     * @param ticket
     */
    public TicketDTO(Ticket ticket) {
        this.id = ticket.getId();
        this.surname = ticket.getUser().getSurname();
        this.name = ticket.getUser().getName();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        this.birthday = dateFormat.format(ticket.getUser().getBirthday());
        this.train = ticket.getTrain().getTrainNumber();
        this.startStation = ticket.getStartStation().getName();
        this.endStation = ticket.getEndStation().getName();

        this.trainId = ticket.getTrain().getId();

        this.setStartStationTrain(ticket.getTrain().getStations().get(0).getStation().getName());
        this.setEndStationTrain(ticket.getTrain().getStations().get(ticket.getTrain().getStations().size() - 1).getStation().getName());

    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getStartStation() {
        return startStation;
    }

    public void setStartStation(String startStation) {
        this.startStation = startStation;
    }

    public String getTrain() {
        return train;
    }

    public void setTrain(String train) {
        this.train = train;
    }

    public String getEndStation() {
        return endStation;
    }

    public void setEndStation(String endStation) {
        this.endStation = endStation;
    }

    public String getStartStationTrain() {
        return startStationTrain;
    }

    public void setStartStationTrain(String startStationTrain) {
        this.startStationTrain = startStationTrain;
    }

    public String getEndStationTrain() {
        return endStationTrain;
    }

    public void setEndStationTrain(String endStationTrain) {
        this.endStationTrain = endStationTrain;
    }

    public Long getTrainId() {
        return trainId;
    }

    public void setTrainId(Long trainId) {
        this.trainId = trainId;
    }

    public Long getVacancies() {
        return vacancies;
    }

    public void setVacancies(Long vacancies) {
        this.vacancies = vacancies;
    }

    public String getTimeDeparture() {
        return timeDeparture;
    }

    public void setTimeDeparture(String timeDeparture) {
        this.timeDeparture = timeDeparture;
    }

    public String getTimeArrive() {
        return timeArrive;
    }

    public void setTimeArrive(String timeArrive) {
        this.timeArrive = timeArrive;
    }

    public String getDateDeparture() {
        return dateDeparture;
    }

    public void setDateDeparture(String dateDeparture) {
        this.dateDeparture = dateDeparture;
    }

    public String getDateArrive() {
        return dateArrive;
    }

    public void setDateArrive(String dateArrive) {
        this.dateArrive = dateArrive;
    }
}
