package com.tsystems.trains.service;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 15.04.14
 * Time: 21:31
 * To change this template use File | Settings | File Templates.
 */

import com.tsystems.trains.DTO.TicketDTO;
import com.tsystems.trains.DTO.TimetableDTO;
import com.tsystems.trains.DTO.TrainDTO;
import com.tsystems.trains.dao.StationDAO;
import com.tsystems.trains.dao.TicketDAO;
import com.tsystems.trains.dao.TimetableDAO;
import com.tsystems.trains.dao.TrainDAO;
import com.tsystems.trains.entity.Station;
import com.tsystems.trains.entity.Ticket;
import com.tsystems.trains.entity.Timetable;
import com.tsystems.trains.entity.Train;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Class implements train service
 */
@Service
public class TrainService {

    @Autowired
    private TrainDAO trainDAO;
    @Autowired
    private TicketDAO ticketDAO;
    @Autowired
    private StationDAO stationDAO;
    @Autowired
    private TimetableDAO timetableDAO;

    private static Logger logger = Logger.getLogger(TrainService.class);

    /**
     * Get all trains
     * @return List of trains
     */
    @Transactional
    public List<TrainDTO> getAllTrains() {
        List<Train> trains = trainDAO.selectAllRecords();
        List<TrainDTO> trainsDTO = new ArrayList<TrainDTO>();
        for(Train train : trains) {
            trainsDTO.add(new TrainDTO(train));
        }
        logger.info("Get all trains");
        return trainsDTO;
    }

    /**
     * Delete train
     * @param trainId Id of train
     * @return success
     */
    @Transactional
    public boolean deleteTrain(Long trainId){

        Train train = trainDAO.findByPk(trainId);
        List<Ticket> tickets = ticketDAO.findByTrain(trainId);
        if(train != null && tickets.isEmpty()) {
            trainDAO.delete(train);
            logger.info("Delete train with id "+trainId + ". Success");
            return true;
        }
        logger.warn("Delete train with id "+trainId + ". Failed");
        return false;
    }

    /**
     * Get timetable of train
     * @param trainId Train
     * @return list
     */
    @Transactional
    public List<TimetableDTO> getTrainTimetable(Long trainId) {
        Train train = trainDAO.findByPk(trainId);
        List<TimetableDTO> timetablesTrain = new ArrayList<TimetableDTO>();
        for(Timetable timetable : train.getStations()) {
            timetablesTrain.add(new TimetableDTO(timetable));
        }
        logger.info("Get timetable of train");
        return timetablesTrain;
    }

    /**
     * Get train
     * @return List of trains
     */
    @Transactional
    public TrainDTO getTrain(Long trainId) {
        Train train = trainDAO.findByPk(trainId);
        if(train != null) {
            TrainDTO trainDTO = new TrainDTO(train);
            logger.info("Get train with id "+trainId + ". Success");
            return trainDTO;
        }
        logger.warn("Get train with id "+trainId + ". Failed");
        return null;
    }

    /**
     * Update
     * @param trainDTO
     */
    @Transactional
    public boolean updateTrain(TrainDTO trainDTO) {
        Train train = trainDTO.getEntityObject();
        trainDAO.update(train);
        logger.info("Update train with id "+ trainDTO.getId() + ". Success");
        return true;
    }

    /**
     * Get all passengers of train
     * @param trainId Train
     * @return List of passengers
     */
    @Transactional
    public List<TicketDTO> getPassengersTrain(Long trainId) {
        List<Ticket> tickets = ticketDAO.findByTrain(trainId);
        List<TicketDTO> ticketsDTO = new ArrayList<TicketDTO>();
        for(Ticket ticket : tickets) {
            ticketsDTO.add(new TicketDTO(ticket));
        }
        logger.info("Get passengers of train with id "+trainId + ". Success");
        return ticketsDTO;
    }

    /**
     * Add new train
     * @param train Train
     * @return success
     */
    @Transactional
    public boolean addTrain(Train train, List<TimetableDTO> stations){
        //if name of train is unique
        if(!trainDAO.isTrain(train)) {
            //save train
            trainDAO.insert(train);

            Train newTrain = trainDAO.findByTrainNumber(train.getTrainNumber());
//
            //save stations
            int numberSequence = 1;
            for(TimetableDTO timetableDTO : stations) {
                Timetable saveTimetable = new Timetable();
                //train
                saveTimetable.setTrain(newTrain);
                //station
                saveTimetable.setStation(stationDAO.findByName(timetableDTO.getStation()));
                //count of passengers
                saveTimetable.setVacancies(newTrain.getCountPassengers());
                //number of sequence
                saveTimetable.setNumberSequence(numberSequence);
                //time of arrive
                String[] time = timetableDTO.getTimeArrive().split(":");
                saveTimetable.setTimeArrive(new Time(Integer.parseInt(time[0]), Integer.parseInt(time[1]), 0));

                //time of departure
                time = timetableDTO.getTimeDeparture().split(":");
                saveTimetable.setTimeDeparture(new Time(Integer.parseInt(time[0]), Integer.parseInt(time[1]), 0));

                //date
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    saveTimetable.setDate(dateFormat.parse(timetableDTO.getDate()));
                } catch (ParseException e) {
                    return false;
                }

                //save
                timetableDAO.insert(saveTimetable);

                numberSequence ++;
            }
            logger.info("Add train. Success");
            return true;
        }
        logger.warn("Add train. Failed");
        return false;
    }

    /**
     * Get stations of train
     * @param trainId
     * @return
     */
    @Transactional
    public List<Station> getTrainStations(Long trainId) {
        Train train = trainDAO.findByPk(trainId);
        List<Station> stations = new ArrayList<Station>();
        if(train != null) {
            List<Timetable> timetables = train.getStations();
            for(Timetable timetable : timetables) {
                stations.add(timetable.getStation());
            }
            Station st = new Station();
            st.setName("center");
            Double centerLat = (Double.parseDouble(stations.get(0).getLat()) +
                    Double.parseDouble(stations.get(stations.size() - 1).getLat()))/2;
            Double centerLng = (Double.parseDouble(stations.get(0).getLng()) +
                    Double.parseDouble(stations.get(stations.size() - 1).getLng()))/2;
            st.setLat(centerLat.toString());
            st.setLng(centerLng.toString());
            stations.add(st);
            logger.info("Get stations of train. Success");
            return  stations;
        }
        logger.warn("Get stations of train. Failed");
        return null;
    }
}
