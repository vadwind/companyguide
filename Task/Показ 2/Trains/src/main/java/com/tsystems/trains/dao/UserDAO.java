package com.tsystems.trains.dao;

import com.tsystems.trains.entity.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 12.04.14
 * Time: 16:15
 * To change this template use File | Settings | File Templates.
 */

/**
 * Class implements DAO for User
 */
@Repository
public class UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Insert new record
     * @param user User entity
     */
    public void insert(User user) {
        sessionFactory.getCurrentSession().persist(user);
    }

    /**
     * Update user
     * @param user User
     */
    public void update(User user) {
        sessionFactory.getCurrentSession().update(user);
    }

    /**
     * Delete record
     * @param user User entity
     */
    public void delete(User user) {
        sessionFactory.getCurrentSession().delete(user);
    }

    /**
     * Select all users
     * @return List of users
     */
    public List<User> selectAllRecords() {
        return sessionFactory.getCurrentSession().createQuery("from User").list();
    }

    /**
     * Check user
     * @param user User entity
     * @return true if user there
     */
    public boolean isUser(User user) {
        //check user in db
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        List<User> users = sessionFactory.getCurrentSession().createQuery("from User where surname='" +
                user.getSurname() + "' AND name='" + user.getName() + "' AND DATE_FORMAT(birthday,'%Y-%m-%d')='" +
                dateFormat.format(user.getBirthday())+"'").list();
        if(!users.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Get user by login
     * @param login Login
     * @return User
     */
    public User getUser(String login) {
        List<User> users = sessionFactory.getCurrentSession().createQuery("from User where login='" + login + "'").list();
        if(!users.isEmpty()) {
            return users.get(0);
        }
        User user = new User();
        user.setLogin("anon");
        user.setPassword("anon");
        user.setRole("ROLE_ANONYMOUS");
        return user;
    }

    /**
     * Get User by pk
     * @param userId  id
     * @return User
     */
    public User getUserByPk(Long userId) {
        List<User> users = sessionFactory.getCurrentSession().createQuery("from User where id=" + userId).list();
        if(!users.isEmpty()) {
            return users.get(0);
        }
        return null;
    }

    /**
     * Get user by username
     * @param username Username
     * @return User
     */
    public User getUserByUsername(String username) {
        List<User> users = sessionFactory.getCurrentSession().createQuery("from User where login='" + username + "'").list();
        if(!users.isEmpty()) {
            return users.get(0);
        }
        return null;
    }


}
