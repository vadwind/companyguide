package com.tsystems.trains.controller;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 13.04.14
 * Time: 23:38
 * To change this template use File | Settings | File Templates.
 */

import com.google.gson.Gson;
import com.tsystems.trains.entity.Station;
import com.tsystems.trains.service.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

/**
 * Station controller
 */
@Controller
@RequestMapping("/station")
public class StationController {

    @Autowired
    StationService stationService;

    /**
     * Form for creating new station
     * @param model Model
     * @return  view
     */
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String create(HttpSession session, ModelMap model) {
        model.put("station", new Station());
        if(session.getAttribute("message") != null) {
            model.put("message", session.getAttribute("message"));
            session.removeAttribute("message");
        }
        return "station/create";
    }

    /**
     * Save of new station
     * @param station  new Station
     * @return  view
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addStation(HttpSession session,
                             @Valid @ModelAttribute("station")Station station,
                             BindingResult result, ModelMap model) {
        if(result.hasErrors()) {
            session.setAttribute("message", "Incorrect data. Name station must be " +
                    "from 2 to 50 symbols and you must specify location of station on the map");
            return "redirect:/station/create";
        }
        if(!stationService.addStation(station))  {
            session.setAttribute("message", "Station already exists");
            return "redirect:/station/create";
        }
        session.setAttribute("message", "Station successfully added");
        return "redirect:/station/admin";
    }

    /**
     * Admin panel for station
     * @param model
     * @return
     */
    @RequestMapping("/admin")
    public String admin(HttpSession session, ModelMap model) {

        model.put("stationList", stationService.getAllStations());
        if(session.getAttribute("message") != null) {
            model.put("message", session.getAttribute("message"));
            session.removeAttribute("message");
        }
        return "station/admin";
    }

    /**
     * Delete station
     * @param stationId
     * @return
     */
    @RequestMapping("/delete/{stationId}")
    public String delete(HttpSession session,
                         @PathVariable("stationId") Long stationId) {
        Station station = stationService.getStation(stationId);
        if(station != null) {
            if(stationService.deleteStation(stationId)) {
                session.setAttribute("message", "Station successfully been delete");
            }
            else {
                session.setAttribute("message", "Station didn`t been delete");
            }
            return "redirect:/station/admin";
        }
        else
            return "error404";
    }

    /**
     * Update station
     * @param stationId
     * @return
     */
    @RequestMapping("/update/{stationId}")
    public String update(HttpSession session,
                         @PathVariable("stationId") Long stationId,
                         ModelMap model) {
        Station station = stationService.getStation(stationId);
        if(station != null) {
            model.put("station", stationService.getStation(stationId));
            if(session.getAttribute("message") != null) {
                model.put("message", session.getAttribute("message"));
                session.removeAttribute("message");
            }
            return "station/update";
        }
        else
            return "error404";
    }

    /**
     * Refresh station
     * @param station  Station
     * @return  view
     */
    @RequestMapping(value = "/refresh", method = RequestMethod.POST)
    public String refreshStation(ModelMap model,
                                 @Valid @ModelAttribute("station")Station station,
                                 BindingResult result,
                                 HttpSession session) {
        if(result.hasErrors()) {
            session.setAttribute("message", "Incorrect data. Name station must be " +
                    "from 2 to 50 symbols");
            return "redirect:/station/update/"+station.getId();
        }
        if(!stationService.updateStation(station)) {
            session.setAttribute("message", "Station already exists");
            return "redirect:/station/update/"+station.getId();
        }
        session.setAttribute("message", "Station successfully been update");
        return "redirect:/station/admin";
    }

    /**
     * Index for station
     * @param model
     * @return
     */
    @RequestMapping("/index")
    public String index(ModelMap model) {

        model.put("stationList", stationService.getAllStations(true));
        return "station/index";
    }

    /**
     * Default for station
     * @return
     */
    @RequestMapping("/")
    public String home() {
        return "redirect:/station/index";
    }

    /**
     * View station
     * @param stationId
     * @return
     */
    @RequestMapping("/view/{stationId}")
    public String view(@PathVariable("stationId") Long stationId,
                         ModelMap model) {
        Station station = stationService.getStation(stationId);
        if(station != null) {
            model.put("station", station);
            model.put("trainList", stationService.getTrainsOnStations(stationId));
            return "station/view";
        }
        else
            return "error404";
    }

    /**
     * View station
     * @param stationName
     * @return
     */
    @RequestMapping("/view/name={stationName}")
    public String view(@PathVariable("stationName") String stationName,
                       ModelMap model) {
        Station station = stationService.getStationByName(stationName);
        if(station != null) {
            model.put("station", station);
            model.put("trainList", stationService.getTrainsOnStations(station.getId()));
            return "station/view";
        }
        else
            return "error404";
    }

    /**
     * Get all stations
     * @return array
     */
    @RequestMapping(value = "/getStationsCoord", method = RequestMethod.POST)
    public @ResponseBody
    String getStationsCoordinats() {
        List<Station> stations = stationService.getAllStations();
        if(stations != null) {
            Gson gson = new Gson();
            String json = gson.toJson(stations);
            return json;
        }
        return "";
    }
}
