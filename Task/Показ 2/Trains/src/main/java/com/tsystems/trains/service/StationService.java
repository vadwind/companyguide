package com.tsystems.trains.service;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 13.04.14
 * Time: 23:33
 * To change this template use File | Settings | File Templates.
 */

import com.tsystems.trains.DTO.TrainDTO;
import com.tsystems.trains.dao.StationDAO;
import com.tsystems.trains.dao.TimetableDAO;
import com.tsystems.trains.dao.TrainDAO;
import com.tsystems.trains.entity.Station;
import com.tsystems.trains.entity.Timetable;
import com.tsystems.trains.entity.Train;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Class implements station service
 */
@Service
public class StationService {

    @Autowired
    private StationDAO stationDAO;

    @Autowired
    private TimetableDAO timetableDAO;

    private static Logger logger = Logger.getLogger(StationService.class);


    /**
     * Add new station
     * @param station Station
     * @return success
     */
    @Transactional
    public boolean addStation(Station station){
        if(!stationDAO.isStation(station)) {
            stationDAO.insert(station);
            logger.info("Add station " + station.getName() + ". Success");
            return true;
        }
        logger.warn("Add station " + station.getName() + ". Failed");
        return false;
    }

    /**
     * Delete station
     * @param stationId Id of station
     * @return success
     */
    @Transactional
    public boolean deleteStation(Long stationId){

        Station station = stationDAO.findByPk(stationId);
        if(station != null) {
            if(!timetableDAO.isStationInTrains(station)) {
                stationDAO.delete(station);
                logger.info("Delete station " + station.getName() + ". Success");
                return true;
            }
        }
        logger.warn("Delete station " + station.getName() + ". Success");
        return false;
    }

    /**
     * Get all stations
     * @return List of stations
     */
    @Transactional
    public List<Station> getAllStations() {
        logger.info("Get all stations. Success");
        return stationDAO.selectAllRecords();
    }

    /**
     * Get all stations with sort
     * @return List of stations
     */
    @Transactional
    public List<Station> getAllStations(boolean sort) {
        logger.info("Get all stations. Success");
        return stationDAO.selectAllSortRecords();
    }


    /**
     * Get station by id
     * @param stationId  id
     * @return  Station
     */
    @Transactional
    public Station getStation(Long stationId){
        logger.info("Get station with id "+ stationId +". Success");
        return stationDAO.findByPk(stationId);
    }


    /**
     * Get station
     * @param stationName  Name of station
     * @return
     */
    @Transactional
    public Station getStationByName(String stationName){
        logger.info("Get station with name "+ stationName +". Success");
        return stationDAO.findByName(stationName);
    }

    /**
     * Update station
     * @param station Station
     * @return  success
     */
    @Transactional
    public boolean updateStation(Station station){
        stationDAO.update(station);
        logger.info("Update station " + station.getName() + ". Success");
        return true;
    }

    /**
     * View timetable of station
     * @param stationId
     * @return
     */
    @Transactional
    public List<TrainDTO> getTrainsOnStations(Long stationId){
        List<Timetable> timetablesTrainsOnStations = timetableDAO.selectTrainsOnStations(stationId);

        List<TrainDTO> trainsDTO = new ArrayList<TrainDTO>();
        for(Timetable train : timetablesTrainsOnStations) {
            TrainDTO trainDTO = new TrainDTO(train.getTrain());
            trainDTO.setTimeArrive(train.getTimeArrive().toString());
            trainDTO.setTimeDeparture(train.getTimeDeparture().toString());
            trainsDTO.add(trainDTO);
        }
        logger.info("Get trains on station with id " + stationId + ". Success");
        return trainsDTO;
    }
}
