package com.tsystems.trains.service;

import com.tsystems.trains.DTO.TicketDTO;
import com.tsystems.trains.DTO.UserDTO;
import com.tsystems.trains.dao.TicketDAO;
import com.tsystems.trains.dao.TimetableDAO;
import com.tsystems.trains.dao.UserDAO;
import com.tsystems.trains.entity.Ticket;
import com.tsystems.trains.entity.Timetable;
import com.tsystems.trains.entity.Train;
import com.tsystems.trains.entity.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 12.04.14
 * Time: 16:14
 * To change this template use File | Settings | File Templates.
 */

/**
 * Class implements user service
 */
@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private TicketDAO ticketDAO;

    @Autowired
    private TimetableDAO timetableDAO;

    private static Logger logger = Logger.getLogger(UserService.class);

    /**
     * Registration new user
     * @param user USer
     * @return success
     */
    @Transactional
    public boolean registrationUser(User user) {
        if(!userDAO.isUser(user)) {
            userDAO.insert(user);
            logger.info("Add user with login " + user.getLogin() + ". Success");
            return true;
        }
        logger.warn("Add user with login " + user.getLogin() + ". Failed");
        return false;
    }

    /**
     * Get user tickets
     * @param userId Id of user
     * @return ticketDTO
     */
    @Transactional
    public List<TicketDTO> getUserTickets(Long userId) {
        User user = userDAO.getUserByPk(userId);
        List<Ticket> tickets = ticketDAO.findByUser(user);
        List<TicketDTO> ticketsDTO = new ArrayList<TicketDTO>();
        for(Ticket ticket : tickets) {
            TicketDTO ticketDTO = new TicketDTO(ticket);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

            Timetable stationDeparture = timetableDAO.getStationByTrainAndStation(ticket.getTrain().getId(), ticket.getStartStation().getId());
            Timetable stationArrive = timetableDAO.getStationByTrainAndStation(ticket.getTrain().getId(), ticket.getEndStation().getId());
            ticketDTO.setTimeDeparture(stationDeparture.getTimeDeparture().toString());
            ticketDTO.setTimeArrive(stationArrive.getTimeArrive().toString());
            ticketDTO.setDateDeparture(dateFormat.format(stationDeparture.getDate()));
            ticketDTO.setDateArrive(dateFormat.format(stationArrive.getDate()));

            ticketsDTO.add(ticketDTO);
        }
        logger.info("Get tickets of user");
        return ticketsDTO;
    }

    /**
     * Get user
     * @param userId Id
     * @return UserDTO
     */
    @Transactional
    public UserDTO getUser(Long userId) {
        User user = userDAO.getUserByPk(userId);
        if(user != null) {
            logger.info("Get user wit id "+userId +". Success");
            return new UserDTO(userDAO.getUserByPk(userId));
        }
        logger.warn("Get user wit id "+userId +". Failed");
        return null;
    }

    /**
     * Get user entity
     * @param userId Id
     * @return UserDTO
     */
    @Transactional
    public User getUserEntity(Long userId) {
        logger.info("Get user wit id "+userId +". Success");
        return userDAO.getUserByPk(userId);
    }

    /**
     * Get user bu username
     * @param username username
     * @return user
     */
    @Transactional
    public User getUSerEntityByUsername(String username) {
        logger.info("Get user wit login "+username +". Success");
        return userDAO.getUser(username);
    }

    /**
     * Get user by username
     * @param username Username
     * @return DTO of user
     */
    @Transactional
    public UserDTO getUserByUsername(String username) {
        UserDTO userDTO = new UserDTO(userDAO.getUserByUsername(username));
        logger.info("Get user wit login "+username +". Success");
        return userDTO;
    }

    /**
     * Update user
     * @param user User
     * @return success
     */
    @Transactional
    public boolean updateUser(User user) {
        userDAO.update(user);
        logger.info("Update user wit id "+user.getId() +". Success");
        return true;
    }

    /**
     * Delete user
     * @param userId Id of user
     * @return success
     */
    @Transactional
    public boolean deleteUser(Long userId){

        User user = userDAO.getUserByPk(userId);
        List<Ticket> tickets = ticketDAO.findByUser(user);
        if(user != null && tickets.isEmpty()) {
            userDAO.delete(user);
            logger.info("Delete user wit id "+user.getId() +". Success");
            return true;
        }
        logger.warn("Delete user wit id "+user.getId() +". Failed");
        return false;
    }

    /**
     * Get all users
     * @return List of users
     */
    @Transactional
    public List<UserDTO> getAllUsers() {
        List<User> users = userDAO.selectAllRecords();
        List<UserDTO> usersDTO = new ArrayList<UserDTO>();
        for(User user : users) {
            usersDTO.add(new UserDTO(user));
        }
        logger.info("Get all users. Success");
        return usersDTO;
    }

    /**
     * Get user tickets
     * @param userId Id of user
     * @return ticketDTO
     */
    @Transactional
    public TicketDTO getUserTicketByTrain(Long userId, Long trainId) {
        User user = userDAO.getUserByPk(userId);
        Ticket ticket = ticketDAO.findByUserAndTrain(user, trainId);
        TicketDTO ticketDTO = new TicketDTO(ticket);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        Timetable stationDeparture = timetableDAO.getStationByTrainAndStation(ticket.getTrain().getId(), ticket.getStartStation().getId());
        Timetable stationArrive = timetableDAO.getStationByTrainAndStation(ticket.getTrain().getId(), ticket.getEndStation().getId());
        ticketDTO.setTimeDeparture(stationDeparture.getTimeDeparture().toString());
        ticketDTO.setTimeArrive(stationArrive.getTimeArrive().toString());
        ticketDTO.setDateDeparture(dateFormat.format(stationDeparture.getDate()));
        ticketDTO.setDateArrive(dateFormat.format(stationArrive.getDate()));
        logger.info("Get ticket of users by train. Success");
        return ticketDTO;
    }

}
