package com.tsystems.trains.service;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 23.04.14
 * Time: 1:43
 * To change this template use File | Settings | File Templates.
 */

import com.tsystems.trains.DTO.TicketDTO;
import com.tsystems.trains.DTO.UserDTO;
import com.tsystems.trains.dao.*;
import com.tsystems.trains.entity.*;

import com.tsystems.trains.util.CreatePDF;
import com.tsystems.trains.util.SendMessage;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import javax.servlet.http.HttpServletResponse;

/**
 * Class implements ticket service
 */
@Service
public class TicketService {

    @Autowired
    private TicketDAO ticketDAO;
    @Autowired
    private StationDAO stationDAO;
    @Autowired
    private TimetableDAO timetableDAO;
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private TrainDAO trainDAO;

    private static Logger logger = Logger.getLogger(TicketService.class);

    /**
     * Search trains
     * @param ticketDTO search ticket
     * @return List of trains
     */
    @Transactional
    public List<TicketDTO> searchTrains(TicketDTO ticketDTO) {
        //get stations of departure and arrive
        Station stationDeparture = stationDAO.findByName(ticketDTO.getStartStation());
        Station stationArrive = stationDAO.findByName(ticketDTO.getEndStation());

        logger.info("Search ticket");
        if(stationDeparture != null && stationArrive != null) {
            //get trains on stations departure in this date
            List<Timetable> trainsOnStationOfDeparture = timetableDAO.searchTrainsOnStationOfDeparture(stationDeparture.getId(), ticketDTO.getDateDeparture());

            List<TicketDTO> trains = ticketDAO.findTickets(trainsOnStationOfDeparture, stationDeparture, stationArrive);
            for(TicketDTO train : trains) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Timetable stationDepartureTimetable = timetableDAO.getStationByTrainAndStation(train.getTrainId(), stationDeparture.getId());
                Timetable stationArriveTimetable = timetableDAO.getStationByTrainAndStation(train.getTrainId(), stationArrive.getId());
                train.setTimeDeparture(stationDepartureTimetable.getTimeDeparture().toString());
                train.setTimeArrive(stationArriveTimetable.getTimeArrive().toString());
                train.setDateDeparture(dateFormat.format(stationDepartureTimetable.getDate()));
                train.setDateArrive(dateFormat.format(stationArriveTimetable.getDate()));
            }
            return trains;
        }
        return null;
    }

    /**
     * Buy ticket
     * @param userDTO User
     * @param stationDepartureString Station
     * @param stationArriveString Station
     * @param trainId Train id
     * @return success
     */
    @Transactional
    public boolean buyTicket(UserDTO userDTO, String stationDepartureString,
                             String stationArriveString, Long trainId) {
        //get stations of departure and arrive
        logger.info("Search ticket");
        Station stationDeparture = stationDAO.findByName(stationDepartureString);
        Station stationArrive = stationDAO.findByName(stationArriveString);
        //get user
        User user = userDAO.getUserByUsername(userDTO.getLogin());
        //get user
        Train train = trainDAO.findByPk(trainId);
        //get stations of train
        List<Timetable> stationTrain = train.getStations();
        if(!ticketDAO.isUserInTrain(train, user)) {
            if(ticketDAO.isTicket(stationDeparture, stationArrive, train)) {
                //create ticket
                Ticket ticket = new Ticket();
                ticket.setStartStation(stationDeparture);
                ticket.setEndStation(stationArrive);
                ticket.setUser(user);
                ticket.setTrain(train);
                //save ticket
                ticketDAO.insert(ticket);
                //save timetable
                ticketDAO.changeVacancies(train.getStations(), stationDeparture, stationArrive);
                logger.info("Buy ticket. success");
                return true;
            }
        }
        logger.warn("Buy ticket. Failed");
        return false;

    }

    /**
     * Get ticket
     * @param ticketId
     * @return
     */
    @Transactional
    public TicketDTO getTicketById(Long ticketId) {
        Ticket ticket = ticketDAO.findByPk(ticketId);
        if(ticket != null) {
            TicketDTO ticketDTO = new TicketDTO(ticket);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

            Timetable stationDeparture = timetableDAO.getStationByTrainAndStation(ticket.getTrain().getId(), ticket.getStartStation().getId());
            Timetable stationArrive = timetableDAO.getStationByTrainAndStation(ticket.getTrain().getId(), ticket.getEndStation().getId());
            ticketDTO.setTimeDeparture(stationDeparture.getTimeDeparture().toString());
            ticketDTO.setTimeArrive(stationArrive.getTimeArrive().toString());
            ticketDTO.setDateDeparture(dateFormat.format(stationDeparture.getDate()));
            ticketDTO.setDateArrive(dateFormat.format(stationArrive.getDate()));
            logger.info("Get ticket with id "+ ticketId +". success");
            return ticketDTO;
        }
        logger.warn("Get ticket with id "+ ticketId +". Failed");
        return null;
    }

    /**
     * Send message to user
     * @param user User
     * @param ticketId id of ticket
     */
    synchronized public void sendMessage(UserDTO user, Long ticketId) {
        logger.info("Send message to user "+user.getLogin());
        SendMessage sendMessage = new SendMessage(user, ticketId);
    }


    /**
     * Create PDF ticket in file
     * @param user
     * @param ticket
     */
    public void createPDF(UserDTO user, TicketDTO ticket) {
        CreatePDF createPDF = new CreatePDF(user, ticket);

    }

    /**
     * Create PDF ticket in
     * @param user
     * @param ticket
     */
    public void createPDF(UserDTO user, TicketDTO ticket, HttpServletResponse response) {
        CreatePDF createPDF = new CreatePDF(user, ticket, response);
    }



}
