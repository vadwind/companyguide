package com.tsystems.trains.controller;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 24.04.14
 * Time: 11:24
 * To change this template use File | Settings | File Templates.
 */

import com.tsystems.trains.DTO.TicketDTO;
import com.tsystems.trains.service.StationService;
import com.tsystems.trains.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * Hello controller
 */
@Controller
public class HelloController {

    @Autowired
    StationService stationService;

    /**
     * Error 403
     * @return view
     */
    @RequestMapping("/error403")
    public String error403() {
        return "error403";
    }

    /**
     * Index
     * @return view
     */
    @RequestMapping("/index")
    public String index(HttpSession session, ModelMap model) {
        if(session.getAttribute("ticket") == null) {
            session.setAttribute("ticket", new TicketDTO());
        }
        model.put("ticket", (TicketDTO)session.getAttribute("ticket"));
        session.setAttribute("stationList", stationService.getAllStations(true));
        return "index";
    }

    /**
     * Default
     * @return view
     */
    @RequestMapping("")
    public String defaultPage() {
        return "redirect:/index";
    }

    /**
     * Default
     * @return view
     */
    @RequestMapping("/")
    public String defaultPage2() {
        return "redirect:/index";
    }

    /**
     * Admin
     * @return view
     */
    @RequestMapping("/admin")
    public String admin() {
        return "admin";
    }
}
