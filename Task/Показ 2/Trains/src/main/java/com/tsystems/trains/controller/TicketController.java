package com.tsystems.trains.controller;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.tsystems.trains.DTO.TicketDTO;
import com.tsystems.trains.DTO.TimetableDTO;
import com.tsystems.trains.DTO.TrainDTO;
import com.tsystems.trains.DTO.UserDTO;
import com.tsystems.trains.entity.Station;
import com.tsystems.trains.service.StationService;
import com.tsystems.trains.service.TicketService;
import com.tsystems.trains.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 23.04.14
 * Time: 1:41
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/ticket")
public class TicketController {

    @Autowired
    TicketService ticketService;

    @Autowired
    StationService stationService;

    @Autowired
    UserService userService;

    /**
     * Bind string from jstl to Date in bean
     * @param webDataBinder
     */
    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    /**
     * Open view search
     * @param session Session
     * @param model Model
     * @return view
     */
    @RequestMapping("/search")
    public String search(HttpSession session, ModelMap model) {
        if(session.getAttribute("ticket") == null) {
            session.setAttribute("ticket", new TicketDTO());
        }
        if(session.getAttribute("message") != null) {
            model.put("message", session.getAttribute("message"));
            session.removeAttribute("message");
        }
        model.put("ticket", (TicketDTO)session.getAttribute("ticket"));
        session.setAttribute("stationList", stationService.getAllStations(true));
        return "ticket/search";
    }

    /**
     * Get stations
     * @param session Session
     * @param stationName Name of station
     * @return List of station
     */
    @RequestMapping(value = "/getStations", method = RequestMethod.GET)
    public @ResponseBody
    List<Station> getStations(HttpSession session, @RequestParam String stationName) {
        List<Station> result = new ArrayList<Station>();
        List<Station> stations = (List<Station>) session.getAttribute("stationList");
        for(Station station : stations) {
            if(station.getName().contains(stationName)) {
                result.add(station);
            }
        }
        return result;
    }

    /**
     * Search train
     * @param session
     * @param ticketDTO
     * @param model
     * @return view
     */
    @RequestMapping(value = "/searchTrain", method = RequestMethod.POST)
    public String searchTrain(HttpSession session, @Valid @ModelAttribute("ticket")TicketDTO ticketDTO,
                              BindingResult result,
                              ModelMap model) {
        session.setAttribute("ticket", ticketDTO);
        if(result.hasErrors()) {
            session.setAttribute("message", "Incorrect search data");
            return "redirect:/ticket/search";
        }
        else {
            List<TicketDTO> ticketDTOList = ticketService.searchTrains(ticketDTO);
            model.put("ticketsList", ticketDTOList);
            session.setAttribute("ticketList", ticketDTOList);
            if(ticketDTOList == null) {
                session.setAttribute("message", "Such stations doesn`t exist");
                return "redirect:/ticket/search";
            }
            if(ticketDTOList.isEmpty()) {
                session.setAttribute("message", "No results");
                return "redirect:/ticket/search";
            }
        }
        return "ticket/search";
    }

    /**
     * Buy ticket confirm
     * @param session Session
     * @param trainId Id of train
     * @param model Model
     * @return view
     */
    @RequestMapping("/buy/{trainId}")
    public String buy(HttpSession session, @PathVariable("trainId") Long trainId,
                         ModelMap model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDTO userDTO = userService.getUserByUsername(user.getUsername());
        if(userDTO != null) {
            model.put("user", userDTO);
            session.setAttribute("user", userDTO);
            model.put("ticket", (TicketDTO)session.getAttribute("ticket"));
            List<TicketDTO> ticketDTOList = (List<TicketDTO>) session.getAttribute("ticketList");
            for(TicketDTO ticketDTO : ticketDTOList) {
                if(ticketDTO.getTrainId() == trainId) {
                    model.put("train", ticketDTO);
                    session.setAttribute("train", ticketDTO);
                }
            }
            return "ticket/buy";
        }
        return "error404";
    }

    /**
     * Buy ticket
     * @param session
     * @return
     */
    @RequestMapping("/buyTicket")
    public String buyTicket(HttpSession session) {
        UserDTO userDTO = (UserDTO)session.getAttribute("user");
        String stationDeparture = ((TicketDTO)session.getAttribute("ticket")).getStartStation();
        String stationArrive = ((TicketDTO)session.getAttribute("ticket")).getEndStation();
        Long trainId = ((TicketDTO)session.getAttribute("train")).getTrainId();
        if(ticketService.buyTicket(userDTO, stationDeparture, stationArrive, trainId))  {
            session.setAttribute("message", "You successfully bought ticket. On your email " + userDTO.getEmail()
                                            + " will send ticket");
            ticketService.createPDF(userDTO, userService.getUserTicketByTrain(userDTO.getId(), trainId));
            ticketService.sendMessage(userDTO, userService.getUserTicketByTrain(userDTO.getId(), trainId).getId());
        }
        else
            session.setAttribute("message", "You couldn`t buy ticket, because you already bought ticket on thus train or " +
                    "impossible to buy a ticket for this train");
        return "redirect:/user/tickets/"+userDTO.getId();
    }

    /**
     * Buy ticket confirm
     * @param ticketId Id of train
     * @return view
     */
    @RequestMapping("/print/{ticketId}")
    public void print(@PathVariable("ticketId") Long ticketId,
                      HttpServletResponse response) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDTO userDTO = userService.getUserByUsername(user.getUsername());
        TicketDTO ticketDTO = ticketService.getTicketById(ticketId);
        if(userDTO != null && ticketDTO != null) {
            //create pdf if it isn`t exist
            ticketService.createPDF(userDTO, userService.getUserTicketByTrain(userDTO.getId(), ticketDTO.getTrainId()));
            //String fileURL = "D:/T-Systems/pdf/ticket" + ticketId + ".pdf";
            response.setHeader("Content-Disposition", "outline;filename=\"" +"ticket" + ticketId + ".pdf"+ "\"");
            ticketService.createPDF(userDTO, userService.getUserTicketByTrain(userDTO.getId(), ticketDTO.getTrainId()), response);
        }

    }


}
