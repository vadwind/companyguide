<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 23.04.14
  Time: 2:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/searchTrain.js"></script>
    <title>Search train</title>
</head>
<body>
<c:set var="activeMenu" value="search" />
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">
    <div class="row">
        <div class="span5">
            <%@ include file="/WEB-INF/pages/includes/_searchForm.jsp" %>
        </div>
        <div class="span7">
            <div class="popin">
                <div id="map"></div>
            </div>
        </div>
    </div>

    <c:if test="${!empty ticketsList}">
        <h3>Trains</h3>
        <table id="tableSmart" class="table table-hover">
            <thead>
                <tr>
                    <th>Train</th>
                    <th>Departure date and time</th>
                    <th>Arrive date and time</th>
                    <th>Vacancies</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${ticketsList}" var="ticket">
                <tr>
                    <td><a href="/Trains/train/view/${ticket.trainId}">${ticket.train} ${ticket.startStationTrain} - ${ticket.endStationTrain}</a></td>
                    <td>${ticket.dateDeparture} ${ticket.timeDeparture}</td>
                    <td>${ticket.dateArrive} ${ticket.timeArrive}</td>
                    <td>${ticket.vacancies}</td>
                    <td><a href="/Trains/ticket/buy/${ticket.trainId}"><button class="btn btn-warning">Buy ticket</button></a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
</body>
</html>