<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 24.04.14
  Time: 14:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
    <title>Admin panel</title>
</head>
<body>
<c:set var="activeMenu" value="admin" />
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">

        <div class="tabbable tabs-left">
            <h2>Admin panel</h2>
            <ul class="nav nav-tabs">
                <li><a href="/Trains/station/admin">Management of stations</a></li>
                <li><a href="/Trains/train/admin">Management of trains</a></li>
                <li><a href="/Trains/user/admin">Management of users</a></li>
            </ul>
            <div class="tab-content">

            </div>
        </div>
    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
</body>
</html>