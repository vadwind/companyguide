<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 13.04.14
  Time: 23:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
    <title>Station admin</title>
</head>
<body>
<c:set var="activeMenu" value="admin" />
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">
    <h1>Manage of stations</h1>
    <a href="<c:url value="/station/create" />"><button class="btn btn-large btn-info">Add new station</button></a>
    <h3>List of stations</h3>
    <c:if test="${!empty stationList}">
    <table id="tableSmart" class="table table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <c:forEach items="${stationList}" var="station">
            <tr>
                <td>${station.id}</td>
                <td>${station.name}</td>
                <td><a href="view/${station.id}"><button class="btn btn-warning">View timetable</button></a></td>
                <td><a href="update/${station.id}"><button class="btn">Update</button></a></td>
                <td><a href="delete/${station.id}"><button class="btn btn-danger">Delete</button></a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    </c:if>
    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
</body>
</html>
