<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 17.04.14
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Tickets of ${user.surname} ${user.name}</title>
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
</head>
<body>
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">
    <h1>User ${user.surname} ${user.name}</h1>
    <h3>Tickets</h3>
    <c:if test="${!empty ticketsList}">
        <table id="tableSmart" class="table table-hover">
            <thead>
            <tr>
                <th>Train</th>
                <th>Start station</th>
                <th>End station</th>
                <th>Departure date and time</th>
                <th>Arrive date and time</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${ticketsList}" var="ticket">
                <tr>
                    <td><a href="/Trains/train/view/${ticket.trainId}">${ticket.train} ${ticket.startStationTrain} - ${ticket.endStationTrain}</a></td>
                    <td><a href="/Trains/station/view/name=${ticket.startStation}">${ticket.startStation}</a></td>
                    <td><a href="/Trains/station/view/name=${ticket.endStation}">${ticket.endStation}</a></td>
                    <td>${ticket.dateDeparture} ${ticket.timeDeparture}</td>
                    <td>${ticket.dateArrive} ${ticket.timeArrive}</td>
                    <td><a href="/Trains/ticket/print/${ticket.id}"><button class="btn btn-small btn-warning">Print ticket</button></a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
</body>
</html>