<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 25.04.14
  Time: 9:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
    <title>User admin</title>
</head>
<body>
<c:set var="activeMenu" value="admin" />
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">
    <h1>Manage of users</h1>
    <c:if test="${!empty userList}">
        <h3>List of users</h3>
        <table id="tableSmart" class="table table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Login</th>
                <th>Role</th>
                <th>Surname</th>
                <th>Name</th>
                <th>Birthday</th>
                <th>Email</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${userList}" var="user">
                <tr>
                    <td>${user.id}</td>
                    <td>${user.login}</td>
                    <td>${user.role}</td>
                    <td>${user.surname}</td>
                    <td>${user.name}</td>
                    <td>${user.birthday}</td>
                    <td>${user.email}</td>
                    <td><a href="updateAdmin/${user.id}"><button class="btn">Update</button></a></td>
                    <td><a href="delete/${user.id}"><button class="btn btn-danger">Delete</button></a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
</body>
</html>