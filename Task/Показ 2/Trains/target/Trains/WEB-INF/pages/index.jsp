<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 24.04.14
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/searchTrain.js"></script>
    <title>Trains</title>
</head>
<body>
<c:set var="activeMenu" value="index" />
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">
    <div class="row">
        <div class="span4">

                <%@ include file="/WEB-INF/pages/includes/_searchForm.jsp" %>
        </div>
        <div class="span8">
            <div class="hero-unit popin">
                <!-- Carousel
                ================================================== -->
                <div id="myCarousel" class="carousel slide">
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="../Trains/img/slide-01.jpg" alt="">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>Speed</h1>
                                    <p class="lead">Our trains are the fastest in Western Europe</p>
                                    <a class="btn btn-my" href="/Trains/ticket/search">Search and buy ticket</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <img src="../Trains/img/slide-02.jpg" alt="">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>Quality</h1>
                                    <p class="lead">Passengers are always happy in our trains</p>
                                    <a class="btn btn-my" href="/Trains/user/registration">Sign up and buy ticket</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <img src="../Trains/img/slide-03.jpg" alt="">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>Safety</h1>
                                    <p class="lead">No accident in the history of the company</p>
                                    <a class="btn btn-my" href="/Trains/ticket/search">Search and buy ticket</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
                </div><!-- /.carousel -->
            </div>
            <div class="row-fluid">
                <div class="span4">
                    <h2>Timetable</h2>
                    <p>You can view timetable trains of all stations.</p>
                    <p><a class="btn" href="/Trains/station/index">View details »</a></p>
                </div><!--/span-->
                <div class="span4">
                    <h2>All trains</h2>
                    <p>If you want, you can view all trains our company</p>
                    <p><a class="btn" href="/Trains/train/index">View details »</a></p>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    <h2>Search ticket</h2>
                    <p>Get a train ticket</p>
                    <p><a class="btn" href="/Trains/ticket/search">View details »</a></p>
                </div><!--/span-->
                <div class="span4">
                    <h2>Sign up</h2>
                    <p>Sign up on our website! And you can buy ticket</p>
                    <p><a class="btn" href="/Trains/user/registration">View details »</a></p>
                </div>
            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
<script>
    !function ($) {
        $(function(){
            // carousel demo
            $('#myCarousel').carousel()
        })
    }(window.jQuery)
</script>
</body>
</html>