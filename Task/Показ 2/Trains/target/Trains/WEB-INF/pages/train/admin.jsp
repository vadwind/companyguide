<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 13.04.14
  Time: 23:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
    <title>Train admin</title>
</head>
<body>
<c:set var="activeMenu" value="admin" />
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">
    <h1>Manage of trains</h1>
    <a href="<c:url value="/train/create" />"><button class="btn btn-large btn-info">Add new train</button></a>
    <h3>List of trains</h3>
    <c:if test="${!empty trainList}">
        <table id="tableSmart" class="table table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Number</th>
                <th>Start station</th>
                <th>End station</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${trainList}" var="train">
                <tr>
                    <td>${train.id}</td>
                    <td><a href="view/${train.id}">${train.trainNumber}</a></td>
                    <td><a href="/Trains/station/view/name=${train.startStation}">${train.startStation}</a></td>
                    <td><a href="/Trains/station/view/name=${train.endStation}">${train.endStation}</a></td>
                    <td><a href="passengers/${train.id}"><button class="btn btn-warning">View passengers</button></a></td>
                    <td><a href="update/${train.id}"><button class="btn">Update</button></a></td>
                    <td><a href="delete/${train.id}"><button class="btn btn-danger">Delete</button></a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
</body>
</html>