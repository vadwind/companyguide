<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 16.04.14
  Time: 20:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Train ${train.trainNumber}</title>
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/viewTrain.js"></script>
</head>
<body>
<c:set var="activeMenu" value="train" />
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">
        <div id="trainId" hidden>${train.id}</div>
        <h1>Train ${train.trainNumber}</h1>
        <h5>Count of passengers in train ${train.countPassengers}</h5>
        <h3>Timetable</h3>
    <div class="row">
        <div class="span6">
            <c:if test="${!empty timetableList}">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Station</th>
                            <th>Time arrive</th>
                            <th>Time departure</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${timetableList}" var="timetable">
                            <tr>
                                <td><a href="/Trains/station/view/name=${timetable.station}">${timetable.station}</a></td>
                                <td>${timetable.timeArrive}</td>
                                <td>${timetable.timeDeparture}</td>
                                <td>${timetable.date}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
        <div class="span6">
            <div class="popin">
                <div id="map"></div>
            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
</body>
</html>