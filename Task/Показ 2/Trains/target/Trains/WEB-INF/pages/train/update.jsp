<%--
  Created by IntelliJ IDEA.
  User: Вадим
  Date: 17.04.14
  Time: 0:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Train ${train.trainNumber}</title>
    <%@ include file="/WEB-INF/pages/layouts/_head.jsp" %>
</head>
<body>
<c:set var="activeMenu" value="admin" />
<%@ include file="/WEB-INF/pages/layouts/_header.jsp" %>
<div class="container">
    <h1>Update train id ${train.id}</h1>
    <form:form method="post" action="${pageContext.request.contextPath}/train/refresh" commandName="train">
        <form:label path="trainNumber">Train number</form:label>
        <form:input path="trainNumber" />
        <form:hidden path="id"/>
        <form:hidden path="countPassengers"/>

        <input class="btn" type="submit" value="Save" />

    </form:form>
    <%@ include file="/WEB-INF/pages/layouts/_footer.jsp" %>
</div>
</body>
</html>