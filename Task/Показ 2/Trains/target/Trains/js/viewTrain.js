/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 01.05.14
 * Time: 11:36
 * To change this template use File | Settings | File Templates.
 */
var map;
var namesStations = new Array();
var coordinats = new Array();

$(document).ready(function(){
    $.ajax({
        url: '/Trains/train/getStationsCoord',
        type: 'POST',
        data: ({
            trainId: $('#trainId').text()
        }),
        success: function (data) {
            if(data != '') {
                var stations = JSON.parse(data);
                var i = 0;
                var centerLat;
                var centerLng
                for(var key in stations){
                    namesStations[i] = stations[key]['name'];
                    var arraySt = new Array(stations[key]['lat'], stations[key]['lng'])
                    if(namesStations[i] != 'center')
                        coordinats.push(arraySt);
                    else {
                        centerLat = arraySt[0];
                        centerLng = arraySt[1];
                    }
                    i ++;
                }

                map = new GMaps({
                    div: '#map',
                    lat: centerLat,
                    lng: centerLng,
                    zoom: 4,
                    click: function(e){
                        console.log(e);
                    }
                });

                map.drawPolyline({
                    path: coordinats,
                    strokeColor: '#FF0080',
                    strokeOpacity: 0.6,
                    strokeWeight: 6
                });

                for(i = 0; i < namesStations.length - 1; i ++) {
                    map.drawOverlay({
                        lat: coordinats[i][0],
                        lng: coordinats[i][1],
                        content: '<div class="overlay">'+namesStations[i]+'</div>'
                    });
                    map.addMarker({
                        lat: coordinats[i][0],
                        lng: coordinats[i][1],
                        title: namesStations[i]
                    });

                }
            }
        }
    });
});
