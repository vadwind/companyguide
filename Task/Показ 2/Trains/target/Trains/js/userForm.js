/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 25.04.14
 * Time: 22:35
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {
    $("#user").validate({
        rules: {
            login: {
                required: true,
                minlength: 2,
                maxlength: 50
            },
            password: {
                required: true,
                minlength: 2,
                maxlength: 50
            },

            repeatPassword: {
                equalTo: "#password"

            },

            surname: {
                required: true,
                minlength: 2,
                maxlength: 50
            },
            name: {
                required: true,
                minlength: 2,
                maxlength: 50
            },
            birthday: {
                required: true
            }
        },
        messages: {
            login: {
                required: "It`s field required",
                minlength: "Minimum 2 symbols",
                maxlength: "Maximum 50 symbols"
            },
            password: {
                required: "It`s field required",
                minlength: "Minimum 2 symbols",
                maxlength: "Maximum 50 symbols"
            },
            repeatPassword: {
                equalTo: "Passwords don`t match"
            },
            surname: {
                required: "It`s field required",
                minlength: "Minimum 2 symbols",
                maxlength: "Maximum 50 symbols"
            },
            name: {
                required: "It`s field required",
                minlength: "Minimum 2 symbols",
                maxlength: "Maximum 50 symbols"
            },
            birthday: {
                required: "It`s field required"
            }
        }
    });
});