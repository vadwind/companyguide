/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 20.04.14
 * Time: 11:05
 * To change this template use File | Settings | File Templates.
 */

$(document).ready(function () {
    $('#addstation').click(function () {
        $('#myModal').modal('show');
    });
});

$(document).ready(function () {
    $('#savestation').click(function () {
        var stationName = $('#modstation :selected').text();
        var date = $('#moddate').val();
        var timeArrive = $('#modtimearrive').val();
        var timeDeparture = $('#modtimedeparture').val();
        $.ajax({
            url: '/Trains/train/station',
            type: 'POST',
            data: ({
                stationName: stationName,
                date: date,
                timeArrive: timeArrive,
                timeDeparture: timeDeparture
            }),
            success: function (data) {
                $('#myModal').modal('hide');
                if(data.station != null) {
                    var stringToTable = "<tr><td>" + data.station + "</td>";
                    stringToTable += "<td>" + data.date + "</td>";
                    stringToTable += "<td>" + data.timeArrive + "</td>";
                    stringToTable += "<td>" + data.timeDeparture + "</td>";
                    stringToTable += "<td><button class='btn btn-danger' onclick="/deleteStation('" + data.station + "') /">Delete</button></td></tr>";
                    $("#bodyTable").append(stringToTable);
                    $("#errorModalBody").append("Station successfully added");
                }
                else
                    $("#errorModalBody").append(data.date);
                $('#errorMyModal').modal('show');
            }
        });
    });

});

$(document).ready(function () {
    $('#clearTimetable').click(function () {
        $("#bodyTable").empty();
        $.ajax({
            url: '/Trains/train/cleartimetable',
            type: 'POST',
            data: ({
            })
        });
        $("#bodyTable").empty();
    });
});

$(document).ready(function () {
    $('#errorButton').click(function () {
        $('#errorMyModal').modal('hide');
        $("#errorModalBody").empty();
    });
});

function deleteStation(nameStation) {
    document.location.href = "/Trains/train/clearStation/"+nameStation;
}


