package com.tsystems.trains.common.client;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 28.03.14
 * Time: 19:48
 * To change this template use File | Settings | File Templates.
 */

import java.io.Serializable;

/**
 * Command for view trains on station
 */
public class ViewTrainsOnStationCommand implements Serializable{

    private String nameStation;
    private String[][] trains;

    public ViewTrainsOnStationCommand(String nameStation) {
        this.nameStation = nameStation;
    }

    public String getNameStation() {
        return nameStation;
    }

    public void setNameStation(String nameStation) {
        this.nameStation = nameStation;
    }

    public String[][] getTrains() {
        return trains;
    }

    public void setTrains(String[][] tickets) {
        this.trains = tickets;
    }
}
