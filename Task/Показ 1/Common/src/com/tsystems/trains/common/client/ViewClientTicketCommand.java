package com.tsystems.trains.common.client;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 28.03.14
 * Time: 14:26
 * To change this template use File | Settings | File Templates.
 */

import java.io.Serializable;

/**
 * Command for view ticket
 */
public class ViewClientTicketCommand implements Serializable{

    private Long userId;
    private String[][] tickets;

    public ViewClientTicketCommand(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String[][] getTickets() {
        return tickets;
    }

    public void setTickets(String[][] tickets) {
        this.tickets = tickets;
    }
}
