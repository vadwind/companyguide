package com.tsystems.trains.common.manager;

import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 30.03.14
 * Time: 18:34
 * To change this template use File | Settings | File Templates.
 */
public class AddTrainCommand implements Serializable {

    private String[][] stations;
    private String trainNumber;
    private int countPassengers;
    private Date date;

    private boolean result;

    /**
     * Constructor
     * @param stations Stations
     * @param date Date
     * @param countPassengers Count of passengers
     * @param trainNumber Number of train
     */
    public AddTrainCommand(String[][] stations, Date date, int countPassengers, String trainNumber) {
        this.stations = stations;
        this.date = date;
        this.countPassengers = countPassengers;
        this.trainNumber = trainNumber;
    }

    public String[][] getStations() {
        return stations;
    }

    public void setStations(String[][] stations) {
        this.stations = stations;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public int getCountPassengers() {
        return countPassengers;
    }

    public void setCountPassengers(int countPassengers) {
        this.countPassengers = countPassengers;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
