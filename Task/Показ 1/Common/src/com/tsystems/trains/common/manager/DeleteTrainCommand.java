package com.tsystems.trains.common.manager;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 30.03.14
 * Time: 20:30
 * To change this template use File | Settings | File Templates.
 */

/**
 * Class command for deleting train
 */
public class DeleteTrainCommand implements Serializable {

    private String trainNumber;
    private boolean result;

    /**
     * Constructor
     * @param trainNumber
     */
    public DeleteTrainCommand(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
