package com.tsystems.trains.common.manager;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 29.03.14
 * Time: 20:18
 * To change this template use File | Settings | File Templates.
 */

/**
 * Command for viewing passengers of train
 */
public class ViewAllPassengersCommand implements Serializable {

    private String trainNumber;
    private String[][] passengers;

    /**
     * Constructor
     * @param trainNumber
     */
    public ViewAllPassengersCommand(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String[][] getPassengers() {
        return passengers;
    }

    public void setPassengers(String[][] passengers) {
        this.passengers = passengers;
    }
}
