package com.tsystems.trains.common.manager;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 29.03.14
 * Time: 16:18
 * To change this template use File | Settings | File Templates.
 */

/**
 * Command for adding station
 */
public class AddStationCommand implements Serializable {

    private String stationName;
    private boolean result;

    /**
     * Constructor
     * @param stationName Name of station
     */
    public AddStationCommand(String stationName) {
        this.stationName = stationName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
