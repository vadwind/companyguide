package com.tsystems.trains.common.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 28.03.14
 * Time: 22:41
 * To change this template use File | Settings | File Templates.
 */

/**
 * Command for searching ticket
 */
public class SearchTicketsCommand implements Serializable {

    private String stationArrive;
    private String stationDeparture;
    private Date dateFrom;
    private Date dateTo;

    private String info;
    private String[][] resultTickets;

    /**
     * Constructor
     * @param stationArrive station of arrive
     * @param stationDeparture station of departure
     * @param dateFrom date from
     * @param dateTo date to
     */
    public SearchTicketsCommand(String stationArrive, String stationDeparture, Date dateFrom, Date dateTo) {
        this.stationArrive = stationArrive;
        this.stationDeparture = stationDeparture;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public String getStationArrive() {
        return stationArrive;
    }

    public void setStationArrive(String stationArrive) {
        this.stationArrive = stationArrive;
    }

    public String getStationDeparture() {
        return stationDeparture;
    }

    public void setStationDeparture(String stationDeparture) {
        this.stationDeparture = stationDeparture;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String[][] getResultTickets() {
        return resultTickets;
    }

    public void setResultTickets(ArrayList<String[]> resultTickets) {
        String[][] result = new String[resultTickets.size()][4];
        int i = 0;
        for(String[] s: resultTickets) {
            result[i] = resultTickets.get(i);
        }
        this.resultTickets = result;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
