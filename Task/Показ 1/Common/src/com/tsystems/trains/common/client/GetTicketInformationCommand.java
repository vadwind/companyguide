package com.tsystems.trains.common.client;

import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 31.03.14
 * Time: 10:38
 * To change this template use File | Settings | File Templates.
 */

/**
 * Command for ptinting ticket
 */
public class GetTicketInformationCommand implements Serializable {

    private String ticketNumber;

    private String passengerSurname;
    private String passengerName;
    private Date passengerBirthday;
    private String trainNumber;
    private String stationTrainArrive;
    private String stationTrainDeparture;
    private Date date;
    private String stationDeparture;
    private String stationArrive;
    private String timeDeparture;
    private String timeArrive ;

    /**
     * Constructor
     * @param ticketNumber
     */
    public GetTicketInformationCommand(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getPassengerSurname() {
        return passengerSurname;
    }

    public void setPassengerSurname(String passengerSurname) {
        this.passengerSurname = passengerSurname;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public Date getPassengerBirthday() {
        return passengerBirthday;
    }

    public void setPassengerBirthday(Date passengerBirthday) {
        this.passengerBirthday = passengerBirthday;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getStationTrainArrive() {
        return stationTrainArrive;
    }

    public void setStationTrainArrive(String stationTrainArrive) {
        this.stationTrainArrive = stationTrainArrive;
    }

    public String getStationTrainDeparture() {
        return stationTrainDeparture;
    }

    public void setStationTrainDeparture(String stationTrainDeparture) {
        this.stationTrainDeparture = stationTrainDeparture;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStationDeparture() {
        return stationDeparture;
    }

    public void setStationDeparture(String stationDeparture) {
        this.stationDeparture = stationDeparture;
    }

    public String getStationArrive() {
        return stationArrive;
    }

    public void setStationArrive(String stationArrive) {
        this.stationArrive = stationArrive;
    }

    public String getTimeDeparture() {
        return timeDeparture;
    }

    public void setTimeDeparture(String timeDeparture) {
        this.timeDeparture = timeDeparture;
    }

    public String getTimeArrive() {
        return timeArrive;
    }

    public void setTimeArrive(String timeArrive) {
        this.timeArrive = timeArrive;
    }
}
