package com.tsystems.trains.common.client;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 28.03.14
 * Time: 1:24
 * To change this template use File | Settings | File Templates.
 */
public class CheckUserCommand implements Serializable {

    private Long id;
    private String login;
    private String password;
    private String role;
    private String surname;
    private String name;
    private Date birthday;

    public CheckUserCommand(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Map<String, String> getMap() {
        HashMap<String, String> resultMap = new HashMap<String, String>();
        resultMap.put("id", id.toString());
        resultMap.put("login", login);
        resultMap.put("password", password);
        resultMap.put("name", name);
        resultMap.put("surname", surname);
        resultMap.put("role", role);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        resultMap.put("birthday",  dateFormat.format(birthday));

        return resultMap;
    }
}
