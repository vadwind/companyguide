package com.tsystems.trains.common.manager;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 29.03.14
 * Time: 16:03
 * To change this template use File | Settings | File Templates.
 */

import java.io.Serializable;

/**
 * Command class for getting all trains
 */
public class GetAllTrainsCommand implements Serializable {
    private String[] commands;
    private String[][] allTrainsWithDescription;
    private boolean withDescription;

    public String[] getCommands() {
        return commands;
    }

    public void setCommands(String[] commands) {
        this.commands = commands;
    }

    public String[][] getAllTrainsWithDescription() {
        return allTrainsWithDescription;
    }

    public void setAllTrainsWithDescription(String[][] allTrainsWithDescription) {
        this.allTrainsWithDescription = allTrainsWithDescription;
    }

    public boolean isWithDescription() {
        return withDescription;
    }

    public void setWithDescription(boolean withDescription) {
        this.withDescription = withDescription;
    }
}