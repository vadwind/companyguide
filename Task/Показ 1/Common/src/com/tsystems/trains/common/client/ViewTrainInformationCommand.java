package com.tsystems.trains.common.client;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 28.03.14
 * Time: 21:05
 * To change this template use File | Settings | File Templates.
 */

/**
 * Command for view information about train
 */
public class ViewTrainInformationCommand implements Serializable {

    private String trainNumber;
    private String trainInformation;
    private String[][] stations;

    public ViewTrainInformationCommand(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getTrainInformation() {
        return trainInformation;
    }

    public void setTrainInformation(String trainInformation) {
        this.trainInformation = trainInformation;
    }

    public String[][] getStations() {
        return stations;
    }

    public void setStations(String[][] stations) {
        this.stations = stations;
    }
}
