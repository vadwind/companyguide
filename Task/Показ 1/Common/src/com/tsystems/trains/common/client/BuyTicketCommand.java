package com.tsystems.trains.common.client;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 29.03.14
 * Time: 1:53
 * To change this template use File | Settings | File Templates.
 */

import java.io.Serializable;

/**
 * Command for buying ticket
 */
public class BuyTicketCommand implements Serializable{

    private Long userId;
    private String stationArrive;
    private String stationDeparture;
    private String trainNumber;
    private boolean result;

    /**
     * Constructor
     * @param userId User
     * @param stationArrive  Station of arrive
     * @param stationDeparture Station of departure
     * @param trainNumber Train
     */
    public BuyTicketCommand(Long userId, String stationArrive, String stationDeparture, String trainNumber) {
        this.userId = userId;
        this.stationArrive = stationArrive;
        this.stationDeparture = stationDeparture;
        this.trainNumber = trainNumber;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStationArrive() {
        return stationArrive;
    }

    public void setStationArrive(String stationArrive) {
        this.stationArrive = stationArrive;
    }

    public String getStationDeparture() {
        return stationDeparture;
    }

    public void setStationDeparture(String stationDeparture) {
        this.stationDeparture = stationDeparture;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
