package com.tsystems.trains.common.client;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 28.03.14
 * Time: 15:53
 * To change this template use File | Settings | File Templates.
 */

/**
 * Command class for getting all stations
 */
public class GetAllStationsCommand implements Serializable {
    String[] commands;

    public String[] getCommands() {
        return commands;
    }

    public void setCommands(String[] commands) {
        this.commands = commands;
    }
}
