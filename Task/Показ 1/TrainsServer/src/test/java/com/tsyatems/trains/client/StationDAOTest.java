package com.tsyatems.trains.client;


import com.tsystems.trains.common.manager.AddStationCommand;
import com.tsystems.trains.common.manager.DeleteStationCommand;
import com.tsystems.trains.server.dao.StationDAO;
import com.tsystems.trains.server.entity.Station;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 31.03.14
 * Time: 13:56
 * To change this template use File | Settings | File Templates.
 */
public class StationDAOTest {
    private EntityManagerFactory emf;
    private EntityManager em;
    private StationDAO stationDAO;

    private String stationName = "test";

    @Before
    public void initStationDAO() {
        emf = Persistence.createEntityManagerFactory("Trains");
        em = emf.createEntityManager();
        stationDAO = new StationDAO(em);

    }

    /**
     * Check adding station
     */
    @Test
    public void addStation() {
        AddStationCommand addStationCommand = new AddStationCommand(stationName);
        stationDAO.addStation(addStationCommand);
        Query nativeQuery = em.createNativeQuery("SELECT * FROM station WHERE name=?", Station.class);
        nativeQuery.setParameter(1, addStationCommand.getStationName());
        List<Station> resultList = nativeQuery.getResultList();
        Integer resultCount = resultList.size();
        Assert.assertEquals(Integer.valueOf(1), resultCount);
    }

    /**
     * Check deleting station
     */
    @Test
    public void deleteStation() {
        DeleteStationCommand deleteStationCommand = new DeleteStationCommand((stationName));
        stationDAO.deleteStation(deleteStationCommand);
        Query nativeQuery = em.createNativeQuery("SELECT * FROM station WHERE name=?", Station.class);
        nativeQuery.setParameter(1, deleteStationCommand.getStationName());
        List<Station> resultList = nativeQuery.getResultList();
        Integer resultCount = resultList.size();
        Assert.assertEquals(Integer.valueOf(0), resultCount);

    }


}