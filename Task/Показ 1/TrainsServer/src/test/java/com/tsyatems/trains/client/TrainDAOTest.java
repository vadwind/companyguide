package com.tsyatems.trains.client;

import com.tsystems.trains.common.manager.AddTrainCommand;
import com.tsystems.trains.common.manager.DeleteTrainCommand;
import com.tsystems.trains.server.dao.TrainDAO;
import com.tsystems.trains.server.entity.Train;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 31.03.14
 * Time: 14:15
 * To change this template use File | Settings | File Templates.
 */
public class TrainDAOTest {
    private EntityManagerFactory emf;
    private EntityManager em;
    private TrainDAO trainDAO;

    private String trainNumber = "test";
    private String[][] stations = {{"Moscow", "10:10", "10:10"}, {"Saint-Petersburg", "12:00", "12:00"}};
    private int countPassengers = 10;

    @Before
    public void initStationDAO() {
        emf = Persistence.createEntityManagerFactory("Trains");
        em = emf.createEntityManager();
        trainDAO = new TrainDAO(em);

    }

    /**
     * Check adding train
     */
    @Test
    public void addTrain() {
        AddTrainCommand addTrain = new AddTrainCommand(stations, new Date(), countPassengers, trainNumber);
        trainDAO.addTrain(addTrain);
        Query nativeQuery = em.createNativeQuery("SELECT * FROM train WHERE train_number=?", Train.class);
        nativeQuery.setParameter(1, addTrain.getTrainNumber());
        List<Train> resultList = nativeQuery.getResultList();
        Integer resultCount = resultList.size();
        Assert.assertEquals(Integer.valueOf(1), resultCount);
    }

    /**
     * Check deleting train
     */
    @Test
    public void deleteTrain() {
        DeleteTrainCommand deleteTrain = new DeleteTrainCommand(trainNumber);
        trainDAO.deleteTrain(deleteTrain);
        Query nativeQuery = em.createNativeQuery("SELECT * FROM train WHERE train_number=?", Train.class);
        nativeQuery.setParameter(1, deleteTrain.getTrainNumber());
        List<Train> resultList = nativeQuery.getResultList();
        Integer resultCount = resultList.size();
        Assert.assertEquals(Integer.valueOf(0), resultCount);
    }
}