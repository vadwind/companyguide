package com.tsystems.trains.server;

import com.tsystems.trains.common.client.*;
import com.tsystems.trains.common.manager.*;
import com.tsystems.trains.server.dao.*;
import org.apache.log4j.Logger;


import javax.persistence.EntityManager;
import java.io.*;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 26.03.14
 * Time: 5:14
 * To change this template use File | Settings | File Templates.
 */
public class ClientConnectionThread implements Runnable {

    private static Logger logger = Logger.getLogger(ClientConnectionThread.class);

    private Socket client;
    private Thread t;
    private EntityManager em;

    /**
     * constructor
     * @param client  client`s socket
     */
    public ClientConnectionThread(Socket client, EntityManager em) {
        this.client = client;
        this.em = em;
        t = new Thread(this);
        t.start();
    }

    /**
     * start client`s thread
     */
    public void run() {
        try {
            logger.info("New connection from "+client.getLocalSocketAddress());
            Object toClient = null;
            //receive from client
            ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(client.getInputStream()));
            Object objectFromClient = (Object)input.readObject();

            //perform command
            if(objectFromClient instanceof CheckUserCommand) {
                UserDAO userDao = new UserDAO((CheckUserCommand)objectFromClient , em);
                toClient = userDao.checkUser();

            }
            if(objectFromClient instanceof RegistrationCommand) {
                UserDAO userDao = new UserDAO((RegistrationCommand)objectFromClient , em);
                toClient = userDao.registrationUser();
            }
            if(objectFromClient instanceof ChangeUserDataCommand) {
                UserDAO userDao = new UserDAO((ChangeUserDataCommand)objectFromClient,em);
                toClient = userDao.changeUserData();
            }

            if(objectFromClient instanceof ViewClientTicketCommand) {
                TicketDAO ticketDao = new TicketDAO((ViewClientTicketCommand)objectFromClient , em);
                toClient = ticketDao.getUserTickets();
            }

            if(objectFromClient instanceof GetAllStationsCommand) {
                StationDAO stationDao = new StationDAO(em);
                toClient = stationDao.getAllStations();
            }

            if(objectFromClient instanceof GetAllTrainsCommand) {
                TrainDAO trainDao = new TrainDAO(em);
                //with description yes or no
                if(!((GetAllTrainsCommand) objectFromClient).isWithDescription())
                    toClient = trainDao.getAllTrains(false);
                else
                    toClient = trainDao.getAllTrains(true);
            }

            if(objectFromClient instanceof ViewTrainsOnStationCommand) {
                TimetableDAO timetableDAO = new TimetableDAO(em);
                toClient = timetableDAO.getTimeTableStation((ViewTrainsOnStationCommand)objectFromClient);
            }

            if(objectFromClient instanceof ViewTrainInformationCommand) {
                TimetableDAO timetableDAO = new TimetableDAO(em);
                toClient = timetableDAO.getTrainInformation((ViewTrainInformationCommand) objectFromClient);
            }

            if(objectFromClient instanceof SearchTicketsCommand) {
                TimetableDAO timetableDAO = new TimetableDAO(em);
                toClient = timetableDAO.searchTickets((SearchTicketsCommand) objectFromClient);
            }

            if(objectFromClient instanceof BuyTicketCommand) {
                TicketDAO ticketDAO = new TicketDAO(em);
                toClient = ticketDAO.buyTicket((BuyTicketCommand) objectFromClient);
            }

            if(objectFromClient instanceof AddStationCommand) {
                StationDAO stationDAO = new StationDAO(em);
                toClient = stationDAO.addStation((AddStationCommand) objectFromClient);
            }

            if(objectFromClient instanceof DeleteStationCommand) {
                StationDAO stationDAO = new StationDAO(em);
                toClient = stationDAO.deleteStation((DeleteStationCommand) objectFromClient);
            }

            if(objectFromClient instanceof ViewAllPassengersCommand) {
                UserDAO userDAO = new UserDAO(em);
                toClient = userDAO.getPassengersTrain((ViewAllPassengersCommand) objectFromClient);
            }

            if(objectFromClient instanceof AddTrainCommand) {
                TrainDAO trainDAO = new TrainDAO(em);
                toClient = trainDAO.addTrain((AddTrainCommand) objectFromClient);
            }

            if(objectFromClient instanceof DeleteTrainCommand) {
                TrainDAO trainDAO = new TrainDAO(em);
                toClient = trainDAO.deleteTrain((DeleteTrainCommand) objectFromClient);
            }

            if(objectFromClient instanceof GetTicketInformationCommand) {
                TicketDAO ticketDAO = new TicketDAO(em);
                toClient = ticketDAO.getFullInformationAboutTicket((GetTicketInformationCommand) objectFromClient);
            }

            ObjectOutputStream output = new ObjectOutputStream(new BufferedOutputStream(client.getOutputStream()));
            output.writeObject(toClient);
            output.flush();
        }
        catch(IOException e) {
            logger.error(e.getMessage());
        }
        catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
    }
}
