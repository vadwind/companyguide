package com.tsystems.trains.server.dao;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 28.03.14
 * Time: 13:38
 * To change this template use File | Settings | File Templates.
 */

import com.tsystems.trains.common.client.ChangeUserDataCommand;
import com.tsystems.trains.common.client.CheckUserCommand;
import com.tsystems.trains.common.client.RegistrationCommand;
import com.tsystems.trains.common.manager.ViewAllPassengersCommand;
import com.tsystems.trains.server.entity.Ticket;
import com.tsystems.trains.server.entity.Train;
import com.tsystems.trains.server.entity.User;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Implements DAO class for user
 */
public class UserDAO {

    private static Logger logger = Logger.getLogger(UserDAO.class);

    private EntityManager entityManager;
    private User beginUser;

    /**
     * Constructor
     * @param em EntityManager
     */
    public UserDAO(EntityManager em) {
        this.entityManager = em;
    }

    /**
     * Constructor
     * @param checkUserCommand Command
     * @param em EntityManager
     */
    public UserDAO(CheckUserCommand checkUserCommand, EntityManager em) {
        this.entityManager = em;
        beginUser = new User();
        beginUser.setLogin(checkUserCommand.getLogin());
        beginUser.setPassword(checkUserCommand.getPassword());
    }

    /**
     * Constructor
     * @param registrationCommand Command
     * @param em EntityManager
     */
    public UserDAO(RegistrationCommand registrationCommand, EntityManager em) {
        this.entityManager = em;
        beginUser = new User();
        beginUser.setLogin(registrationCommand.getLogin());
        beginUser.setPassword(registrationCommand.getPassword());
        beginUser.setBirthday(registrationCommand.getBirthday());
        beginUser.setName(registrationCommand.getName());
        beginUser.setRole(registrationCommand.getRole());
        beginUser.setSurname(registrationCommand.getSurname());
    }

    /**
     * Constructor
     * @param changeUserDataCommand Command
     * @param em EntityManager
     */
    public UserDAO(ChangeUserDataCommand changeUserDataCommand, EntityManager em) {
        this.entityManager = em;
        beginUser = new User();
        beginUser.setId(changeUserDataCommand.getId());
        beginUser.setLogin(changeUserDataCommand.getLogin());
        beginUser.setPassword(changeUserDataCommand.getPassword());
        beginUser.setBirthday(changeUserDataCommand.getBirthday());
        beginUser.setName(changeUserDataCommand.getName());
        beginUser.setRole(changeUserDataCommand.getRole());
        beginUser.setSurname(changeUserDataCommand.getSurname());
    }

    /**
     * Check user in db
     * @return CheckUserCommand with user
     */
    public CheckUserCommand checkUser() {
        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM user WHERE login=? AND password=?", User.class);
        nativeQuery.setParameter(1, beginUser.getLogin());
        nativeQuery.setParameter(2, beginUser.getPassword());
        List<User> resultList = nativeQuery.getResultList();
        if(!resultList.isEmpty()) {
            User user = resultList.get(0);
            CheckUserCommand result = new CheckUserCommand(user.getLogin(), user.getPassword());
            result.setId(user.getId());
            result.setSurname(user.getSurname());
            result.setName(user.getName());
            result.setRole(user.getRole());
            result.setBirthday(user.getBirthday());
            logger.info("Log in user " + beginUser.getLogin());
            return result;
        }
        logger.warn("Log in user " + beginUser.getLogin() + ". Fail");
        return null;
    }

    /**
     * Registered user in db
     * @return success query
     */
    public boolean registrationUser() {
        //check user in db
        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM user WHERE surname=? AND name=? AND DATE_FORMAT(birthday,'%Y-%m-%d')=?", User.class);
        nativeQuery.setParameter(1, beginUser.getSurname());
        nativeQuery.setParameter(2, beginUser.getName());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        nativeQuery.setParameter(3, dateFormat.format(beginUser.getBirthday()));
        List<User> resultList = nativeQuery.getResultList();

        //if user had already registered, new record don`t create
        if(!resultList.isEmpty()) {
            logger.warn("User couldn`t registered");
            return false;
        }
        else {
            logger.info("User registered with login "+beginUser.getLogin());
            entityManager.getTransaction().begin();
            entityManager.persist(beginUser);
            entityManager.getTransaction().commit();
            return true;
        }
    }

    /**
     * Change user data
     * @return success
     */
    public boolean changeUserData() {
        entityManager.getTransaction().begin();
        User dbUser = entityManager.find(User.class, beginUser.getId());
        dbUser.setLogin(beginUser.getLogin());
        dbUser.setPassword(beginUser.getPassword());
        dbUser.setSurname(beginUser.getSurname());
        dbUser.setName(beginUser.getName());
        dbUser.setBirthday(beginUser.getBirthday());
        entityManager.getTransaction().commit();
        logger.info("User " + dbUser.getLogin() + "changed data");
        return  true;
    }

    /**
     * Get passengers of train
     * @param viewAllPassengersCommand Command
     * @return Command
     */
    public ViewAllPassengersCommand getPassengersTrain(ViewAllPassengersCommand viewAllPassengersCommand) {
        logger.info("User saw all passengers");
        //get train
        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM train WHERE train_number=?", Train.class);
        nativeQuery.setParameter(1, viewAllPassengersCommand.getTrainNumber());
        List<Train> trains = nativeQuery.getResultList();
        Train train = trains.get(0);

        //get users
        nativeQuery = entityManager.createNativeQuery("SELECT * FROM ticket WHERE train_id=?", Ticket.class);
        nativeQuery.setParameter(1, train.getId());
        List<Ticket> tickets = nativeQuery.getResultList();
        String[][] result = new String[tickets.size()][3];

        int i = 0;
        for(Ticket ticket: tickets) {
            String[] resultArrsy = new String[3];

            resultArrsy[0] = ticket.getUser().getSurname();
            resultArrsy[1] = ticket.getUser().getName();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            resultArrsy[2] = dateFormat.format(ticket.getUser().getBirthday());
            result[i] = resultArrsy;
            i ++;
        }

        viewAllPassengersCommand.setPassengers(result);
        return viewAllPassengersCommand;

    }
}
