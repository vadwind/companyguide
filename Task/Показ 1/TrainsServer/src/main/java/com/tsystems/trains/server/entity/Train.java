package com.tsystems.trains.server.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 25.03.14
 * Time: 0:34
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "train")

public class Train implements Serializable {
    @Id
    private long id;

    @Column(name = "train_number")
    private String trainNumber;

    @Column(name = "count_passengers")
    private int countPassengers;

    @OneToMany(mappedBy = "train", cascade={CascadeType.REMOVE})
    private List<Timetable> stations;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public int getCountPassengers() {
        return countPassengers;
    }

    public void setCountPassengers(int countPassengers) {
        this.countPassengers = countPassengers;
    }

    public List<Timetable> getStations() {
        return stations;
    }

    public void setStations(List<Timetable> stations) {
        this.stations = stations;
    }


}
