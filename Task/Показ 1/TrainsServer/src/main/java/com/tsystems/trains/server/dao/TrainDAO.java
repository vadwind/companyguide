package com.tsystems.trains.server.dao;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 29.03.14
 * Time: 16:07
 * To change this template use File | Settings | File Templates.
 */

import com.tsystems.trains.common.manager.AddTrainCommand;
import com.tsystems.trains.common.manager.DeleteTrainCommand;
import com.tsystems.trains.common.manager.GetAllTrainsCommand;
import com.tsystems.trains.server.TrainsServer;
import com.tsystems.trains.server.entity.Station;
import com.tsystems.trains.server.entity.Timetable;
import com.tsystems.trains.server.entity.Train;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Implements DAO class for train
 */
public class TrainDAO {

    private static Logger logger = Logger.getLogger(TrainDAO.class);

    private EntityManager entityManager;

    /**
     * Constructor EntityManager
     * @param entityManager
     */
    public TrainDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Get all trains
     * @return List
     */
    public GetAllTrainsCommand getAllTrains(boolean withDescription) {
        logger.info("User saw all trains");
        GetAllTrainsCommand result = new GetAllTrainsCommand();
        List<Train> trains = entityManager.createNativeQuery("SELECT * FROM train ORDER BY train_number ASC", Train.class).getResultList();
        //create String array
        //if without description
        if(!withDescription) {
            String[] resultArray = new String[trains.size()];
            int i = 0;
            for(Train train : trains) {
                resultArray[i] = train.getTrainNumber();
                i ++;
            }
            result.setCommands(resultArray);
        }
        else {
            //if with description
            String[][] resultArray = new String[trains.size()][4];
            int i = 0;
            for(Train train : trains) {
                String[] stringArray = new String[4];
                stringArray[0] = train.getTrainNumber();
                stringArray[1] = train.getStations().get(0).getStation().getName();
                stringArray[2] = train.getStations().get(train.getStations().size() - 1).getStation().getName();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                stringArray[3] = dateFormat.format(train.getStations().get(0).getDate());
                resultArray[i] = stringArray;
                i ++;
            }
            result.setAllTrainsWithDescription(resultArray);
        }
        return result;
    }

    /**
     * Add train
     * @param addTrain Command
     * @return Command
     */
    public AddTrainCommand addTrain(AddTrainCommand addTrain) {
        //check train in db
        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM train WHERE train_number=?", Train.class);
        nativeQuery.setParameter(1, addTrain.getTrainNumber());

        List<Train> resultList = nativeQuery.getResultList();

        //if it is not new record, new record don`t create
        if(resultList.size() > 0) {
            logger.warn("User can`t add train");
            addTrain.setResult(false);
        }
        else {
            // add train
            Train train = new Train();
            train.setCountPassengers(addTrain.getCountPassengers());
            train.setTrainNumber(addTrain.getTrainNumber());

            entityManager.getTransaction().begin();
            entityManager.persist(train);
            entityManager.getTransaction().commit();
            entityManager.clear();

            //get id train
            resultList = nativeQuery.getResultList();

            // add to timetable
            int i = 0;
            for(String[] station : addTrain.getStations()) {
                Timetable timetable = new Timetable();
                timetable.setTrain(resultList.get(0));
                timetable.setVacancies(addTrain.getCountPassengers());
                timetable.setDate(addTrain.getDate());
                timetable.setNumberSequence(i + 1);

                //time of arrive
                String[] time = station[1].split(":");
                timetable.setTimeArrive(new Time(Integer.parseInt(time[0]), Integer.parseInt(time[1]), 0));
                time = station[2].split(":");
                timetable.setTimeDeparture(new Time(Integer.parseInt(time[0]), Integer.parseInt(time[1]), 0));

                //get station
                nativeQuery = entityManager.createNativeQuery("SELECT * FROM station WHERE name=?", Station.class);
                nativeQuery.setParameter(1, station[0]);
                List<Station> stations = nativeQuery.getResultList();
                Station stat = stations.get(0);

                timetable.setStation(stat);

                //save in db
                entityManager.getTransaction().begin();
                entityManager.persist(timetable);
                entityManager.getTransaction().commit();
                entityManager.clear();

                i ++;
            }
            addTrain.setResult(true);
            logger.info("User add train with number " + train.getTrainNumber());
        }
        return addTrain;
    }

    /**
     * Delete train
     * @param deleteTrainCommand Command
     * @return Command
     */
    public DeleteTrainCommand deleteTrain(DeleteTrainCommand deleteTrainCommand) {
        logger.info("User deleted train number " + deleteTrainCommand.getTrainNumber());
        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM train WHERE train_number=?", Train.class);
        nativeQuery.setParameter(1, deleteTrainCommand.getTrainNumber());
        List<Train> trains = nativeQuery.getResultList();
        Train train = trains.get(0);

        entityManager.getTransaction().begin();
        entityManager.remove(train);
        entityManager.getTransaction().commit();
        entityManager.clear();

        deleteTrainCommand.setResult(true);
        return deleteTrainCommand;
    }

}
