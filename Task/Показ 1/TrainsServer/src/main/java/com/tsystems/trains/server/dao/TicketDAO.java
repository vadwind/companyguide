package com.tsystems.trains.server.dao;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 28.03.14
 * Time: 14:20
 * To change this template use File | Settings | File Templates.
 */

import com.tsystems.trains.common.client.BuyTicketCommand;
import com.tsystems.trains.common.client.GetTicketInformationCommand;
import com.tsystems.trains.common.client.ViewClientTicketCommand;
import com.tsystems.trains.server.TrainsServer;
import com.tsystems.trains.server.entity.*;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Implements DAO class for ticket
 */
public class TicketDAO {

    private static Logger logger = Logger.getLogger(TicketDAO.class);

    private EntityManager entityManager;
    private ViewClientTicketCommand viewClientTicketCommand;

    /**
     * Constructor
     * @param entityManager
     */
    public TicketDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    /**
     * Constructor
     * @param viewClientTicketCommand Command
     */
    public TicketDAO(ViewClientTicketCommand viewClientTicketCommand,
                     EntityManager entityManager) {
        this.entityManager = entityManager;
        this.viewClientTicketCommand = viewClientTicketCommand;

    }

    /**
     * Get all user tickets
     * @return Command
     */
    public ViewClientTicketCommand getUserTickets() {
        logger.info("User saw his tickets");
        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM ticket WHERE user_id=?", Ticket.class);
        nativeQuery.setParameter(1, viewClientTicketCommand.getUserId());
        List<Ticket> resultList = nativeQuery.getResultList();
        String[][] result = new String[resultList.size()][6];
        int i = 0;
        for(Ticket ticket: resultList) {
            String[] ticketString = new String[6];
            ticketString[0] = Long.toString(ticket.getId());
            ticketString[1] = ticket.getTrain().getTrainNumber();
            ticketString[2] = ticket.getStartStation().getName();
            ticketString[3] = ticket.getEndStation().getName();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            ticketString[4] = dateFormat.format(ticket.getDate());

            //time
            nativeQuery = entityManager.createNativeQuery("SELECT * FROM timetable WHERE " +
                    "station_id=? AND train_id=?", Timetable.class);
            nativeQuery.setParameter(1, ticket.getStartStation().getId());
            nativeQuery.setParameter(2, ticket.getTrain().getId());
            List<Timetable> time = nativeQuery.getResultList();
            ticketString[5] = time.get(0).getTimeDeparture().toString();

            result[i] = ticketString;
            i ++;
        }

        viewClientTicketCommand.setTickets(result);
        return viewClientTicketCommand;
    }

    public BuyTicketCommand buyTicket(BuyTicketCommand buyTicketCommand) {
        //find user by id
        User user = entityManager.find(User.class, buyTicketCommand.getUserId());

        //get stations arrive and departure
        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM station WHERE name=?", Station.class);
        nativeQuery.setParameter(1, buyTicketCommand.getStationArrive());
        List<Station> stations = nativeQuery.getResultList();
        Station stationArrive = stations.get(0);
        nativeQuery.setParameter(1, buyTicketCommand.getStationDeparture());
        stations = nativeQuery.getResultList();
        Station stationDeparture = stations.get(0);

        //get train
        nativeQuery = entityManager.createNativeQuery("SELECT * FROM train WHERE train_number=?", Train.class);
        nativeQuery.setParameter(1, buyTicketCommand.getTrainNumber());
        List<Train> trains = nativeQuery.getResultList();
        Train train = trains.get(0);

        //get all stations of train
        nativeQuery = entityManager.createNativeQuery("SELECT * FROM timetable WHERE train_id=? ORDER BY number_sequence ASC", Timetable.class);
        nativeQuery.setParameter(1, train.getId());
        List<Timetable> stationTrain = nativeQuery.getResultList();

        //check if user buy ticket on this train
        nativeQuery = entityManager.createNativeQuery("SELECT * FROM ticket WHERE train_id=? AND " +
                " user_id=?", Ticket.class);
        nativeQuery.setParameter(1, train.getId());
        nativeQuery.setParameter(2, user.getId());
        List<Ticket> us = nativeQuery.getResultList();

        //buy ticket
        if(us.isEmpty()&&isTicket(stationArrive, stationDeparture, train)) {
            //create ticket
            Ticket ticket = new Ticket();
            ticket.setStartStation(stationArrive);
            ticket.setEndStation(stationDeparture);
            ticket.setUser(user);
            ticket.setDate(stationTrain.get(0).getDate());
            ticket.setTrain(train);

            entityManager.getTransaction().begin();
            entityManager.persist(ticket);
            entityManager.getTransaction().commit();

            //change vacancies
            changeVacancies(train.getStations(), stationArrive, stationDeparture);

            buyTicketCommand.setResult(true);
            logger.info("User bought ticket");
        }
        else {
            buyTicketCommand.setResult(false);
            logger.warn("User can`t buy ticket");
        }
        return buyTicketCommand;
    }

    /**
     * Check vacancies in train
     * @param stationArrive station of Arrive
     * @param stationDeparture station of departure
     * @param train train
     * @return true if is
     */
    private boolean isTicket(Station stationArrive, Station stationDeparture,
                            Train train) {
        //check station departure
        List<Timetable> stationsTrain = train.getStations();
        int vacancies = checkStationDepartureAndVacancies(stationsTrain, stationArrive, stationDeparture);
        if(vacancies > 0) {
            if(checkTime(stationArrive, train))
                return true;
        }
        return false;
    }

    /**
     * Check station arrive
     * @param stationsTrain Stations
     * @param stationArrive Station arrive
     * @param stationDeparture Station departure
     * @return true - if ok
     */
    private int checkStationDepartureAndVacancies(List<Timetable> stationsTrain, Station stationArrive, Station stationDeparture) {
        //check station departure
        int sequenceArrive = 0, sequenceDeparture = 0;
        for(Timetable t: stationsTrain) {
            if(t.getStation().equals(stationArrive))
                sequenceArrive = t.getNumberSequence();
            if(t.getStation().equals(stationDeparture))
                sequenceDeparture = t.getNumberSequence();
        }

        //check vacancies
        if(sequenceDeparture > sequenceArrive) {
            Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM timetable WHERE train_id=? AND " +
                    "vacancies > 0 AND number_sequence > ? AND number_sequence < ?", Timetable.class);
            nativeQuery.setParameter(1, stationsTrain.get(0).getTrain().getId());
            nativeQuery.setParameter(2, sequenceArrive - 1);
            nativeQuery.setParameter(3, sequenceDeparture);
            List<Timetable> trains = nativeQuery.getResultList();
            if(trains.size() == (sequenceDeparture - sequenceArrive)) {
                int vacancies = trains.get(0).getVacancies();
                for(Timetable t: trains) {
                    if(t.getVacancies() < vacancies) {
                        vacancies = t.getVacancies();
                    }
                }
                return vacancies;
            }
        }
        return 0;
    }

    /**
     * Change vacancies
     * @param stationsTrain  All stations if train
     * @param stationArrive  station of arrive
     * @param stationDeparture station of departure
     */
    private void changeVacancies(List<Timetable> stationsTrain, Station stationArrive, Station stationDeparture) {
        //get station
        int sequenceArrive = 0, sequenceDeparture = 0;
        for(Timetable t: stationsTrain) {
            if(t.getStation().equals(stationArrive))
                sequenceArrive = t.getNumberSequence();
            if(t.getStation().equals(stationDeparture))
                sequenceDeparture = t.getNumberSequence();
        }
        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM timetable WHERE train_id=? AND " +
                "vacancies > 0 AND number_sequence > ? AND number_sequence < ?", Timetable.class);
        nativeQuery.setParameter(1, stationsTrain.get(0).getTrain().getId());
        nativeQuery.setParameter(2, sequenceArrive - 1);
        nativeQuery.setParameter(3, sequenceDeparture);
        List<Timetable> trains = nativeQuery.getResultList();
        entityManager.getTransaction().begin();
        for(Timetable stationItem: trains) {
            stationItem.setVacancies(stationItem.getVacancies() - 1);
        }
        entityManager.getTransaction().commit();
        entityManager.clear();
    }

    /**
     * Check time
     * @param stationArrive station of arrive
     * @param train train
     * @return true if ok
     */
    private boolean checkTime(Station stationArrive, Train train) {
        // get timetable
        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM timetable WHERE train_id=? AND " +
                "station_id = ?", Timetable.class);
        nativeQuery.setParameter(1, train.getId());
        nativeQuery.setParameter(2, stationArrive.getId());
        List<Timetable> stations = nativeQuery.getResultList();
        Timetable station = stations.get(0);

        //get time of departure
        Time timeDeparture = station.getTimeDeparture();
        Long timeDepartureLong = timeDeparture.getTime();

        //get now time
        Calendar nowTime = Calendar.getInstance();
        Time time = new Time(nowTime.get(Calendar.HOUR_OF_DAY), nowTime.get(Calendar.MINUTE), 0);

        // if between time of departure and now time < 10 minutes
        Long nowTimeLong = time.getTime();

        if((timeDepartureLong - nowTimeLong) > 0 &&(timeDepartureLong - nowTimeLong) < 600000)
            return false;
        return true;
    }

    /**
     * Get information about ticket
     * @param ticketInformation Command
     * @return Command
     */
    public GetTicketInformationCommand getFullInformationAboutTicket(GetTicketInformationCommand ticketInformation) {
        logger.info("User saw full information about ticket");

        //get ticket
        Ticket ticket = entityManager.find(Ticket.class, Long.parseLong(ticketInformation.getTicketNumber()));

        //set all fields
        ticketInformation.setPassengerSurname(ticket.getUser().getSurname());
        ticketInformation.setPassengerName(ticket.getUser().getName());
        ticketInformation.setPassengerBirthday(ticket.getUser().getBirthday());
        ticketInformation.setTrainNumber(ticket.getTrain().getTrainNumber());
        ticketInformation.setStationTrainDeparture(ticket.getTrain().getStations().get(0).getStation().getName());
        ticketInformation.setStationTrainArrive(ticket.getTrain().getStations().get(ticket.getTrain().getStations().size() -1 ).getStation().getName());
        ticketInformation.setDate(ticket.getDate());
        ticketInformation.setStationDeparture(ticket.getStartStation().getName());
        ticketInformation.setStationArrive(ticket.getEndStation().getName());

        //get time departure
        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM timetable WHERE " +
                "station_id=? AND train_id=?", Timetable.class);
        nativeQuery.setParameter(1, ticket.getStartStation().getId());
        nativeQuery.setParameter(2, ticket.getTrain().getId());
        List<Timetable> time = nativeQuery.getResultList();
        ticketInformation.setTimeDeparture(time.get(0).getTimeDeparture().toString());
        //get time arrive
        nativeQuery = entityManager.createNativeQuery("SELECT * FROM timetable WHERE " +
                "station_id=? AND train_id=?", Timetable.class);
        nativeQuery.setParameter(1, ticket.getEndStation().getId());
        nativeQuery.setParameter(2, ticket.getTrain().getId());
        time = nativeQuery.getResultList();
        ticketInformation.setTimeArrive(time.get(0).getTimeArrive().toString());

        return ticketInformation;

    }
}
