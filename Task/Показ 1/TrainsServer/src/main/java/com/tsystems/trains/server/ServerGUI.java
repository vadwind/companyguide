package com.tsystems.trains.server;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 26.03.14
 * Time: 3:39
 * To change this template use File | Settings | File Templates.
 */

/**
 * View class for server
 */
public class ServerGUI extends JFrame implements ActionListener {

    private boolean isStarted = false;
    private JPanel mainPanel;
    private JLabel status;
    private JButton startServer, stopServer;
    private ServerConnection serverConnection;

    /**
     * Default class constructor
     */
    public ServerGUI() {
        initWindow();

        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        addLogo();
        addLabels();
        addButtons();

        this.getContentPane().add(mainPanel);
    }

    /**
     * Initialization parameters of window
     */
    private void initWindow() {
        this.setSize(250, 300);
        this.setResizable(false);
        this.setLocation(800, 100);
        this.setTitle("Trains");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Add logo of T-Systems
     */
    private void addLogo() {
        JLabel labelLogo = new JLabel();
        ImageIcon icon = new ImageIcon("src\\main\\java\\com\\tsystems\\trains\\server\\images\\tsystems.png");
        labelLogo.setIcon(icon);
        labelLogo.setBounds(0, 0 , 250, 42);
        mainPanel.add(labelLogo);

    }

    /**
     * Add fields for login and password
     */
    private void addLabels() {
        status = new JLabel("STATUS: Server is not start");
        status.setBounds(10, 230, 250, 20);
        mainPanel.add(status);

    }

    /**
     * Add buttons on frame
     */
    private void addButtons() {
        stopServer = new JButton("Stop server") ;
        stopServer.setBounds(50, 160, 150, 40);

        startServer = new JButton("Start server") ;
        startServer.setBounds(50, 90, 150, 40);

        mainPanel.add(stopServer);
        mainPanel.add(startServer);

        stopServer.addActionListener(this);
        startServer.addActionListener(this);
    }

    /**
     * Action for buttons
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == stopServer) {
            if(isStarted) {
                System.exit(0);
                isStarted = false;
                status.setText("STATUS: Server is not start");
            }
            else {
                JOptionPane.showMessageDialog(this, "Server is not started", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

        if (e.getSource() == startServer) {
            if(!isStarted){
                serverConnection = new ServerConnection();
                status.setText("STATUS: Server started");
                isStarted = true;
            }
            else
                JOptionPane.showMessageDialog(this, "Server has already started", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

}
