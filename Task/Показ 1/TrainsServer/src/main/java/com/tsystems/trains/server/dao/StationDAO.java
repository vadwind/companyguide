package com.tsystems.trains.server.dao;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 28.03.14
 * Time: 16:05
 * To change this template use File | Settings | File Templates.
 */

import com.tsystems.trains.common.client.GetAllStationsCommand;
import com.tsystems.trains.common.manager.AddStationCommand;
import com.tsystems.trains.common.manager.DeleteStationCommand;
import com.tsystems.trains.server.TrainsServer;
import com.tsystems.trains.server.entity.Station;
import com.tsystems.trains.server.entity.Timetable;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * Implements DAO class for station
 */
public class StationDAO {

    private static Logger logger = Logger.getLogger(StationDAO.class);

    private EntityManager entityManager;

    /**
     * Constructor EntityManager
     * @param entityManager
     */
    public StationDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Get all stations
     * @return List
     */
    public GetAllStationsCommand getAllStations() {
        logger.info("Get all stations");
        List<Station> stations = entityManager.createNativeQuery("SELECT * FROM station ORDER BY name ASC", Station.class).getResultList();
        //create String array
        String[] resultArray = new String[stations.size()];
        int i = 0;
        for(Station station : stations) {
            resultArray[i] = station.getName();
            i ++;
        }
        GetAllStationsCommand result = new GetAllStationsCommand();
        result.setCommands(resultArray);
        return result;
    }

    /**
     * Add station
     * @param addStationCommand Command
     * @return Command
     */
    public AddStationCommand addStation(AddStationCommand addStationCommand) {
        //check station in db
        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM station WHERE name=?", Station.class);
        nativeQuery.setParameter(1, addStationCommand.getStationName());

        List<Station> resultList = nativeQuery.getResultList();

        //if it is not new record, new record don`t create
        if(resultList.size() > 0) {
            addStationCommand.setResult(false);
            logger.warn("User can`t add station");
        }
        else {
            Station station = new Station();
            station.setName(addStationCommand.getStationName());

            entityManager.getTransaction().begin();
            entityManager.persist(station);
            entityManager.getTransaction().commit();
            entityManager.clear();

            addStationCommand.setResult(true);
            logger.info("User added station with name " + station.getName());
        }
        return addStationCommand;
    }

    /**
     * Delete station
     * @param deleteStationCommand Command
     * @return Command
     */
    public DeleteStationCommand deleteStation(DeleteStationCommand deleteStationCommand) {

        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM station WHERE name=?", Station.class);
        nativeQuery.setParameter(1, deleteStationCommand.getStationName());
        List<Station> stations = nativeQuery.getResultList();
        Station station = stations.get(0);

        nativeQuery = entityManager.createNativeQuery("SELECT * FROM timetable WHERE station_id=? ", Timetable.class);
        nativeQuery.setParameter(1, station.getId());
        List<Timetable> trains = nativeQuery.getResultList();
        if(trains.isEmpty()) {
            entityManager.getTransaction().begin();
            entityManager.remove(station);
            entityManager.getTransaction().commit();
            entityManager.clear();

            deleteStationCommand.setResult(true);
            logger.info("User delete station with name "+deleteStationCommand.getStationName());

        }
        else {
            deleteStationCommand.setResult(false);
            logger.warn("User try delete station with name "+deleteStationCommand.getStationName());
        }
        return deleteStationCommand;
    }

}
