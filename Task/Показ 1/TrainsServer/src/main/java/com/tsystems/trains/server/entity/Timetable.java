package com.tsystems.trains.server.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 27.03.14
 * Time: 12:13
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "timetable")
public class Timetable implements Serializable {
    @Id
    private long id;

    @ManyToOne
    @JoinColumn(name = "station_id")
    private Station station;

    @ManyToOne
    @JoinColumn(name = "train_id")
    private Train train;

    private Date date;

    @Column(name = "time_arrive")
    private Time timeArrive;

    @Column(name = "time_departure")
    private Time timeDeparture;

    private int vacancies;

    @Column(name = "number_sequence")
    private int numberSequence;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTimeArrive() {
        return timeArrive;
    }

    public void setTimeArrive(Time timeArrive) {
        this.timeArrive = timeArrive;
    }

    public Time getTimeDeparture() {
        return timeDeparture;
    }

    public void setTimeDeparture(Time timeDeparture) {
        this.timeDeparture = timeDeparture;
    }

    public int getVacancies() {
        return vacancies;
    }

    public void setVacancies(int vacancies) {
        this.vacancies = vacancies;
    }

    public int getNumberSequence() {
        return numberSequence;
    }

    public void setNumberSequence(int numberSequence) {
        this.numberSequence = numberSequence;
    }
}
