package com.tsystems.trains.server.dao;

import com.tsystems.trains.common.client.SearchTicketsCommand;
import com.tsystems.trains.common.client.ViewTrainInformationCommand;
import com.tsystems.trains.common.client.ViewTrainsOnStationCommand;
import com.tsystems.trains.server.entity.Station;
import com.tsystems.trains.server.entity.Timetable;
import com.tsystems.trains.server.entity.Train;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 28.03.14
 * Time: 20:14
 * To change this template use File | Settings | File Templates.
 */
public class TimetableDAO {

    private static Logger logger = Logger.getLogger(TimetableDAO.class);

    private EntityManager entityManager;

    /**
     * Constructor
     * @param entityManager
     */
    public TimetableDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Get timetable on station
     * @param fromServer Command
     * @returnCommandn
     */
    public ViewTrainsOnStationCommand getTimeTableStation(ViewTrainsOnStationCommand fromServer) {
        logger.info("User saw timetable of station");
        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM station WHERE name=?", Station.class);
        nativeQuery.setParameter(1, fromServer.getNameStation());
        List<Station> station = nativeQuery.getResultList();

        nativeQuery = entityManager.createNativeQuery("SELECT * FROM timetable WHERE station_id=? ORDER BY date, time_arrive  ASC", Timetable.class);
        nativeQuery.setParameter(1, station.get(0).getId());
        List<Timetable> resultList = nativeQuery.getResultList();

        String[][] result = new String[resultList.size()][5];
        int i = 0;
        for(Timetable train : resultList) {
            String[] trainsString = new String[5];

            trainsString[0] = train.getTrain().getTrainNumber();
            trainsString[1] = train.getTrain().getStations().get(0).getStation().getName() +
                    " - " + train.getTrain().getStations().get(train.getTrain().getStations().size() - 1).getStation().getName();
            trainsString[2] = train.getTimeArrive().toString();
            trainsString[3] = train.getTimeDeparture().toString();

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            trainsString[4] = dateFormat.format(train.getDate());

            result[i] = trainsString;
            i ++;
        }

        fromServer.setTrains(result);
        return fromServer;
    }

    /**
     * View train information
     * @param fromUser Command
     * @return  Command
     */
    public ViewTrainInformationCommand getTrainInformation(ViewTrainInformationCommand fromUser) {
        logger.info("User saw information about train");
        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM train WHERE train_number=?", Train.class);
        nativeQuery.setParameter(1, fromUser.getTrainNumber());
        List<Train> trains = nativeQuery.getResultList();

        //string information about train
        Train train = trains.get(0);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String trainInformation = train.getTrainNumber() +
                " " + train.getStations().get(0).getStation().getName() +
                " - " + train.getStations().get(train.getStations().size() - 1).getStation().getName() +
                " " + dateFormat.format(train.getStations().get(0).getDate());
        fromUser.setTrainInformation(trainInformation);

        //stations
        nativeQuery = entityManager.createNativeQuery("SELECT * FROM timetable WHERE train_id=? ORDER BY number_sequence ASC", Timetable.class);
        nativeQuery.setParameter(1, train.getId());
        List<Timetable> resultList = nativeQuery.getResultList();
        String[][] result = new String[resultList.size()][3];
        int i = 0;
        for(Timetable station : resultList) {
            String[] trainsString = new String[5];

            trainsString[0] = station.getStation().getName();
            trainsString[1] = station.getTimeArrive().toString();
            trainsString[2] = station.getTimeDeparture().toString();

            result[i] = trainsString;
            i ++;
        }

        fromUser.setStations(result);
        return fromUser;
    }

    /**
     * Search ticket
     * @param fromUser Command
     * @return Command
     */
    public SearchTicketsCommand searchTickets(SearchTicketsCommand fromUser) {
        logger.info("User searched tickets");
        //get stations arrive and departure
        Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM station WHERE name=?", Station.class);
        nativeQuery.setParameter(1, fromUser.getStationArrive());
        List<Station> stations = nativeQuery.getResultList();
        Station stationArrive = stations.get(0);
        nativeQuery.setParameter(1, fromUser.getStationDeparture());
        stations = nativeQuery.getResultList();
        Station stationDeparture = stations.get(0);

        //search all train on station of arrive between date arrive and date departure
        if(!fromUser.getDateFrom().equals(fromUser.getDateTo())) {
            nativeQuery = entityManager.createNativeQuery("SELECT * FROM timetable WHERE station_id=? AND " +
                    "DATE(date) BETWEEN ? AND ?", Timetable.class);
            nativeQuery.setParameter(2, fromUser.getDateFrom());
            nativeQuery.setParameter(3, fromUser.getDateTo());
        }
        else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            nativeQuery = entityManager.createNativeQuery("SELECT * FROM timetable WHERE station_id=? AND " +
                    "DATE_FORMAT(date,'%Y-%m-%d')=?", Timetable.class);
            nativeQuery.setParameter(2, dateFormat.format(fromUser.getDateFrom()));
        }
        nativeQuery.setParameter(1, stationArrive.getId());
        List<Timetable> trains = nativeQuery.getResultList();

        //check station departure

        ArrayList<String[]> resultArrayList = new ArrayList<String[]>();
        for(Timetable trainTimetable : trains) {
            Train train = trainTimetable.getTrain();
            //check station departure
            List<Timetable> stationsTrain = train.getStations();
            int vacancies = checkStationDepartureAndVacancies(stationsTrain, stationArrive, stationDeparture);

            if(vacancies > 0) {
                //set string array
                String[] trainString = new String[4];
                trainString[0] = train.getTrainNumber();
                trainString[1] = train.getStations().get(0).getStation().getName() +
                        " - " + train.getStations().get(train.getStations().size() - 1).getStation().getName();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                trainString[2] = dateFormat.format(train.getStations().get(0).getDate());
                trainString[3] = Integer.toString(vacancies);
                resultArrayList.add(trainString);
            }
        }
        fromUser.setResultTickets(resultArrayList);
        return fromUser;
    }

    /**
     * Check station arrive
     * @param stationsTrain Stations
     * @param stationArrive Station arrive
     * @param stationDeparture Station departure
     * @return true - if ok
     */
    private int checkStationDepartureAndVacancies(List<Timetable> stationsTrain, Station stationArrive, Station stationDeparture) {
        //check station departure
        int sequenceArrive = 0, sequenceDeparture = 0;
        for(Timetable t: stationsTrain) {
            if(t.getStation().equals(stationArrive))
                sequenceArrive = t.getNumberSequence();
            if(t.getStation().equals(stationDeparture))
                sequenceDeparture = t.getNumberSequence();
        }

        //check vacancies
        if(sequenceDeparture > sequenceArrive) {
            Query nativeQuery = entityManager.createNativeQuery("SELECT * FROM timetable WHERE train_id=? AND " +
                    "vacancies > 0 AND number_sequence > ? AND number_sequence < ?", Timetable.class);
            nativeQuery.setParameter(1, stationsTrain.get(0).getTrain().getId());
            nativeQuery.setParameter(2, sequenceArrive - 1);
            nativeQuery.setParameter(3, sequenceDeparture);
            List<Timetable> trains = nativeQuery.getResultList();
            if(trains.size() == (sequenceDeparture - sequenceArrive)) {
                int vacancies = trains.get(0).getVacancies();
                for(Timetable t: trains) {
                   if(t.getVacancies() < vacancies) {
                       vacancies = t.getVacancies();
                   }
                }
                return vacancies;
            }
        }
        return 0;
    }
}
