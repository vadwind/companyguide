package com.tsystems.trains.server;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 26.03.14
 * Time: 4:44
 * To change this template use File | Settings | File Templates.
 */

import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Create server connection
 */
public class ServerConnection implements Runnable{

    private static Logger logger = Logger.getLogger(ServerConnection.class);

    private ServerSocket serverScoket;
    private Thread t;
    private ClientConnectionThread clientThread;
    private EntityManager em;
    private EntityManagerFactory emf;
    private Socket client;

    /**
     * dafault constructor
     */
    public ServerConnection() {
        //create connection to db
        emf = Persistence.createEntityManagerFactory("Trains");
        em = emf.createEntityManager();
        t = new Thread(this);
        t.start();
    }

    /**
     * start thread
     */
    public void run() {
        try {
            //create net connection
            serverScoket = new ServerSocket(1234);
            while (true) {
                client = serverScoket.accept();
                //create thread for each client
                synchronized (em) {
                    logger.info("New connection to server");
                    clientThread = new ClientConnectionThread(client, em);
                }
            }
        }
        catch(IOException e) {
            logger.error(e.getMessage());
            System.exit(-1);
        }
        //close sockets
        finally {
            try {
                serverScoket.close();
                em.close();
                emf.close();
            }
            catch (IOException e){
                logger.error(e.getMessage());
            }
        }
    }
}
