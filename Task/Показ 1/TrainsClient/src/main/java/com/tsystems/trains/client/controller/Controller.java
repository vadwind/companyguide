package com.tsystems.trains.client.controller;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 23.03.14
 * Time: 15:01
 * To change this template use File | Settings | File Templates.
 */

import com.tsystems.trains.client.model.Model;
import com.tsystems.trains.client.views.*;
import com.tsystems.trains.client.views.client.*;
import com.tsystems.trains.client.views.manager.*;
import com.tsystems.trains.common.client.*;
import com.tsystems.trains.common.manager.*;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.util.Date;

/**
 * Class controller application
 */
public class Controller {
    private static Logger logger = Logger.getLogger(Controller.class);

    private Model model;
    private Long userId;
    private String role;
    private ManagerAccount managerAccount;
    private AddTrain addTrain;

    /**
     * Constructor
     */
    public Controller(Model model) {
        this.model = model;
        startGUI();
    }

    /**
     * Start GUI application (Check user)
     */
    private void startGUI() {
        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                CheckOut checkOut = new CheckOut(this.getController());
            }
        });

    }

    /**
     * Check Out user
     */
    public void signUp(CheckOut view) {
        logger.info("User registered");
        //close window
        view.dispose();
        //open registration window
        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                Registration checkOut = new Registration(this.getController());
            }
        });
    }

    /**
     * If user selected "Cancel"
     * Close window with registration and open CheckOut
     */
    public void cancelRegistration(Registration view) {
        view.dispose();
        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                CheckOut checkOut = new CheckOut(this.getController());
            }
        });
    }

    /**
     * Sign in user. Check user`s login and password
     */
    public void signIn(CheckOut view) {
        logger.info("User signed in");
        String login = view.getLogin();
        String password = view.getPassword();

        //create new user for check
        CheckUserCommand checkUser = new CheckUserCommand(login, password);

        //send user to server and receive result user
        final CheckUserCommand userFromClient = model.checkUser(checkUser);

        if (userFromClient == null) {
            logger.warn("Invalid login or/and password "+ login + " " + password);
            JOptionPane.showMessageDialog(view, "Invalid login or/and password", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            logger.info("User sign in with login "+login + " and role "+userFromClient.getRole());
            //close this window and open account window
            view.dispose();
            //open manager window
            if(userFromClient.getRole().equals("client")) {
                javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
                    @Override
                    public void run() {
                        super.run();
                        userId = userFromClient.getId();
                        role = userFromClient.getRole();
                        ClientAccount checkOut = new ClientAccount(this.getController(), userFromClient.getMap());
                    }
                });

            }
            //open client window
            if(userFromClient.getRole().equals("manager")) {
                GetAllStationsCommand toServer = new GetAllStationsCommand();
                final GetAllStationsCommand fromServer = model.getAllStations(toServer);
                GetAllTrainsCommand toServerTrains = new GetAllTrainsCommand();
                final GetAllTrainsCommand fromServerTrains = model.getAllTrains(toServerTrains);
                javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
                    @Override
                    public void run() {
                        super.run();
                        ManagerAccount checkOut = new ManagerAccount(this.getController(), userFromClient.getMap(), fromServer.getCommands(), fromServerTrains.getCommands());
                        managerAccount = checkOut;
                    }
                });
            }
        }
    }

    /**
     * Registration user
     */
    public void registrationUser(Registration view) {
        RegistrationCommand registrationCommand = new RegistrationCommand(view.getLogin(), view.getPassword());
        registrationCommand.setRole("client");
        registrationCommand.setBirthday(view.getDate());
        registrationCommand.setSurname(view.getSurname());
        registrationCommand.setName(view.getUserName());
        boolean resutRegistration = model.registrationUser(registrationCommand);
        if(resutRegistration) {
            logger.info("User registered with login "+registrationCommand.getLogin());
            JOptionPane.showMessageDialog(view, "You are registered. Please log in", "Success", JOptionPane.INFORMATION_MESSAGE);
            view.dispose();
            javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
                @Override
                public void run() {
                    super.run();
                    CheckOut checkOut = new CheckOut(this.getController());
                }
            });
        }
        else {
            logger.warn("Error of registration of user");
            JOptionPane.showMessageDialog(view, "Error of registration", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Change user data
     * @param view
     */
    public void changeUserData(ClientAccount view) {
        ChangeUserDataCommand changeUserDataCommand = new ChangeUserDataCommand();
        changeUserDataCommand.setId(Long.parseLong(view.getId()));
        changeUserDataCommand.setSurname(view.getSurname());
        changeUserDataCommand.setName(view.getUserName());
        changeUserDataCommand.setLogin(view.getLogin());
        changeUserDataCommand.setRole(view.getRole());
        changeUserDataCommand.setBirthday(view.getDate());
        changeUserDataCommand.setPassword(view.getPassword());
        boolean changeData = model.changeUserData(changeUserDataCommand);
        if(changeData) {
            logger.info("User with login " + changeUserDataCommand.getLogin() + " changed data");
            JOptionPane.showMessageDialog(view, "Your data had been changed", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
        else {
            JOptionPane.showMessageDialog(view, "Error. Your data didn`t change", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * View all ticket user
     * @param view
     */
    public void viewClientTicket(ClientAccount view) {
        logger.info("User with login " + view.getLogin() + " view his tickets");
        Long id = Long.parseLong(view.getId());
        ViewClientTicketCommand toServer = new ViewClientTicketCommand(id);
        final ViewClientTicketCommand fromServer = model.viewClientTicket(toServer);
        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                ClientTickets checkOut = new ClientTickets(this.getController(), fromServer.getTickets());
            }
        });
    }

    /**
     * Get all stations for timetable
     */
    public void getAllStations() {
        GetAllStationsCommand toServer = new GetAllStationsCommand();
        final GetAllStationsCommand fromServer = model.getAllStations(toServer);
        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                SearchStation checkOut = new SearchStation(this.getController(), fromServer.getCommands());
            }
        });
    }

    /**
     * View timetable train on station
     * @param view
     */
    public void trainsOnStation(SearchStation view) {
        final String stationName = view.getSelectStation();
        view.dispose();
        ViewTrainsOnStationCommand toServer = new ViewTrainsOnStationCommand(stationName);
        final ViewTrainsOnStationCommand fromServer = model.viewTrainsOnStation(toServer);
        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                TrainsStation trainsStation = new TrainsStation(this.getController(), stationName, fromServer.getTrains());
            }
        });
    }

    /**
     * View information about train and all stations
     * @param view
     */
    public void viewTrainInformation(TrainsStation view) {
        final String trainNumber = view.getSelectedRow();
        ViewTrainInformationCommand toServer = new ViewTrainInformationCommand(trainNumber);
        final ViewTrainInformationCommand fromServer = model.viewTrainInformation(toServer);
        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                Object[] headerTable = {"Station", "Arrive", "Departure"};
                TrainInformation trainInformation = new TrainInformation(this.getController(), fromServer.getTrainInformation(), fromServer.getStations(), headerTable);
            }
        });
    }
    /**
     * View information about train and all stations
     * @param view
     */
    public void viewTrainInformation(AllTrains view) {
        final String trainNumber = view.getSelectedRow();
        ViewTrainInformationCommand toServer = new ViewTrainInformationCommand(trainNumber);
        final ViewTrainInformationCommand fromServer = model.viewTrainInformation(toServer);
        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                Object[] headerTable = {"Station", "Arrive", "Departure"};
                TrainInformation trainInformation = new TrainInformation(this.getController(), fromServer.getTrainInformation(), fromServer.getStations(), headerTable);
            }
        });
    }
    /**
     * View information about train and all stations
     * @param view
     */
    public void viewTrainInformationFromSearchTicket(TrainsResult view) {
        final String trainNumber = view.getSelectedRow();
        ViewTrainInformationCommand toServer = new ViewTrainInformationCommand(trainNumber);
        final ViewTrainInformationCommand fromServer = model.viewTrainInformation(toServer);
        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                Object[] headerTable = {"Station", "Arrive", "Departure"};
                TrainInformation trainInformation = new TrainInformation(this.getController(), fromServer.getTrainInformation(), fromServer.getStations(), headerTable);
            }
        });
    }

    /**
     * Get all stations
     */
    public void searchTrain() {
        GetAllStationsCommand toServer = new GetAllStationsCommand();
        final GetAllStationsCommand fromServer = model.getAllStations(toServer);
        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                SearchTrain stations = new SearchTrain(this.getController(), fromServer.getCommands());
            }
        });
    }

    /**
     * Search tickets
     * @param view View
     */
    public void searchTickets(SearchTrain view) {
        logger.info("User with id " + userId + " searched ticket");
        String stationArrive = view.getSelectStationArrive();
        String stationDeparture = view.getSelectStationDeparture();
        Date dateFrom = view.getDateFrom();
        Date dateTo = view.getDateTo();

        view.dispose();
        //create command
        SearchTicketsCommand toServer = new SearchTicketsCommand(stationArrive,
                stationDeparture, dateFrom, dateTo);

        final SearchTicketsCommand fromServer = model.searchTickets(toServer);
        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                String[] informationTrain = new String[3];
                informationTrain[0] = fromServer.getStationArrive();
                informationTrain[1] = fromServer.getStationDeparture();
                TrainsResult tickets = new TrainsResult(this.getController(), informationTrain, fromServer.getResultTickets());
            }
        });
    }

    /**
     * Buy ticket
     * @param view View
     */
    public void buyTicket(TrainsResult view) {
        view.dispose();
        BuyTicketCommand toServer = new BuyTicketCommand(userId,
                view.getStantionArrive(), view.getStantionDeparture(), view.getSelectedRow());
        final BuyTicketCommand fromServer = model.buyTicket(toServer);
        if(fromServer.isResult()) {
            logger.info("User with id " + userId + " buy ticket");
            JOptionPane.showMessageDialog(null, "You bought ticket. You can print it", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
        else {
            logger.warn("User with id " + userId + " didn`t can buy ticket");
            JOptionPane.showMessageDialog(null, "Error. You didn`t buy ticket", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Open window for adding new station
     */
    public void openAddStation() {
        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                AddStation view = new AddStation(this.getController());
            }
        });
    }

    /**
     * Add station
     * @param view View class
     */
    public void addStation(AddStation view) {
        AddStationCommand toServer = new AddStationCommand(view.getNameStation());
        view.dispose();
        final AddStationCommand fromServer = model.addStation(toServer);
        if(fromServer.isResult()) {
            logger.info("User with id " + userId + " add station");
            GetAllStationsCommand to = new GetAllStationsCommand();
            final GetAllStationsCommand from = model.getAllStations(to);
            managerAccount.setNewStations(from.getCommands());
            JOptionPane.showMessageDialog(null, "Station added", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
        else {
            logger.warn("User with id " + userId + " didn`t can add station");
            JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Delete station
     * @param view view class
     */
    public void deleteStation(ManagerAccount view) {
        DeleteStationCommand toServer = new DeleteStationCommand(view.getSelectStation());
        final DeleteStationCommand fromServer = model.deleteStation(toServer);
        if(fromServer.isResult())  {
            logger.info("User with id " + userId + " delete station");
            GetAllStationsCommand to = new GetAllStationsCommand();
            final GetAllStationsCommand from = model.getAllStations(to);
            managerAccount.setNewStations(from.getCommands());
            JOptionPane.showMessageDialog(null, "Station deleted", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
        else {
            logger.warn("User with id " + userId + " didn`t can delete station");
            JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * View all trains
     */
    public void viewAllTrains() {
        GetAllTrainsCommand toServerTrains = new GetAllTrainsCommand();
        toServerTrains.setWithDescription(true);
        final GetAllTrainsCommand fromServerTrains = model.getAllTrains(toServerTrains);

        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                AllTrains allTrains = new AllTrains(this.getController(), fromServerTrains.getAllTrainsWithDescription());
            }
        });
    }

    /**
     * View passengers of train
     */
    public void viewPassengers(ManagerAccount view) {
        ViewAllPassengersCommand toServer = new ViewAllPassengersCommand(view.getSelectTrainPassengers());
        final ViewAllPassengersCommand fromServer = model.viewPassengers(toServer);

        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                Object[] headerTable = {"Surname", "Name", "Birthday"};
                TrainInformation trainInformation = new TrainInformation(this.getController(), fromServer.getTrainNumber(), fromServer.getPassengers(), headerTable);
            }
        });
    }

    /**
     * Open window
     */
    public void openAddingWindow() {
        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                AddTrain addTrain = new AddTrain(this.getController());
            }
        });
    }

    /**
     * Add stations for train
     * @param view View
     */
    public void addingStationsForTrain(AddTrain view) {
        this.addTrain = view;
        GetAllStationsCommand toServer = new GetAllStationsCommand();
        final GetAllStationsCommand fromServer = model.getAllStations(toServer);

        final int countStation = view.getCountStations();

        javax.swing.SwingUtilities.invokeLater(new RunnableSwing(this) {
            @Override
            public void run() {
                super.run();
                AddStationsForTrain addStations = new AddStationsForTrain(this.getController(),
                        countStation, fromServer.getCommands());
            }
        });
    }

    /**
     * Add array of stations to view AddTrain
     * @param stations Array of stations
     */
    public void addInView(String[][] stations) {
         addTrain.setStations(stations);
    }

    /**
     * Save train
     * @param view View
     */
    public void saveTrain(AddTrain view) {
        view.dispose();
        AddTrainCommand toServer = new AddTrainCommand(view.getStations(), view.getDate(),
                view.getCountPassengers(), view.getTrainNumber());
        final AddTrainCommand fromServer = model.addTrain(toServer);
        if(fromServer.isResult()) {
            logger.info("User with id " + userId + " added train");
            GetAllTrainsCommand to = new GetAllTrainsCommand();
            final GetAllTrainsCommand from = model.getAllTrains(to);
            managerAccount.setNewTrains(from.getCommands());
            JOptionPane.showMessageDialog(null, "Train added", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
        else {
            logger.warn("User with id " + userId + " didn`t can add train");
            JOptionPane.showMessageDialog(null, "Train is not added", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Delete train
     * @param view view class
     */
    public void deleteTrain(ManagerAccount view) {
        DeleteTrainCommand toServer = new DeleteTrainCommand(view.getSelectTrain());
        final DeleteTrainCommand fromServer = model.deleteTrain(toServer);
        if(fromServer.isResult())  {
            logger.info("User with id " + userId + " delete train");
            GetAllTrainsCommand to = new GetAllTrainsCommand();
            final GetAllTrainsCommand from = model.getAllTrains(to);
            managerAccount.setNewTrains(from.getCommands());
            JOptionPane.showMessageDialog(null, "Station deleted", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
        else {
            logger.info("User with id " + userId + " didn`t can delete train");
            JOptionPane.showMessageDialog(null, "Error", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Print ticket
     * @param view View
     */
    public void printTicket(ClientTickets view, String filePath) {
        logger.info("User with id " + userId + " print ticket");
        String ticketNumber = view.getSelectedRow();
        GetTicketInformationCommand toServer = new GetTicketInformationCommand(ticketNumber);
        final GetTicketInformationCommand fromServer = model.printTicket(toServer);
        model.createPDF(fromServer, filePath);
    }

    /**
     * Get model application
     *
     * @return model
     */
    public Model getModel() {
        return model;
    }

    /**
     * Set model application
     *
     * @param model
     */
    public void setModel(Model model) {
        this.model = model;
    }
}
