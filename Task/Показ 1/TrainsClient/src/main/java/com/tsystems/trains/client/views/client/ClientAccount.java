/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 21.03.14
 * Time: 23:17
 * To change this template use File | Settings | File Templates.
 */
package com.tsystems.trains.client.views.client;

import com.tsystems.trains.client.controller.Controller;
import com.tsystems.trains.client.views.Registration;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Map;

/**
 * Class for view client`s account
 */
public class ClientAccount extends Registration implements ActionListener {

    protected String id, role;

    private JButton myTickets, buyTicket, viewTimeTable;

    /**
     * class constructor
     */
    public ClientAccount(Controller controller, Map<String, String> userInfo) {
        super(controller);
        initWindow();
        addLabelsAndFields();
        addButtons();
        setUserInfo(userInfo);
    }

    /**
     * Initialization parameters of window
     */
    private void initWindow() {
        this.setSize(570, 390);
    }

    /**
     * Add fields for login and password
     */
    private void addLabelsAndFields() {
        labelEnter.setText("Client data");

        labelEnter.setBounds(20, 45, 450, 20);

        labelLogin.setBounds(20, 80, 100, 20);
        labelPassword.setBounds(20, 120, 100, 20);
        labelRepeatPassword.setBounds(20, 160, 100, 20);
        labelSurname.setBounds(20, 200, 100, 20);
        labelName.setBounds(20, 240, 100, 20);
        labelBirthday.setBounds(20, 280, 100, 20);

        login.setBounds(125, 75, 220, 30);
        password.setBounds(125, 115, 220, 30);
        repeatPassword.setBounds(125, 155, 220, 30);
        surname.setBounds(125, 195, 220, 30);
        name.setBounds(125, 235, 220, 30);
        birthday.setBounds(125, 275, 220, 30);
    }

    /**
     * Add buttons on frame
     */
    protected void addButtons() {
        cancel.setText("Close");
        cancel.setBounds(460, 310, 80, 30);
        save.setBounds(265, 310, 80, 30);

        myTickets = new JButton("My tickets");
        myTickets.setBounds(390, 75, 150, 40);

        buyTicket = new JButton("Buy ticket");
        buyTicket.setBounds(390, 155, 150, 40);

        viewTimeTable = new JButton("View timetable");
        viewTimeTable.setBounds(390, 235, 150, 40);

        mainPanel.add(myTickets);
        mainPanel.add(buyTicket);
        mainPanel.add(viewTimeTable);

        myTickets.addActionListener(this);
        buyTicket.addActionListener(this);
        viewTimeTable.addActionListener(this);
    }

    /**
     * Set user info in text fields
     */
    private void setUserInfo(Map<String, String> userInfo) {
        if (userInfo.containsKey("id"))
            id = userInfo.get("id");
        if (userInfo.containsKey("login"))
            login.setText(userInfo.get("login"));
        if (userInfo.containsKey("surname"))
            surname.setText(userInfo.get("surname"));
        if (userInfo.containsKey("name"))
            name.setText(userInfo.get("name"));
        if (userInfo.containsKey("birthday")) {
            //get string with birthday
            String stringBirthday = userInfo.get("birthday");
            //string to array
            String[] dates = stringBirthday.split("-");
            //set birthday in JDatePicker
            birthday.getModel().setDate(Integer.parseInt(dates[0]), Integer.parseInt(dates[1]), Integer.parseInt(dates[2]));
            birthday.getModel().setSelected(true);
        }

    }

    /**
     * Action for buttons
     *
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cancel) {
            this.dispose();
        }

        if (e.getSource() == save) {

            String stringPassword = new String(password.getPassword());
            String stringRepeatPassword = new String(repeatPassword.getPassword());

            if (!stringPassword.equals(stringRepeatPassword)||stringPassword.equals("")) {
                JOptionPane.showMessageDialog(this, "The password fields do not match.", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                Date date = getDate();
                if (date == null)
                    JOptionPane.showMessageDialog(this, "You must enter date.", "Error", JOptionPane.ERROR_MESSAGE);
                else {
                    controller.changeUserData(this);
                }

            }
        }

        if (e.getSource() == myTickets) {
            controller.viewClientTicket(this);
        }

        if (e.getSource() == buyTicket) {
            controller.searchTrain();
        }
        if (e.getSource() == viewTimeTable) {
            controller.getAllStations();
        }

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
