package com.tsystems.trains.client.views.client;

import com.tsystems.trains.client.controller.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 22.03.14
 * Time: 13:45
 * To change this template use File | Settings | File Templates.
 */
public class TrainInformation extends JFrame implements ActionListener {

    protected Controller controller;

    protected JPanel mainPanel;
    protected JLabel informationTrain;
    protected JScrollPane scroll;
    protected JTable table;
    protected JButton close;

    /**
     * default class constructor
     */
    public TrainInformation() {

    }

    /**
     * class constructor
     */
    public TrainInformation(Controller controller, String trainNumberAndName, Object[][] stationsTable, Object[] headers) {
        this.controller = controller;

        initWindow();

        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        addLogo();
        addLabelAndTable(trainNumberAndName, stationsTable, headers);
        addButtons();

        this.getContentPane().add(mainPanel);
    }

    /**
     * Initialization parameters of window
     */
    protected void initWindow() {
        this.setSize(650, 450);
        this.setResizable(false);
        this.setLocation(100, 100);
        this.setTitle("Trains");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Add logo of T-Systems
     */
    protected void addLogo() {
        JLabel labelLogo = new JLabel();
        ImageIcon icon = new ImageIcon("src\\main\\java\\com\\tsystems\\trains\\client\\views\\images\\tsystems.png");
        labelLogo.setIcon(icon);
        labelLogo.setBounds(0, 0, 250, 42);
        mainPanel.add(labelLogo);

    }

    /**
     * Add labels and tabel
     */
    protected void addLabelAndTable(String trainNumberAndName, Object[][] stationsTable, Object[] headers) {
        informationTrain = new JLabel("Train " + trainNumberAndName);
        informationTrain.setBounds(5, 45, 460, 20);

        //add table with timetable
        table = new JTable(stationsTable, headers);
        table.getColumnModel().getColumn(0).setResizable(false);
        table.getColumnModel().getColumn(1).setResizable(false);
        table.getColumnModel().getColumn(2).setResizable(false);

        //add scroll
        scroll = new JScrollPane(table);
        table.setPreferredScrollableViewportSize(new Dimension(630, 290));
        scroll.setBounds(10, 70, 620, 290);

        mainPanel.add(informationTrain);
        mainPanel.add(scroll);

    }

    /**
     * Add buttons on frame
     */
    protected void addButtons() {
        close = new JButton("Close");
        close.setBounds(550, 380, 80, 30);

        mainPanel.add(close);

        close.addActionListener(this);

    }

    /**
     * Action for buttons
     *
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == close) {
            this.dispose();
        }
    }
}
