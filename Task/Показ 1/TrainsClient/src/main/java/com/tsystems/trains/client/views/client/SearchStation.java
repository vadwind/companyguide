/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 22.03.14
 * Time: 1:45
 * To change this template use File | Settings | File Templates.
 */
package com.tsystems.trains.client.views.client;

import com.tsystems.trains.client.controller.Controller;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * view class for search station
 */
public class SearchStation extends JFrame implements ActionListener {

    protected Controller controller;

    protected JPanel mainPanel;
    protected JLabel labelEnter;
    protected JButton cancel, view;
    protected JComboBox stations;

    /**
     * class constructor
     */
    public SearchStation(Controller controller, String[] stringsStations) {
        this.controller = controller;

        initWindow();

        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        addLogo();
        addLabelsAndComboBox(stringsStations);
        addButtons();

        this.getContentPane().add(mainPanel);
    }

    /**
     * Initialization parameters of window
     */
    private void initWindow() {
        this.setSize(280, 190);
        this.setResizable(false);
        this.setLocation(100, 100);
        this.setTitle("Trains");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Add logo of T-Systems
     */
    private void addLogo() {
        JLabel labelLogo = new JLabel();
        ImageIcon icon = new ImageIcon("src\\main\\java\\com\\tsystems\\trains\\client\\views\\images\\tsystems.png");
        labelLogo.setIcon(icon);
        labelLogo.setBounds(0, 0, 250, 42);
        mainPanel.add(labelLogo);

    }

    /**
     * Add buttons on frame
     */
    private void addButtons() {
        cancel = new JButton("Cancel");
        cancel.setBounds(10, 120, 80, 30);

        view = new JButton("View");
        view.setBounds(180, 120, 80, 30);

        mainPanel.add(cancel);
        mainPanel.add(view);

        cancel.addActionListener(this);
        view.addActionListener(this);
    }

    /**
     * Add fields and labels
     *
     * @param stringsStations stations for select
     */
    private void addLabelsAndComboBox(String[] stringsStations) {
        labelEnter = new JLabel("Select station");
        labelEnter.setBounds(10, 45, 150, 20);

        stations = new JComboBox(stringsStations);
        stations.setBounds(10, 70, 250, 30);

        mainPanel.add(labelEnter);
        mainPanel.add(stations);
    }

    /**
     * Action for buttons
     *
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cancel) {
            this.dispose();
        }
        if (e.getSource() == view) {
            controller.trainsOnStation(this);
        }

    }

    /**
     * Get select stations
     */
    public String getSelectStation() {
        return (String) stations.getSelectedItem();
    }
}
