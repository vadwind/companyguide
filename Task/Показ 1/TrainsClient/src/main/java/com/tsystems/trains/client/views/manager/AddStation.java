package com.tsystems.trains.client.views.manager;

import com.tsystems.trains.client.controller.Controller;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 29.03.14
 * Time: 16:16
 * To change this template use File | Settings | File Templates.
 */

/**
 * View class for adding station
 */
public class AddStation extends JFrame implements ActionListener {

    protected Controller controller;

    private JTextField nameStation;

    protected JPanel mainPanel;
    protected JLabel labelName;

    private JButton cancel, save;

    /**
     * Constructor
     * @param controller Controller
     */
    public AddStation(Controller controller){
        this.controller = controller;

        initWindow();

        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        addLogo();
        addTextFieldAndLabel();
        addButtons();

        this.getContentPane().add(mainPanel);
    }

    /**
     * Initialization parameters of window
     */
    private void initWindow() {
        this.setSize(280, 190);
        this.setResizable(false);
        this.setLocation(100, 100);
        this.setTitle("Trains");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Add logo of T-Systems
     */
    private void addLogo() {
        JLabel labelLogo = new JLabel();
        ImageIcon icon = new ImageIcon("src\\main\\java\\com\\tsystems\\trains\\client\\views\\images\\tsystems.png");
        labelLogo.setIcon(icon);
        labelLogo.setBounds(0, 0, 250, 42);
        mainPanel.add(labelLogo);
    }

    public void addTextFieldAndLabel() {
        nameStation = new JTextField();
        nameStation.setBounds(80, 70, 170, 30);

        labelName = new JLabel("Name");
        labelName.setBounds(20, 75, 50, 20);

        mainPanel.add(labelName);
        mainPanel.add(nameStation);

    }

    /**
     * Add buttons on frame
     */
    private void addButtons() {
        cancel = new JButton("Cancel");
        cancel.setBounds(10, 120, 80, 30);

        save = new JButton("Save");
        save.setBounds(180, 120, 80, 30);

        mainPanel.add(cancel);
        mainPanel.add(save);

        cancel.addActionListener(this);
        save.addActionListener(this);
    }


    /**
     * Action for buttons
     *
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cancel) {
            this.dispose();
        }
        if (e.getSource() == save) {
            if(nameStation.getText().isEmpty())
                JOptionPane.showMessageDialog(this, "Name is empty", "Error", JOptionPane.ERROR_MESSAGE);
            else
                controller.addStation(this);
        }

    }

    public String getNameStation() {
        return nameStation.getText();
    }

    public void setNameStation(JTextField nameStation) {
        this.nameStation = nameStation;
    }
}
