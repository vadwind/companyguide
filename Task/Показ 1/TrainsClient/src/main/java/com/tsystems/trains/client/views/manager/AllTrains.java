package com.tsystems.trains.client.views.manager;

import com.tsystems.trains.client.controller.Controller;
import com.tsystems.trains.client.views.client.TrainInformation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 29.03.14
 * Time: 19:57
 * To change this template use File | Settings | File Templates.
 */
public class AllTrains extends TrainInformation implements ActionListener {

    private JButton viewTrainInformation;
    private JLabel labelSelect;

    /**
     * class constructor
     */
    public AllTrains(Controller controller, Object[][] trainsTable) {

        this.controller = controller;

        initWindow();
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        addLogo();
        addLabelAndTable(trainsTable);
        addButtons();

        this.getContentPane().add(mainPanel);
    }

    /**
     * Add labels and tabel
     */
    protected void addLabelAndTable(Object[][] trainsTable) {
        informationTrain = new JLabel("All trains");
        informationTrain.setBounds(5, 45, 460, 20);

        //add table with timetable
        Object[] headerTable = {"Number", "Start station", "End station", "Date"};
        table = new JTable(trainsTable, headerTable);
        table.getColumnModel().getColumn(0).setResizable(false);
        table.getColumnModel().getColumn(1).setResizable(false);
        table.getColumnModel().getColumn(2).setResizable(false);
        table.getColumnModel().getColumn(3).setResizable(false);

        //add scroll
        scroll = new JScrollPane(table);
        table.setPreferredScrollableViewportSize(new Dimension(620, 290));
        scroll.setBounds(10, 70, 620, 290);

        mainPanel.add(informationTrain);
        mainPanel.add(scroll);
    }

    @Override
    /**
     * Add buttons on frame
     */
    protected void addButtons() {
        close = new JButton("Close");
        close.setBounds(10, 380, 80, 30);

        viewTrainInformation = new JButton("View information about train");
        viewTrainInformation.setBounds(430, 380, 200, 30);

        labelSelect = new JLabel("Select train in table");
        labelSelect.setBounds(310, 385, 150, 20);

        mainPanel.add(viewTrainInformation);
        mainPanel.add(close);
        mainPanel.add(labelSelect);

        close.addActionListener(this);
        viewTrainInformation.addActionListener(this);

    }
    /**
     * Action for buttons
     *
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == close) {
            this.dispose();
        }

        if (e.getSource() == viewTrainInformation) {
            if(table.getSelectedRow() > -1) {
                controller.viewTrainInformation(this);
            }
            else {
                JOptionPane.showMessageDialog(this, "You must select train.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Get number train
     *
     * @return number
     */
    public String getSelectedRow() {
        int selectedRows = table.getSelectedRow();
        return (String) table.getModel().getValueAt(selectedRows, 0);
    }
}
