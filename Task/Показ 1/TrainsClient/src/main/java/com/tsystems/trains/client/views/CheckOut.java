package com.tsystems.trains.client.views;

import com.tsystems.trains.client.controller.Controller;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 20.03.14
 * Time: 2:46
 * To change this template use File | Settings | File Templates.
 */

/**
 * View class CheckOut check user`s login and password
 */
public class CheckOut extends JFrame implements ActionListener {

    private Controller controller;
    private JPanel mainPanel;
    private JTextField login;
    private JPasswordField password;

    private JButton signIn, signUp;

    /**
     * Default class constructor
     */
    public CheckOut(Controller controller) {
        this.controller = controller;

        initWindow();

        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        addLogo();
        addLabelsAndFields();
        addButtons();

        this.getContentPane().add(mainPanel);
    }

    /**
     * Initialization parameters of window
     */
    private void initWindow() {
        this.setSize(250, 300);
        this.setResizable(false);
        this.setLocation(100, 100);
        this.setTitle("Trains");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Add logo of T-Systems
     */
    private void addLogo() {
        JLabel labelLogo = new JLabel();
        ImageIcon icon = new ImageIcon("src\\main\\java\\com\\tsystems\\trains\\client\\views\\images\\tsystems.png");
        labelLogo.setIcon(icon);
        labelLogo.setBounds(0, 0, 250, 42);
        mainPanel.add(labelLogo);

    }

    /**
     * Add fields for login and password
     */
    private void addLabelsAndFields() {
        JLabel labelEnter = new JLabel("Please, enter your login and password");
        labelEnter.setBounds(10, 45, 250, 20);

        JLabel labelLogin = new JLabel("login");
        labelLogin.setBounds(10, 75, 100, 20);

        JLabel labelPassword = new JLabel("Password");
        labelPassword.setBounds(10, 140, 100, 20);

        login = new JTextField();
        login.setBounds(10, 95, 220, 30);

        password = new JPasswordField();
        password.setBounds(10, 160, 220, 30);

        mainPanel.add(labelEnter);
        mainPanel.add(labelLogin);
        mainPanel.add(labelPassword);

        mainPanel.add(login);
        mainPanel.add(password);

    }

    /**
     * Add buttons on frame
     */
    private void addButtons() {
        signIn = new JButton("Sign in");
        signIn.setBounds(150, 220, 80, 30);

        signUp = new JButton("Sign up");
        signUp.setBounds(10, 220, 80, 30);

        mainPanel.add(signIn);
        mainPanel.add(signUp);

        signIn.addActionListener(this);
        signUp.addActionListener(this);
    }

    /**
     * Action for buttons
     *
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == signIn) {
            controller.signIn(this);
        }

        if (e.getSource() == signUp) {
            controller.signUp(this);
        }
    }

    /**
     * get login`s user
     *
     * @return entering login
     */
    public String getLogin() {
        return login.getText();
    }

    /**
     * get login`s password
     *
     * @return entering password
     */
    public String getPassword() {
        return new String(password.getPassword());
    }

}
