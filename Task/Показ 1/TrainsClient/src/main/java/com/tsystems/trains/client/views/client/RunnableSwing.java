package com.tsystems.trains.client.views.client;

import com.tsystems.trains.client.controller.Controller;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 26.03.14
 * Time: 19:16
 * To change this template use File | Settings | File Templates.
 */

/**
 * Implements Runnable with controller
 */
public class RunnableSwing implements Runnable {
    private Thread t;
    private Controller controller;

    /**
     * controller
     *
     * @param controller
     */
    public RunnableSwing(Controller controller) {
        this.controller = controller;
        t = new Thread();
        t.start();
    }

    /**
     * default run
     */
    public void run() {
    }

    /**
     * Get Controller
     *
     * @return controller
     */
    public Controller getController() {
        return this.controller;
    }
}
