package com.tsystems.trains.client.views.manager;

import com.tsystems.trains.client.controller.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 29.03.14
 * Time: 23:13
 * To change this template use File | Settings | File Templates.
 */

/**
 * View class for adding stations
 */
public class AddStationsForTrain extends JFrame implements ActionListener {

    private Controller controller;
    private AddTrain view;

    private int countStations;
    protected JPanel mainPanel, panel;
    private JButton close, save;

    private JComboBox[] comboBoxes;
    private JTextField[] timesArrive;
    private JTextField[] timesDepartures;

    private JScrollPane scroll;

    /**
     * Constructor
     * @param controller Controller
     * @param countStations count stations
     * @param stations Stations
     */
    public AddStationsForTrain(Controller controller, int countStations, String[] stations) {
        this.controller = controller;
        this.countStations = countStations;

        comboBoxes = new JComboBox[countStations];
        timesArrive = new JTextField[countStations];
        timesDepartures = new JTextField[countStations];

        initWindow();

        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        addLogo();
        addLabelAndTextFields(stations);
        addButtons();

        this.getContentPane().add(mainPanel);

    }

    /**
     * Initialization parameters of window
     */
    protected void initWindow() {
        this.setSize(350, 580);
        this.setResizable(false);
        this.setLocation(100, 100);
        this.setTitle("Trains");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Add logo of T-Systems
     */
    protected void addLogo() {
        JLabel labelLogo = new JLabel();
        ImageIcon icon = new ImageIcon("src\\main\\java\\com\\tsystems\\trains\\client\\views\\images\\tsystems.png");
        labelLogo.setIcon(icon);
        labelLogo.setBounds(0, 0, 250, 42);
        mainPanel.add(labelLogo);
    }

    /**
     * Add label and textfields
     * @param stations Stations
     */
    private void addLabelAndTextFields(String[] stations) {
        JLabel label = new JLabel("Enter stations and time of arrive and departure");
        label.setBounds(10, 50, 300, 20);

        JLabel table = new JLabel("        Stantion                  Time Arrive          Time Departure");
        table.setBounds(10, 70, 340, 20);

        //add scroll
        JPanel stationPanel = new JPanel();
        scroll = new JScrollPane(stationPanel);
        scroll.setBounds(10, 90, 330, 400);
        stationPanel.setLayout(new BoxLayout(stationPanel, BoxLayout.Y_AXIS));

        for(int i = 0; i < countStations; i ++) {
            JPanel itemPanel = new JPanel();
            comboBoxes[i] = new JComboBox(stations);
            timesArrive[i] = new JTextField();
            timesDepartures[i] = new JTextField();
            itemPanel.add(comboBoxes[i]);
            itemPanel.add(timesArrive[i]);
            itemPanel.add(timesDepartures[i]);
            itemPanel.setLayout(new BoxLayout(itemPanel, BoxLayout.X_AXIS));
            itemPanel.setMaximumSize((new Dimension(500, 30)));
            stationPanel.add(itemPanel);
        }
        mainPanel.add(label);
        mainPanel.add(table);
        mainPanel.add(scroll);
    }

    private void addButtons() {
        close = new JButton("Close");
        close.setBounds(10, 510, 100, 30);

        save = new JButton("Save");
        save.setBounds(240, 510, 100, 30);

        mainPanel.add(close);
        mainPanel.add(save);

        close.addActionListener(this);
        save.addActionListener(this);
    }

    /**
     * Action for buttons
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
         if(e.getSource() == close) {
             this.dispose();
         }

         if(e.getSource() == save) {
             //check input data
             boolean isError = false;
             String[][] result = new String[countStations][3];
             HashSet<String> check = new HashSet<String>();
             for(int i = 0; i < countStations; i ++) {
                 check.add(comboBoxes[i].getSelectedItem().toString());
                 if(testTime(timesArrive[i].getText()) && testTime(timesDepartures[i].getText())) {
                     result[i][0] = comboBoxes[i].getSelectedItem().toString();
                     result[i][1] = timesArrive[i].getText();
                     result[i][2] = timesDepartures[i].getText();
                 }
                 else
                     isError = true;
             }
             if(!isError && (check.size() == countStations)) {
                 controller.addInView(result);
                 this.dispose();
             }
             else
                 JOptionPane.showMessageDialog(this, "Check input data", "Error", JOptionPane.ERROR_MESSAGE);
         }
    }

    /**
     * chak time
     * @param time
     * @return
     */
    public static boolean testTime(String time){
        Pattern p = Pattern.compile("^(([0,1][0-9])|(2[0-3])):[0-5][0-9]$");
        Matcher m = p.matcher(time);
        return m.matches();
    }
}
