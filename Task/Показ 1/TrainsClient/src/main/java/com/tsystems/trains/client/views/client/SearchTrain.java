/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 22.03.14
 * Time: 11:45
 * To change this template use File | Settings | File Templates.
 */
package com.tsystems.trains.client.views.client;

import com.tsystems.trains.client.controller.Controller;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * view class for search train
 */
public class SearchTrain extends JFrame implements ActionListener {

    protected Controller controller;

    protected JPanel mainPanel;
    protected JDatePickerImpl dateFrom, dateTo;
    protected JLabel labelEnter, labelStationArrive, labelStationDeparture, labelDateFrom, labelDateTo;
    protected JComboBox stationArrive, stationDeparture;
    protected JButton cancel, search;

    /**
     * class constructor
     */
    public SearchTrain(Controller controller, String[] stations) {
        this.controller = controller;

        initWindow();

        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        addLogo();
        addLabelsAndFields(stations);
        addButtons();

        this.getContentPane().add(mainPanel);
    }

    /**
     * Initialization parameters of window
     */
    private void initWindow() {
        this.setSize(350, 410);
        this.setResizable(false);
        this.setLocation(100, 100);
        this.setTitle("Trains");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Add logo of T-Systems
     */
    private void addLogo() {
        JLabel labelLogo = new JLabel();
        ImageIcon icon = new ImageIcon("src\\main\\java\\com\\tsystems\\trains\\client\\views\\images\\tsystems.png");
        labelLogo.setIcon(icon);
        labelLogo.setBounds(0, 0, 250, 42);
        mainPanel.add(labelLogo);

    }

    /**
     * Add fields
     */
    private void addLabelsAndFields(String[] stations) {
        labelEnter = new JLabel("Search train. Select stations arrive and departure and date");
        labelEnter.setBounds(5, 45, 460, 20);

        labelStationArrive = new JLabel("Station departure");
        labelStationArrive.setBounds(10, 75, 100, 20);

        labelStationDeparture = new JLabel("Station arrive");
        labelStationDeparture.setBounds(10, 140, 100, 20);

        labelDateFrom = new JLabel("Date from");
        labelDateFrom.setBounds(10, 200, 100, 20);

        labelDateTo = new JLabel("Date to");
        labelDateTo.setBounds(10, 270, 100, 20);


        stationArrive = new JComboBox(stations);
        stationArrive.setBounds(10, 100, 250, 30);

        stationDeparture = new JComboBox(stations);
        stationDeparture.setBounds(10, 160, 250, 30);

        UtilDateModel dateModelFrom = new UtilDateModel();
        dateModelFrom.setValue(new Date());
        dateModelFrom.setSelected(true);
        JDatePanelImpl datePanelFrom = new JDatePanelImpl(dateModelFrom);
        dateFrom = new JDatePickerImpl(datePanelFrom);
        dateFrom.setBounds(10, 230, 220, 30);

        UtilDateModel dateModelTo = new UtilDateModel();
        dateModelTo.setValue(new Date());
        dateModelTo.setSelected(true);
        JDatePanelImpl datePanelTo = new JDatePanelImpl(dateModelTo);
        dateTo = new JDatePickerImpl(datePanelTo);
        dateTo.setBounds(10, 300, 220, 30);

        mainPanel.add(labelEnter);
        mainPanel.add(labelStationArrive);
        mainPanel.add(labelStationDeparture);
        mainPanel.add(labelDateFrom);
        mainPanel.add(labelDateTo);

        mainPanel.add(stationArrive);
        mainPanel.add(stationDeparture);
        mainPanel.add(dateFrom);
        mainPanel.add(dateTo);

    }

    /**
     * Add buttons on frame
     */
    private void addButtons() {
        cancel = new JButton("Cancel");
        cancel.setBounds(10, 340, 80, 30);

        search = new JButton("Search");
        search.setBounds(250, 340, 80, 30);

        mainPanel.add(cancel);
        mainPanel.add(search);

        cancel.addActionListener(this);
        search.addActionListener(this);
    }

    /**
     * Action for buttons
     *
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cancel) {
            this.dispose();
        }

        if (e.getSource() == search) {
            //check stations arrive and departure.
            if (getSelectStationArrive().equals(getSelectStationDeparture()))
                JOptionPane.showMessageDialog(this, "Stations of arrive and departure are the same", "Error", JOptionPane.ERROR_MESSAGE);
            else {
                //check dates. DateTo must been after than DateFrom
                if (!getDateTo().after(getDateFrom()) && !getDateFrom().equals(getDateTo()))
                    JOptionPane.showMessageDialog(this, "DateFrom must be before DateTo", "Error", JOptionPane.ERROR_MESSAGE);
                else {
                    long date = new Date().getTime() - 43200000;
                    if(getDateFrom().before(new Date(date))) {
                        JOptionPane.showMessageDialog(this, "DateFrom and DateTo must be after now", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    else
                        controller.searchTickets(this);
                }
            }
        }
    }

    /**
     * Get select station arrive
     */
    public String getSelectStationArrive() {
        return (String) stationArrive.getSelectedItem();
    }

    /**
     * Get select station departure
     */
    public String getSelectStationDeparture() {
        return (String) stationDeparture.getSelectedItem();
    }

    /**
     * Get  user`s date_from from GUI
     *
     * @return user`s date from frame
     */
    public Date getDateFrom() {
        Date selectedDate = (Date) dateFrom.getModel().getValue();
        return selectedDate;
    }

    /**
     * Get  user`s date_to from GUI
     *
     * @return user`s date_to from frame
     */
    public Date getDateTo() {
        Date selectedDate = (Date) dateTo.getModel().getValue();
        return selectedDate;
    }

}
