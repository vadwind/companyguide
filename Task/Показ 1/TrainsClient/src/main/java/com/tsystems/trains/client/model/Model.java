package com.tsystems.trains.client.model;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.tsystems.trains.common.client.*;
import com.tsystems.trains.common.manager.*;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.text.SimpleDateFormat;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 23.03.14
 * Time: 15:03
 * To change this template use File | Settings | File Templates.
 */
public class Model {

    private static Logger logger = Logger.getLogger(Model.class);

    private final int PORT = 1234;
    private final String HOST = "localhost";

    /**
     * Create command and send it to server
     * @param checkUser Object user
     * @return user from server
     */
    public CheckUserCommand checkUser(CheckUserCommand checkUser) {
        CheckUserCommand resultUser = (CheckUserCommand) createConnection(checkUser);
        return resultUser;
    }

    /**
     * Registration
     * @param registrationCommand Command
     * @return success
     */
    public boolean registrationUser(RegistrationCommand registrationCommand) {
        boolean result = (Boolean) createConnection(registrationCommand);
        return result;
    }

    /**
     * Change user data
     * @param changeUserDataCommand Command
     * @return Command
     */
    public boolean changeUserData(ChangeUserDataCommand changeUserDataCommand) {
        boolean result = (Boolean) createConnection(changeUserDataCommand);
        return result;
    }

    /**
     * View client tickets
     * @param viewClientTicketCommand command
     * @return command
     */
    public ViewClientTicketCommand viewClientTicket(ViewClientTicketCommand viewClientTicketCommand) {
        ViewClientTicketCommand ticketsFromServer = (ViewClientTicketCommand) createConnection(viewClientTicketCommand);
        return ticketsFromServer;
    }

    /**
     * Get all stations
     * @param getAllStationsCommand Command to server
     * @return Command from server
     */
    public GetAllStationsCommand getAllStations(GetAllStationsCommand getAllStationsCommand) {
        GetAllStationsCommand stationsFromServer = (GetAllStationsCommand) createConnection(getAllStationsCommand);
        return stationsFromServer;
    }

    /**
     * Get all trains
     * @param getAllTrainsCommand Command to server
     * @return Command from server
     */
    public GetAllTrainsCommand getAllTrains(GetAllTrainsCommand getAllTrainsCommand) {
        GetAllTrainsCommand trainsFromServer = (GetAllTrainsCommand) createConnection(getAllTrainsCommand);
        return trainsFromServer;
    }

    /**
     * Get all train on station
     * @param viewTrainsOnStationCommand Command
     * @return Command from server
     */
    public ViewTrainsOnStationCommand viewTrainsOnStation(ViewTrainsOnStationCommand viewTrainsOnStationCommand) {
        ViewTrainsOnStationCommand trainsFromServer = (ViewTrainsOnStationCommand)createConnection(viewTrainsOnStationCommand);
        return trainsFromServer;
    }

    /**
     * View train information
     * @param viewTrainInformationCommand Command
     * @return Command from server
     */
    public ViewTrainInformationCommand viewTrainInformation(ViewTrainInformationCommand viewTrainInformationCommand) {
        ViewTrainInformationCommand trainInformation = (ViewTrainInformationCommand)createConnection(viewTrainInformationCommand);
        return trainInformation;
    }

    /**
     * Search ticket
     * @param searchTicketsCommand Command
     * @return Command from server
     */
    public SearchTicketsCommand searchTickets(SearchTicketsCommand searchTicketsCommand) {
        SearchTicketsCommand searchTickets = (SearchTicketsCommand)createConnection(searchTicketsCommand);
        return searchTickets;
    }

    /**
     * Buy ticket
     * @param buyTicketCommand Command
     * @return Command
     */
    public BuyTicketCommand buyTicket(BuyTicketCommand buyTicketCommand) {
        BuyTicketCommand buyTicket = (BuyTicketCommand)createConnection(buyTicketCommand);
        return buyTicket;
    }

    /**
     * Add station
     * @param addStationCommand Command
     * @return Command
     */
    public AddStationCommand addStation(AddStationCommand addStationCommand) {
        AddStationCommand addStation = (AddStationCommand) createConnection(addStationCommand);
        return addStation;
    }

    /**
     * Delete station
     * @param deleteStationCommand Command
     * @return Command
     */
    public DeleteStationCommand deleteStation(DeleteStationCommand deleteStationCommand) {
        DeleteStationCommand deleteStation = (DeleteStationCommand) createConnection(deleteStationCommand);
        return deleteStation;
    }

    /**
     * View all passengers
     * @param viewAllPassengersCommand
     * @return
     */
    public ViewAllPassengersCommand viewPassengers(ViewAllPassengersCommand viewAllPassengersCommand) {
        ViewAllPassengersCommand allPassengers = (ViewAllPassengersCommand) createConnection(viewAllPassengersCommand);
        return allPassengers;
    }

    /**
     * Add train
     * @param addTrainCommand  Command
     * @return Command
     */
    public AddTrainCommand addTrain(AddTrainCommand addTrainCommand) {
        AddTrainCommand addTrain = (AddTrainCommand) createConnection(addTrainCommand);
        return addTrain;
    }

    /**
     * Delete train
     * @param deleteTrainCommand Command
     * @return Command
     */
    public DeleteTrainCommand deleteTrain(DeleteTrainCommand deleteTrainCommand) {
        DeleteTrainCommand deleteTrain = (DeleteTrainCommand) createConnection(deleteTrainCommand);
        return deleteTrain;
    }

    /**
     * Get information for printing ticket
     * @param printTicketCommand
     * @return
     */
    public GetTicketInformationCommand printTicket(GetTicketInformationCommand printTicketCommand) {
        GetTicketInformationCommand printTicket = (GetTicketInformationCommand) createConnection(printTicketCommand);
        return printTicket;
    }

    /**
     * Create pdf
     * @param command Command
     */
    public void createPDF(GetTicketInformationCommand command, String filePath) {
        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filePath));
        }
        catch (FileNotFoundException e) {
            logger.error(e.getMessage());
        }
        catch (DocumentException e) {
            logger.error(e.getMessage());
        }
        document.open();

        try {
            //image
            Image icon = Image.getInstance("src\\main\\java\\com\\tsystems\\trains\\client\\views\\images\\tsystems.png");
            document.add(icon);

            //ticket number
            document.add(new Paragraph("Ticket number: " + command.getTicketNumber(),
                    FontFactory.getFont(FontFactory.TIMES_ROMAN, 25, Font.BOLD,	new CMYKColor(0, 0, 0, 255))));

            //user data
            document.add(new Paragraph("User data",
                    FontFactory.getFont(FontFactory.COURIER, 25, Font.BOLD,	new CMYKColor(0, 0, 0, 255))));

            //table with user data
            PdfPTable userTable = new PdfPTable(3);
            userTable.setSpacingBefore(25);
            userTable.setSpacingAfter(25);
            PdfPCell c1 = new PdfPCell(new Phrase("Surname"));
            userTable.addCell(c1);
            PdfPCell c2 = new PdfPCell(new Phrase("Name"));
            userTable.addCell(c2);
            PdfPCell c3 = new PdfPCell(new Phrase("Birthday"));
            userTable.addCell(c3);
            userTable.addCell(command.getPassengerSurname());
            userTable.addCell(command.getPassengerName());
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            userTable.addCell(dateFormat.format(command.getPassengerBirthday()));
            document.add(userTable);

            //train information
            document.add(new Paragraph("Train information",
                    FontFactory.getFont(FontFactory.COURIER, 25, Font.BOLD,	new CMYKColor(0, 0, 0, 255))));
            document.add(new Paragraph("Train № " + command.getTrainNumber() + " " + command.getStationTrainDeparture()
                    + " - " + command.getStationTrainArrive(),
                    FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, Font.BOLD,	new CMYKColor(0, 0, 0, 255))));
            document.add(new Paragraph("Date of departure " + dateFormat.format(command.getDate()),
                    FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, Font.BOLD,	new CMYKColor(0, 0, 0, 255))));

            //ticket information
            document.add(new Paragraph("Ticket information",
                    FontFactory.getFont(FontFactory.COURIER, 25, Font.BOLD,	new CMYKColor(0, 0, 0, 255))));
            //table with user data
            PdfPTable ticketTable = new PdfPTable(4);
            ticketTable.setSpacingBefore(25);
            ticketTable.setSpacingAfter(290);
            c1 = new PdfPCell(new Phrase("Station departure"));
            ticketTable.addCell(c1);
            c2 = new PdfPCell(new Phrase("Time departure"));
            ticketTable.addCell(c2);
            c3 = new PdfPCell(new Phrase("Station arrive"));
            ticketTable.addCell(c3);
            PdfPCell c4 = new PdfPCell(new Phrase("Time arrive"));
            ticketTable.addCell(c4);

            ticketTable.addCell(command.getStationDeparture());
            ticketTable.addCell(command.getTimeDeparture());
            ticketTable.addCell(command.getStationArrive());
            ticketTable.addCell(command.getTimeArrive());
            document.add(ticketTable);
            //Have a good trip!
            document.add(new Paragraph("Have a good trip, " + command.getPassengerName() + "!",
                    FontFactory.getFont(FontFactory.COURIER_BOLD, 25, Font.BOLD,	new CMYKColor(0, 255, 0, 0))));


        }
        catch (Exception e) {

        }
        document.close();
    }

    /**
     * Create connection with server and get answer
     * @param in
     * @return
     */
    private Object createConnection(Object in) {
        Object out = null;
        try {
            Socket client = new Socket(HOST, PORT);

            //send
            ObjectOutputStream output = new ObjectOutputStream(new BufferedOutputStream(client.getOutputStream()));
            output.writeObject(in);
            output.flush();

            //receive
            ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(client.getInputStream()));
            out = input.readObject();

            client.close();
        } catch (IOException e) {
            logger.error("Error connecting with server");
            JOptionPane.showMessageDialog(null, "Error connecting with server", "Error", JOptionPane.ERROR_MESSAGE);
            logger.error(e.getMessage());

        } catch (ClassNotFoundException e) {
            logger.error("Error class not found");
            logger.error(e.getMessage());
        }
        return out;
    }
}
