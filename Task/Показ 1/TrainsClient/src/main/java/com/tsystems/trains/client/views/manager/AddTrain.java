package com.tsystems.trains.client.views.manager;

import com.tsystems.trains.client.controller.Controller;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 29.03.14
 * Time: 21:47
 * To change this template use File | Settings | File Templates.
 */

/**
 * View class for adding train
 */
public class AddTrain extends JFrame implements ActionListener {

    private JButton cancel, save, addStation;
    private JLabel numberTrainLabel, countStationLabel, dateLabel, countPassengersLabel;
    private JTextField numberTrain, countStations, countPassengers;
    private String[][] stations = new String[1][1];
    private JDatePickerImpl date;

    protected Controller controller;
    protected JPanel mainPanel;

    /**
     * class constructor
     */
    public AddTrain (Controller controller) {
        this.controller = controller;

        initWindow();

        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        addLogo();
        addLabelAndTextFields();
        addButtons();

        this.getContentPane().add(mainPanel);
    }

    /**
     * Initialization parameters of window
     */
    protected void initWindow() {
        this.setSize(250, 320);
        this.setResizable(false);
        this.setLocation(100, 100);
        this.setTitle("Trains");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Add logo of T-Systems
     */
    protected void addLogo() {
        JLabel labelLogo = new JLabel();
        ImageIcon icon = new ImageIcon("src\\main\\java\\com\\tsystems\\trains\\client\\views\\images\\tsystems.png");
        labelLogo.setIcon(icon);
        labelLogo.setBounds(0, 0, 250, 42);
        mainPanel.add(labelLogo);
    }

    /**
     * Add label and textfields
     */
    protected void addLabelAndTextFields() {

        numberTrainLabel = new JLabel("Number train");
        numberTrainLabel.setBounds(10, 50, 100, 20);

        numberTrain = new JTextField();
        numberTrain.setBounds(120, 45, 100, 30);

        countStationLabel = new JLabel("Count of station");
        countStationLabel.setBounds(10, 90, 100, 20);

        countStations = new JTextField();
        countStations.setBounds(120, 85, 100, 30);

        dateLabel = new JLabel("Date");
        dateLabel.setBounds(10, 130, 100, 20);

        countPassengersLabel = new JLabel("Count of passengers");
        countPassengersLabel.setBounds(10, 180, 120, 20);

        countPassengers = new JTextField();
        countPassengers.setBounds(135, 175, 100, 30);

        UtilDateModel dateModelFrom = new UtilDateModel();
        dateModelFrom.setValue(new Date());
        dateModelFrom.setSelected(true);
        JDatePanelImpl datePanelFrom = new JDatePanelImpl(dateModelFrom);
        date = new JDatePickerImpl(datePanelFrom);
        date.setBounds(80, 125, 140, 30);

        mainPanel.add(numberTrainLabel);
        mainPanel.add(date);
        mainPanel.add(numberTrain);
        mainPanel.add(countStationLabel);
        mainPanel.add(countStations);
        mainPanel.add(dateLabel);
        mainPanel.add(countStationLabel);
        mainPanel.add(countPassengers);
        mainPanel.add(countPassengersLabel);
    }

    /**
     * Add buttons
     */
    public void addButtons() {
        addStation = new JButton("Add stations");
        addStation.setBounds(50, 215, 150, 30);

        cancel = new JButton("Cancel");
        cancel.setBounds(10, 255, 100, 30);

        save = new JButton("Save");
        save.setBounds(135, 255, 100, 30);

        mainPanel.add(addStation);
        mainPanel.add(cancel);
        mainPanel.add(save);

        addStation.addActionListener(this);
        cancel.addActionListener(this);
        save.addActionListener(this);
    }


    /**
     * Action for buttons
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cancel) {
            this.dispose();
        }

        if (e.getSource() == save) {
            //check stations
            if(stations.length < 2)
                JOptionPane.showMessageDialog(this, "Enter stations", "Error", JOptionPane.ERROR_MESSAGE);
            else {
                if(numberTrain.getText().isEmpty()||countPassengers.getText().isEmpty())
                    JOptionPane.showMessageDialog(this, "Enter number of train and count of passengers", "Error", JOptionPane.ERROR_MESSAGE);
                else {
                    controller.saveTrain(this);
                }
            }
        }

        if (e.getSource() == addStation) {
            int countStationsInt = 0;
            try {
                countStationsInt = Integer.parseInt(countStations.getText());
            }
            catch (Exception ex) {
            }
            if(countStationsInt > 1) {
                controller.addingStationsForTrain(this);
            }
            else {
                JOptionPane.showMessageDialog(this, "Enter count of stations more 2", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Get date
     * @return date
     */
    public Date getDate() {
        Date selectedDate = (Date) date.getModel().getValue();
        return selectedDate;
    }

    /**
     * Get number of train
     * @return number
     */
    public String getTrainNumber() {
        return numberTrain.getText();
    }

    /**
     * Get count of stations
     * @return count
     */
    public int getCountStations() {
        return Integer.parseInt(countStations.getText());
    }

    public int getCountPassengers() {
        return Integer.parseInt(countPassengers.getText());
    }

    public String[][] getStations() {
        return stations;
    }

    public void setStations(String[][] stations) {
        this.stations = stations;
    }
}
