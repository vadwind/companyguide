package com.tsystems.trains.client;

import org.apache.log4j.Logger;

import com.tsystems.trains.client.controller.Controller;
import com.tsystems.trains.client.model.Model;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 23.03.14
 * Time: 15:15
 * To change this template use File | Settings | File Templates.
 */

/**
 * Main class
 */
public class TrainsClient {
    private static Logger logger = Logger.getLogger(TrainsClient.class);

    public static void main(String[] args) {
        logger.info("Run application client");
        Model model = new Model();
        Controller controller = new Controller(model);
    }

}
