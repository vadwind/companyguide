package com.tsystems.trains.client.views.manager;

import com.tsystems.trains.client.controller.Controller;
import com.tsystems.trains.client.views.client.ClientAccount;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 29.03.14
 * Time: 15:04
 * To change this template use File | Settings | File Templates.
 */
public class ManagerAccount extends ClientAccount implements ActionListener {

    protected JButton addStation, addTrain,
            deleteStation, deleteTrain, viewTrains, viewPassengers;

    private JComboBox stationsComboBox, trainsComboBox, trainsPassengersComboBox;


    /**
     * class constructor
     */
    public ManagerAccount(Controller controller, Map<String, String> userInfo,
             String[] stations, String[] trains) {
        super(controller, userInfo);
        this.setSize(680, 390);
        //addButtons();
        addComboBox(stations, trains);
    }


    /**
     * Add buttons on frame
     */
    @Override
    protected void addButtons() {
        cancel.setText("Close");
        cancel.setBounds(580, 310, 80, 30);

        save.setText("Save");
        save.setBounds(265, 310, 80, 30);

        addStation = new JButton("Add station");
        addStation.setBounds(390, 15, 150, 30);

        addTrain = new JButton("Add train");
        addTrain.setBounds(390, 65, 150, 30);

        deleteStation = new JButton("Delete station");
        deleteStation.setBounds(540, 115, 120, 30);

        deleteTrain = new JButton("Delete train");
        deleteTrain.setBounds(540, 165, 120, 30);

        viewTrains = new JButton("View all trains");
        viewTrains.setBounds(390, 215, 150, 30);

        viewPassengers = new JButton("View passengers");
        viewPassengers.setBounds(515, 265, 145, 30);

        mainPanel.add(cancel);
        mainPanel.add(save);
        mainPanel.add(addStation);
        mainPanel.add(addTrain);
        mainPanel.add(deleteStation);
        mainPanel.add(deleteTrain);
        mainPanel.add(viewTrains);
        mainPanel.add(viewPassengers);

        addStation.addActionListener(this);
        addTrain.addActionListener(this);
        deleteStation.addActionListener(this);
        deleteTrain.addActionListener(this);
        viewTrains.addActionListener(this);
        viewPassengers.addActionListener(this);
    }

    /**
     * Add comboBox
     * @param stations  Stations
     * @param trains Trains
     */
    private void addComboBox(String[] stations, String[] trains) {
        stationsComboBox = new JComboBox(stations);
        stationsComboBox.setBounds(360, 120, 150, 20);

        trainsComboBox = new JComboBox(trains);
        trainsComboBox.setBounds(360, 170, 150, 20);

        trainsPassengersComboBox = new JComboBox(trains);
        trainsPassengersComboBox.setBounds(360, 270, 150, 20);

        mainPanel.add(stationsComboBox);
        mainPanel.add(trainsComboBox);
        mainPanel.add(trainsPassengersComboBox);

    }

    /**
     * Action for buttons
     *
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cancel) {
            this.dispose();
        }

        if (e.getSource() == save) {

            String stringPassword = new String(password.getPassword());
            String stringRepeatPassword = new String(repeatPassword.getPassword());

            if (!stringPassword.equals(stringRepeatPassword)||stringPassword.equals("")) {
                JOptionPane.showMessageDialog(this, "The password fields do not match.", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                Date date = getDate();
                if (date == null)
                    JOptionPane.showMessageDialog(this, "You must enter date.", "Error", JOptionPane.ERROR_MESSAGE);
                else {
                    controller.changeUserData(this);
                }

            }
        }

        if (e.getSource() == addStation) {
            controller.openAddStation();
        }

        if (e.getSource() == addTrain) {
            controller.openAddingWindow();
        }

        if (e.getSource() == deleteStation) {
            int answer = JOptionPane.showConfirmDialog(this, "Do you delete station " + stationsComboBox.getSelectedItem() +"?");
            if(answer == 0)
                controller.deleteStation(this);
        }

        if (e.getSource() == deleteTrain) {
            int answer = JOptionPane.showConfirmDialog(this, "Do you delete train " + trainsComboBox.getSelectedItem() +"?");
            if(answer == 0)
                controller.deleteTrain(this);
        }

        if (e.getSource() == viewTrains) {
            controller.viewAllTrains();
        }

        if (e.getSource() == viewPassengers) {
            controller.viewPassengers(this);
        }

    }

    /**
     * Get select station
     */
    public String getSelectStation() {
        return (String) stationsComboBox.getSelectedItem();
    }

    /**
     * Get select train
     */
    public String getSelectTrain() {
        return (String) trainsComboBox.getSelectedItem();
    }

    /**
     * Get select train passengers
     */
    public String getSelectTrainPassengers() {
        return (String) trainsPassengersComboBox.getSelectedItem();
    }

    /**
     * change combobox
     * @param stations  array of stations
     */
    public void setNewStations(String[] stations) {
        mainPanel.remove(stationsComboBox);
        stationsComboBox = new JComboBox(stations);
        stationsComboBox.setBounds(360, 120, 150, 20);
        mainPanel.add(stationsComboBox);
    }

    /**
     * change combobox
     * @param trains  array of stations
     */
    public void setNewTrains(String[] trains) {
        mainPanel.remove(trainsComboBox);
        mainPanel.remove( trainsPassengersComboBox);

        trainsComboBox = new JComboBox(trains);
        trainsComboBox.setBounds(360, 170, 150, 20);

        trainsPassengersComboBox = new JComboBox(trains);
        trainsPassengersComboBox.setBounds(360, 270, 150, 20);

        mainPanel.add(trainsComboBox);
        mainPanel.add(trainsPassengersComboBox);
    }
}
