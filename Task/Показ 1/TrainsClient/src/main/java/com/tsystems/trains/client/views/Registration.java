package com.tsystems.trains.client.views;

import com.tsystems.trains.client.controller.Controller;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 20.03.14
 * Time: 21:36
 * To change this template use File | Settings | File Templates.
 */

/**
 * View class for registration new users
 */
public class Registration extends JFrame implements ActionListener {

    protected Controller controller;

    protected JPanel mainPanel;
    protected JTextField login, surname, name;
    protected JPasswordField password, repeatPassword;
    protected JDatePickerImpl birthday;

    protected JLabel labelEnter, labelLogin, labelPassword, labelRepeatPassword,
            labelSurname, labelName, labelBirthday;
    protected JButton cancel, save;

    /**
     * Default class constructor
     */
    public Registration(Controller controller) {

        this.controller = controller;

        initWindow();

        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        addLogo();
        addLabelsAndFields();
        addButtons();

        this.getContentPane().add(mainPanel);
    }

    /**
     * Initialization parameters of window
     */
    private void initWindow() {
        this.setSize(500, 350);
        this.setResizable(false);
        this.setLocation(100, 100);
        this.setTitle("Trains");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Add logo of T-Systems
     */
    private void addLogo() {
        JLabel labelLogo = new JLabel();
        ImageIcon icon = new ImageIcon("src\\main\\java\\com\\tsystems\\trains\\client\\views\\images\\tsystems.png");
        labelLogo.setIcon(icon);
        labelLogo.setBounds(0, 0, 250, 42);
        mainPanel.add(labelLogo);

    }

    /**
     * Add fields for login and password and other
     */
    private void addLabelsAndFields() {
        labelEnter = new JLabel("Registration user.  Please, enter your personal data");
        labelEnter.setBounds(10, 45, 450, 20);

        labelLogin = new JLabel("login");
        labelLogin.setBounds(10, 75, 100, 20);

        labelPassword = new JLabel("Password");
        labelPassword.setBounds(10, 140, 100, 20);

        labelRepeatPassword = new JLabel("Repeat password");
        labelRepeatPassword.setBounds(10, 205, 100, 20);

        login = new JTextField();
        login.setBounds(10, 95, 220, 30);

        password = new JPasswordField();
        password.setBounds(10, 160, 220, 30);

        repeatPassword = new JPasswordField();
        repeatPassword.setBounds(10, 225, 220, 30);

        labelSurname = new JLabel("Surname");
        labelSurname.setBounds(250, 75, 100, 20);

        labelName = new JLabel("Name");
        labelName.setBounds(250, 140, 100, 20);

        labelBirthday = new JLabel("Birthday");
        labelBirthday.setBounds(250, 205, 100, 20);

        surname = new JTextField();
        surname.setBounds(250, 95, 220, 30);

        name = new JTextField();
        name.setBounds(250, 160, 220, 30);

        UtilDateModel dateModel = new UtilDateModel();
        JDatePanelImpl datePanel = new JDatePanelImpl(dateModel);
        birthday = new JDatePickerImpl(datePanel);
        birthday.setBounds(250, 225, 220, 30);


        mainPanel.add(labelEnter);
        mainPanel.add(labelLogin);
        mainPanel.add(labelPassword);
        mainPanel.add(labelRepeatPassword);
        mainPanel.add(labelSurname);
        mainPanel.add(labelName);
        mainPanel.add(labelBirthday);

        mainPanel.add(login);
        mainPanel.add(password);
        mainPanel.add(repeatPassword);
        mainPanel.add(surname);
        mainPanel.add(name);
        mainPanel.add(birthday);

    }

    /**
     * Add buttons on frame
     */
    private void addButtons() {
        cancel = new JButton("Cancel");
        cancel.setBounds(10, 270, 80, 30);

        save = new JButton("Save");
        save.setBounds(390, 270, 80, 30);

        mainPanel.add(cancel);
        mainPanel.add(save);

        cancel.addActionListener(this);
        save.addActionListener(this);
    }


    /**
     * Action for buttons
     *
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cancel) {
            controller.cancelRegistration(this);
        }

        if (e.getSource() == save) {

            String stringPassword = new String(password.getPassword());
            String stringRepeatPassword = new String(repeatPassword.getPassword());

            if (!stringPassword.equals(stringRepeatPassword)) {
                JOptionPane.showMessageDialog(this, "The password fields do not match.", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                Date date = getDate();
                if (date == null)
                    JOptionPane.showMessageDialog(this, "You must enter date.", "Error", JOptionPane.ERROR_MESSAGE);
                else {
                    controller.registrationUser(this);
                }

            }
        }
    }

    /**
     * Get user`s login from GUI
     *
     * @return user`s login from textfield
     */
    public String getLogin() {
        return login.getText();
    }

    /**
     * Get user`s password from GUI
     *
     * @return user`s password from textfield
     */
    public String getPassword() {
        return new String(password.getPassword());
    }

    /**
     * Get  user`s surname from GUI
     *
     * @return user`s surname from textfield
     */
    public String getSurname() {
        return surname.getText();
    }

    /**
     * Get  user`s name from GUI
     *
     * @return user`s name from textfield
     */
    public String getUserName() {
        return name.getText();
    }

    /**
     * Get  user`s date from GUI
     *
     * @return user`s date from frame
     */
    public Date getDate() {
        Date selectedDate = (Date) birthday.getModel().getValue();
        return selectedDate;
    }
}

