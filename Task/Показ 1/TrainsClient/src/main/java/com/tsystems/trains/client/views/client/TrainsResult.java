package com.tsystems.trains.client.views.client;

import com.tsystems.trains.client.controller.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 22.03.14
 * Time: 22:47
 * To change this template use File | Settings | File Templates.
 */
public class TrainsResult extends TrainInformation implements ActionListener {

    private JButton buyTicket, viewTrain;
    private JLabel labelSelect;

    private String stantionDeparture;
    private String stantionArrive;


    /**
     * class constructor
     */
    public TrainsResult(Controller controller, String[] searchInfo, Object[][] trainsTable) {

        stantionArrive = searchInfo[0];
        stantionDeparture = searchInfo[1];

        this.controller = controller;
        initWindow();

        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        addLogo();
        addLabelAndTable(trainsTable);
        addButtons();

        this.getContentPane().add(mainPanel);
    }

    /**
     * Add labels and tabel
     */
    protected void addLabelAndTable(Object[][] trainsTable) {
        informationTrain = new JLabel("From stantion " + stantionArrive + " to stantion "+stantionDeparture + " next trains");
        informationTrain.setBounds(5, 45, 460, 20);

        //add table with timetable
        Object[] headerTable = {"Number train", "Name train", "Date", "Vacancies"};
        table = new JTable(trainsTable, headerTable);
        table.getColumnModel().getColumn(0).setResizable(false);
        table.getColumnModel().getColumn(1).setResizable(false);
        table.getColumnModel().getColumn(2).setResizable(false);
        table.getColumnModel().getColumn(3).setResizable(false);

        //add scroll
        scroll = new JScrollPane(table);
        table.setPreferredScrollableViewportSize(new Dimension(620, 290));
        scroll.setBounds(10, 70, 620, 290);


        mainPanel.add(informationTrain);
        mainPanel.add(scroll);

    }

    @Override
    /**
     * Add buttons on frame
     */
    protected void addButtons() {
        close = new JButton("Close");
        close.setBounds(10, 380, 80, 30);

        buyTicket = new JButton("Buy selected ticket");
        buyTicket.setBounds(480, 380, 150, 30);

        viewTrain = new JButton("View selected train");
        viewTrain.setBounds(320, 380, 150, 30);

        labelSelect = new JLabel("Select ticket for buying or view train");
        labelSelect.setBounds(100, 385, 250, 20);

        mainPanel.add(buyTicket);
        mainPanel.add(close);
        mainPanel.add(viewTrain);
        mainPanel.add(labelSelect);

        close.addActionListener(this);
        buyTicket.addActionListener(this);
        viewTrain.addActionListener(this);

    }

    /**
     * Action for buttons
     *
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == close) {
            this.dispose();
        }

        if (e.getSource() == buyTicket) {
            if(table.getSelectedRow() > -1) {
                int answer = JOptionPane.showConfirmDialog(this, "Do you want to buy this ticket?");
                if(answer == 0) {
                    controller.buyTicket(this);
                }
            }
            else
                JOptionPane.showMessageDialog(this, "You must select train.", "Error", JOptionPane.ERROR_MESSAGE);
        }

        if (e.getSource() == viewTrain) {
            if(table.getSelectedRow() > -1)
                controller.viewTrainInformationFromSearchTicket(this);
            else
                JOptionPane.showMessageDialog(this, "You must select train.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Get number ticket
     *
     * @return number
     */
    public String getSelectedRow() {
        int selectedRows = table.getSelectedRow();
        return (String) table.getModel().getValueAt(selectedRows, 0);
    }

    /**
     * Get number ticket
     *
     * @return number
     */
    public String getSelectedDate() {
        int selectedRows = table.getSelectedRow();
        return (String) table.getModel().getValueAt(selectedRows, 2);
    }

    public String getStantionDeparture() {
        return stantionDeparture;
    }

    public void setStantionDeparture(String stantionDeparture) {
        this.stantionDeparture = stantionDeparture;
    }

    public String getStantionArrive() {
        return stantionArrive;
    }

    public void setStantionArrive(String stantionArrive) {
        this.stantionArrive = stantionArrive;
    }
}

