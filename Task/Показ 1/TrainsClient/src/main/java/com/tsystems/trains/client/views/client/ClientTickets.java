package com.tsystems.trains.client.views.client;

import com.tsystems.trains.client.controller.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Вадим
 * Date: 22.03.14
 * Time: 22:25
 * To change this template use File | Settings | File Templates.
 */
public class ClientTickets extends TrainInformation implements ActionListener {
    private JButton printTicket;
    private JLabel labelSelect;

    /**
     * class constructor
     */
    public ClientTickets(Controller controller, Object[][] trainsTable) {

        this.controller = controller;

        initWindow();
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        addLogo();
        addLabelAndTable(trainsTable);
        addButtons();

        this.getContentPane().add(mainPanel);
    }

    /**
     * Add labels and tabel
     */
    protected void addLabelAndTable(Object[][] trainsTable) {
        informationTrain = new JLabel("All tickets client ");
        informationTrain.setBounds(5, 45, 460, 20);

        //add table with timetable
        Object[] headerTable = {"Number ticket", "Number train", "Station departure", "Station arrive", "Date", "Time"};
        //table = new JTable(trainsTable, headerTable);
        table = new JTable(trainsTable, headerTable);
        table.getColumnModel().getColumn(0).setResizable(false);
        table.getColumnModel().getColumn(1).setResizable(false);
        table.getColumnModel().getColumn(2).setResizable(false);
        table.getColumnModel().getColumn(3).setResizable(false);
        table.getColumnModel().getColumn(4).setResizable(false);
        table.getColumnModel().getColumn(5).setResizable(false);

        //add scroll
        scroll = new JScrollPane(table);
        table.setPreferredScrollableViewportSize(new Dimension(620, 290));
        scroll.setBounds(10, 70, 620, 290);


        mainPanel.add(informationTrain);
        mainPanel.add(scroll);

    }

    @Override
    /**
     * Add buttons on frame
     */
    protected void addButtons() {
        close = new JButton("Close");
        close.setBounds(10, 380, 80, 30);

        printTicket = new JButton("Print selected ticket");
        printTicket.setBounds(480, 380, 150, 30);

        labelSelect = new JLabel("Select ticket for printing");
        labelSelect.setBounds(220, 385, 150, 20);

        mainPanel.add(printTicket);
        mainPanel.add(close);
        mainPanel.add(labelSelect);

        close.addActionListener(this);
        printTicket.addActionListener(this);

    }

    /**
     * Action for buttons
     *
     * @param e action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == close) {
            this.dispose();
        }

        if (e.getSource() == printTicket) {
            if(table.getSelectedRow() != -1) {
                JFileChooser fileopen = new JFileChooser();
                int ret = fileopen.showDialog(null, "Save ticket");
                File file = fileopen.getSelectedFile();
                if(!file.getAbsolutePath().isEmpty())
                    controller.printTicket(this, file.getAbsolutePath());
                else
                    JOptionPane.showMessageDialog(this, "You must select file path.", "Error", JOptionPane.ERROR_MESSAGE);

            }
            else {
                JOptionPane.showMessageDialog(this, "You must select ticket.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Get number ticket
     *
     * @return number
     */
    public String getSelectedRow() {
        int selectedRows = table.getSelectedRow();
        return (String) table.getModel().getValueAt(selectedRows, 0);
    }
}

